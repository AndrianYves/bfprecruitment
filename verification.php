<?php
  require_once 'includes/conn.php';

  session_start();

  if(empty(isset($_SESSION['application']))){
    header('location: index.php');
    exit();
  } 

  if(isset($_POST['send'])){
    $conn->autocommit(FALSE);
    $error = false;

    if (empty($_POST['verificationcode'])) {
      $error = true;
      $_SESSION['error'][] = 'Verification Code is required.';
    } elseif(!is_numeric($_POST['verificationcode'])) {
      $error = true;
      $_SESSION['error'][] = 'Verification Code entered was not numeric.';
    } else {
      $verificationcode = $_POST['verificationcode'];
    }

    $sql = $conn->prepare("SELECT * FROM applicants_pds WHERE verificationcode = ? and id = ? and recruitmentDate = ?");
    $sql->bind_param("sis", $verificationcode, $_SESSION['application'], $end);
    $sql->execute();
    $query = $sql->get_result();
    $row = $query->fetch_assoc();
    if($row['verificationcode'] == $verificationcode){
      $result1 = $conn->prepare("UPDATE applicants_pds SET verified = ? WHERE id = ? and recruitmentDate = ?");
      $result1->bind_param("sis", $verified, $_SESSION['application'], $end);
      $verified = '1';
      $result1->execute();

      $result = $conn->prepare("INSERT INTO test_evaluation(recruitmentDate, applicantID) VALUES(?, ?)");
      $result->bind_param("si", $end, $row['id']);
      $result->execute();

    } else {
      $error = true;
      $_SESSION['error'][] = 'Incorrect Verification Code entered';
    }

    if (!$error) {
      $conn->autocommit(TRUE);
      header('location: success.php');
    } else {
      $conn->rollback();
    }

  }

?>
<?php include 'includes/header.php'; ?>

  <body>

  <?php include 'includes/nav.php'; ?>

    <!--Main layout-->
    <main class="mt-5 pt-5">
      <div class="container">
        <!--Grid row-->
        <div class="row py-5">
          <!--Grid column-->
          <div class="col-md-12 text-center">
          <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fa fa-warning'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>
      <?php
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "</a>
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>

       <!-- Heading & Description -->
        <div class="wow fadeIn">
          <!--Section heading-->
          <h2 class="h1 text-center mb-5">Verification Code Sent!</h2>
          <!--Section description-->
          <p class="text-center">Please enter your verification code</p>
        </div>
        <!-- Heading & Description -->

            <!-- Material form register -->
            <div class="card">
              <!--Card content-->
              <div class="card-body">
                <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                <div class="md-form">
                  <input type="number" id="verificationcode" name="verificationcode" class="form-control">
                  <label for="verificationcode">Verification Code</label>
                </div>
                <button type="submit" class="btn btn-primary waves-effect" name="send" id="send"><i class="fa fa-check-square-o"></i>Submit</button>
                    <!-- Form -->

              </div>
            </div>
            <!--Grid column-->


          </div>
          <!--Grid row-->
        </div>
        <!--Container-->
      </div>
    </main>
    <!--/Main layout-->


<?php include 'includes/footer.php'; ?>

<?php include 'includes/scripts.php'; ?>
  </body>
</html>
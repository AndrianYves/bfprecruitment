<?php
    include 'includes/conn.php';

    session_start();
    if(!empty(isset($_SESSION['admin']))){
      header('location:home.php');
    }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/style.min.css" rel="stylesheet">
    <title>Login Page</title>
  </head>
  <body>

<div class="container">

  <!--Section: Content-->
  <section class="p-3 my-md-3 text-center">
      <div class="row">
        <div class="col-md-5 mx-auto">
          <!-- Material form login -->
          <div class="card">

            <!--Card content-->
            <div class="card-body">

              <!-- Form -->
              <form class="text-center" action="login.php" method="POST">
                          <div class="text-center">
            <img style="height:150px;width:150px" alt="BFP Logo" src="logo.png">
        </div><br>

                <h3 class="font-weight-bold my-4 pb-2 text-center dark-grey-text">Log In</h3>

                <input type="text" id="username" name="username" placeholder="Username" class="form-control mb-4">

                <!-- Password -->
                <input type="password" id="password" name="password" class="form-control mb-4" placeholder="Password">

                <div class="text-center">
                  <button type="submit" name="login" class="btn btn-outline-info btn-rounded my-4 waves-effect">Login</button>
                </div>

              </form>
              <!-- Form -->
        <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fas fa-warning'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>
            </div>

          </div>
          <!-- Material form login -->
        </div>
      </div>

  </section>
  <!--Section: Content-->


</div>



  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Initializations -->
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();

  </script>

  </body>
</html>

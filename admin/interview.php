<?php
$title     = "Panel Interviewer";

$current = "Panel Interviewer";
?>

<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<?php
  if(strtotime($today) < strtotime($submissiondate) && $submissionstatus == 'START'){
    $_SESSION['error'][] = 'Submission of form still ongoing.';
    header('location: home.php');
  }

  if($submissionstatus == 'PAUSED'){
    $_SESSION['error'][] = 'Submission of form still ongoing but paused.';
    header('location: home.php');
  }
  
  $scoressql = $conn->prepare("SELECT panelinterviewpassingscore FROM settings where id = ?");
  $scoressql->bind_param("s", $id);
  $id = '1';
  $scoressql->execute();
  $scorequery = $scoressql->get_result();
  $row1 = $scorequery->fetch_assoc();
  $passingScore = $row1['panelinterviewpassingscore'];

  function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

  if(isset($_POST['submit'])){ 
    mysqli_autocommit($conn, false);
    $adminID=$_POST['adminid'];
    $timestamp = date("Y-m-d H:i:s");
    $error = false;
    $total = $potential = $psychosocial = $personality = "";

    if (empty(array_filter($_POST["potential"]))){
      $error = true;
      $_SESSION['error'][] = 'No applicant scored.';
    }

    $number = count($_POST["potential"]);
    for($i=0; $i<$number; $i++) {
      if(trim($_POST["potential"][$i] != '')) {
          if(empty($_POST["potential"][$i])){
            $error = true;
            $_SESSION['error'][] = 'Potential trait is required.';
          } elseif(!is_numeric($_POST["potential"][$i])) {
            $error = true;
            $_SESSION['error'][] = 'Potential trait entered was not numeric.';
          } else {
            $potential = test_input($_POST["potential"][$i]);
          }

        if(empty($_POST["psychosocial"][$i])){
            $error = true;
            $_SESSION['error'][] = 'Psychosocial Attribute is required.';
          } elseif(!is_numeric($_POST["psychosocial"][$i])) {
            $error = true;
            $_SESSION['error'][] = 'Psychosocial Attribute entered was not numeric.';
          } else {
            $psychosocial = test_input($_POST["psychosocial"][$i]);
          }

        if(empty($_POST["personality"][$i])){
            $error = true;
            $_SESSION['error'][] = 'Personality Traits is required.';
          } elseif(!is_numeric($_POST["personality"][$i])) {
            $error = true;
            $_SESSION['error'][] = 'Personality Traits entered was not numeric.';
          } else {
            $personality = test_input($_POST["personality"][$i]);
            $total = $potential + $psychosocial + $personality;
          }

        $remarks = mysqli_real_escape_string($conn, test_input($_POST["remarks"][$i]));
        $applicantID = mysqli_real_escape_string($conn, test_input($_POST["id"][$i]));

        if ($potential > '40' || $psychosocial > '30' || '30' < $personality){
          $error = true;
          $_SESSION['error'][] = 'Score Exceeded';
        } else {
          if ($total >= $passingScore) {
            $status = 'PASSED';
            $result = $conn->prepare("INSERT INTO test_investigator(recruitmentDate, applicantID) VALUES(?, ?)");
            $result->bind_param("si", $recruitmentDate, $applicantID);
            $result->execute();
            $result->close();
          } else {
            $status = 'FAILED';
          }

          $result1 = $conn->prepare("UPDATE test_interview SET potential = ?, psychosocial = ?, personality = ?, total = ?, remarks = ?, status = ?, timestamp = ?, adminID = ? WHERE applicantID = ?");
          $result1->bind_param("iiiisssii", $potential, $psychosocial, $personality, $total, $remarks, $status, $timestamp, $adminID, $applicantID);
          $result1->execute();
          $result1->close();
        }
      }
    }

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Save';
    } else {
      mysqli_rollback($conn);
    }
  }

?>
<?php
  if (isset($_POST['edit'])) {
    mysqli_autocommit($conn, false);
    $timestamp = date("Y-m-d H:i:s");
    $allowed = array('PASSED', 'FAILED');
    $error = false;
    $total = $potential = $psychosocial = $personality = "";

    if(empty($_POST['epersonality'])) {
      $error = true;
      $_SESSION['error'][] = 'Personality entered required.';
    }else if(!is_numeric($_POST['epersonality'])) {
      $error = true;
      $_SESSION['error'][] = 'Personality entered was not numeric.';
    } else {
      $personality = test_input($_POST['epersonality']);
    } 

    if(empty($_POST['epsychosocial'])) {
      $error = true;
      $_SESSION['error'][] = 'Psychosocial entered required.';
    }else if(!is_numeric($_POST['epsychosocial'])) {
      $error = true;
      $_SESSION['error'][] = 'Psychosocial entered was not numeric.';
    } else {
      $psychosocial = test_input($_POST['epsychosocial']);
    } 

    if(empty($_POST['epotential'])) {
      $error = true;
      $_SESSION['error'][] = 'Potential entered required.';
    } else if(!is_numeric($_POST['epotential'])) {
      $error = true;
      $_SESSION['error'][] = 'Potential entered was not numeric.';
    } else {
      $potential = test_input($_POST['epotential']);
    } 

    if(!empty($_POST['eremarks'])) {
      $remarks = test_input($_POST['eremarks']);
    } 

    if (empty($_POST['applicantID'])) {
      $error = true;
      $_SESSION['error'][] = 'Error. Somethings missing.';
    } else {
      $applicantID = $_POST['applicantID'];
    }

    $total = $potential + $psychosocial + $personality;

    if($total >= $passingScore){
      $status = 'PASSED';
      $result = $conn->prepare("INSERT INTO test_investigator(recruitmentDate, applicantID) VALUES(?, ?)");
      $result->bind_param("si", $recruitmentDate, $applicantID);
      $result->execute();
      $result->close();
    } else {
      $status = 'FAILED';
      $result = $conn->prepare("DELETE FROM test_investigator WHERE applicantID = ?");
      $result->bind_param("i", $applicantID);
      $result->execute();
      $result->close();
    }

    $result1 = $conn->prepare("UPDATE test_interview SET potential = ?, psychosocial = ?, personality = ?, total = ?, remarks = ?, status = ?, timestamp = ?, adminID = ? WHERE applicantID = ?");
    $result1->bind_param("iiiisssii", $potential, $psychosocial, $personality, $total, $remarks, $status, $timestamp, $adminID, $applicantID);
    $result1->execute();
    $result1->close();

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Status Updated';
    } else {
      mysqli_rollback($conn);
    }
  }

?>
<?php
  if (isset($_POST['changestatus'])) {
    $result = $conn->prepare("UPDATE tests_status SET status='DONE' WHERE `test`='Panel Interview'");
    $result->execute();
    $result->close();

    $result3 = $conn->prepare("UPDATE tests_status SET status='ON-GOING' WHERE `test`='Background Investigation'");
    $result3->execute();
    $result3->close();

  }
?>

<body class="grey lighten-3">
  <?php include 'includes/nav.php'; ?>

<?php if ($role == 'Panel Interviewer' || $role == 'HR Officer'): ?>
<?php
    $result1 = $conn -> prepare("SELECT status FROM `tests_status` where `test` = 'Panel Interview'");
    $result1->execute();
    $query = $result1->get_result();
    $row = $query->fetch_assoc();
    $testStatus = $row['status'];
    $result1->close();

  switch ($testStatus): 
?>
<?php case "ON-GOING": ?>
<!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
        <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fas fa-warning'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>

      <?php
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fas fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
<?php 
  $result2 = mysqli_query($conn, "SELECT * FROM `test_interview` join applicants_pds on applicants_pds.id = test_interview.applicantID where `status` is not null and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");

  $sql = mysqli_query($conn, "SELECT * FROM `test_interview` join applicants_pds on applicants_pds.id = test_interview.applicantID where applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");

    $result13 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_interview on applicants_pds.id = test_interview.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate'");

?>

    <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Panel Interview</span>
          </h4>
          <h4 class="mb-2 mb-sm-0 pt-1 text-right">Processed <?php echo mysqli_num_rows($result2);?> out of <?php echo mysqli_num_rows($sql);?> applicants</h4>
        </div>
      </div>
      <!-- Heading -->

 <div class="row">
        <div class="col-md-12">
          <div class="card">
            <!--Card content-->
            <div class="card-body">
              <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                <div class="row">
                  <div class="col-6 float-left">
                    <button class="btn btn-outline-success" name="submit" type="submit"><i class="fas fa-save" aria-hidden="true"></i> Save</button>
                  </div>
                   <div class="col-6 float-right">
                    <h4 class="mb-2 mb-sm-0 pt-1 text-right text-success"> <?php echo mysqli_num_rows($result13);?> Passed</h4>
                 
                </div>
              </div>
              <h5 class="float-left">Passing percentage: <?php echo $passingScore; ?></h5>
              <ul class="nav nav-pills mb-3 justify-content-end" id="pills-tab" role="tablist">
              <li class="nav-item">
              <a class="btn-primary nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
              aria-controls="pills-home" aria-selected="true">ALL</a>
              </li>
              <li class="nav-item">
              <a class="btn-info nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
              aria-controls="pills-profile" aria-selected="false">Finished</a>
              </li>
            </ul>
            <div class="tab-content pt-2 pl-1" id="pills-tabContent">
              <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

              <table class="table table-bordered table-responsive display" cellspacing="0" width="100%">
                <thead>
                  <th width="50">Applicant Number</th>
                  <th width="250">Full Name</th>
                  <th width="50">Potential Trait (40 pts.)</th>
                  <th width="50">Psychosocial Attributes (30 pts.)</th>
                  <th width="50">Personal Traits (30 pts.)</th>
                  <th width="150">Remarks</th>
                  <th width="50">View Profile</th>
                </thead>
                <tbody>

                  <?php
                    $result1 = mysqli_query($conn, "SELECT * FROM `test_interview` join applicants_pds on applicants_pds.id = test_interview.applicantID where `total` is NULL and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");
                    $newStatus = mysqli_num_rows($result1);
                    while ($row = mysqli_fetch_array($result1)) {
                       $potential = '<input type="number" name="potential[]" min="0" max="40" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" class="form-control mb-4" placeholder="Score">
                                <input class="form-check-input" type="hidden" name="id[]" id="'.$row['applicantID'].'" value="'.$row['applicantID'].'" style="visibility: hidden;">
                                <input class="form-check-input" type="hidden" name="adminid" id="adminid" value="'.$user['id'].'" style="visibility: hidden;">';

                        $psychosocial = '<input type="number" name="psychosocial[]" min="0" max="30" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" class="form-control mb-4" placeholder="Score">';

                        $personality = '<input type="number" name="personality[]" min="0" max="30" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" class="form-control mb-4" placeholder="Score">';

                        $remarks = '<textarea type="text" class="form-control rounded-0" id="remarks" name="remarks[]" rows="3"></textarea>';
                      echo "
                        <tr>
                          <td>".$row['applicantnumber']."</td>
                          <td>".ucwords($row['last']).", ".ucwords($row['name'])." ".ucwords($row['middle'])."</td>
                          <td>".$potential."</td>
                          <td>".$psychosocial."</td>
                          <td>".$personality."</td>
                          <td>".$remarks."</td>
                          <td><a href='evaluationview.php?id=$row[applicantID]'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                        </tr>
                      ";
                    }
                  ?>
                
                </tbody>
              </table>

               </div>
              <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        <table class="table table-bordered display table-responsive" cellspacing="0" width="100%">
                <thead>
                  <th width="50">Applicant Number</th>
                  <th width="250">Full Name</th>
                  <th width="50">Potential Trait</th>
                  <th width="50">Psychosocial Attributes</th>
                  <th width="50">Personal Traits</th>
                  <th width="150">Remarks</th>
                  <th width="100">Status</th>
                  <th width="50">View Profile</th>
                  <th width="120">Action</th>
                </thead>
                <tbody>

                  <?php
                    $result2 = mysqli_query($conn, "SELECT * FROM `test_interview` join applicants_pds on applicants_pds.id = test_interview.applicantID where `status` is not null and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");
                    while ($row = mysqli_fetch_array($result2)) {
                    if ($row['status'] == 'PASSED'){
                        $color = 'success';
                      } else {
                        $color = 'danger';
                      }
                      ?>
                        <tr>
                          <td><?php echo $row['applicantnumber'];?></td>
                          <td><?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?></td>
                          <td><?php echo $row['potential'];?></td>
                          <td><?php echo $row['psychosocial'];?></td>
                          <td><?php echo $row['personality'];?></td>
                          <td scope="row"><?php echo $row['remarks'];?></td>
                          <td><span class='badge badge-pill badge-<?php echo $color?>'><?php echo $row['status'];?></span></td>
                          <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                          <td>
                           <div class='text-center'><a data-toggle='modal' data-target='#edit<?php echo $row['applicantID']; ?>' href='#edit?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-edit blue-text'></i></a></div>
                          </td>
                        </tr>
            <div class="modal fade" id="edit<?php echo $row['applicantID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold"><?php echo $row['applicantID'];?> - <?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?>
                    </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                    </button>
                  </div>
                  <div class="modal-body mx-3">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                      <div class="text-center">
                        <input type="hidden" id="id" name="applicantID" class="form-control validate" value="<?php echo $row['applicantID']; ?>">
                        <div class="md-form mt-0">
                        <input type="number" id="epotential" name="epotential" class="form-control validate" value="<?php echo $row['potential']; ?>">
                        <label for="potential">Potential Trait Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" id="epsychosocial" name="epsychosocial" class="form-control validate" value="<?php echo $row['psychosocial']; ?>">
                        <label for="psychosocial">Psychosocial Attribute Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" id="epersonality" name="epersonality" class="form-control validate" value="<?php echo $row['personality']; ?>">
                        <label for="birthplace">Personal Traits Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <textarea type="text" class="form-control rounded-0" id="eremarks" name="eremarks" rows="3" value="<?php echo $row['remarks']; ?>"></textarea>
                        <label for="birthplace">Remarks</label>
                        </div>

                      </div>
                      <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-outline-warning waves-effect" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-outline-primary waves-effect" name="edit" id="edit">Update</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php    } ?>

                </tbody>
              </table>
  </div>
</div>
                <div class="row">
                  <div class="col-6">
                    <button class="btn btn-outline-success waves-effect" name="submit" id="submit"><i class="far fa-save pr-2" aria-hidden="true"></i>Save</button></form>
                  </div>
                  <div class="col-6 text-right">
                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalLoginForm" <?php if($newStatus > 0){ echo 'disabled'; }?>>Finalize</button>
                  </div>
                </div>
            </div>

          </div>
        </div>
      </div>

  </div><!--/container-->
</main><!--/Main layout-->

<?php if($newStatus == 0){?>
<!-- Are you sure -->
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Are you sure?</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <img src="logo.png" class="center mb-3 animated rotateIn rounded-circle" style="width: 150px; height: 150px;">
          <p class="text-center">Please double check the assessment before submitting to avoid unncessary mistakes.<br>
                  Once submitted will be the final result of the panel interview.
          </p>

        </div>
      </div>

      <!--Footer-->
 <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data" class="needs-validation" novalidate>
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</a>
        <button class="btn btn-primary waves-effect" name="changestatus" id="changestatus"><i class="fa fa-check-square-o"></i>Yes</button>
      </div>
    </form>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<?php }?>
<?php break; ?>
<?php case "DONE": ?>
<!--Main layout-->
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-success alert-dismissible fade show' role='alert'>
    <h4><i class='icon fa fa-check'></i> Done!</h4>
    <a href='interviewresults.php' class='alert-link'>View Results</a>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->
<?php break; ?>
<?php default: ?>
<!--Main layout-->
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-info alert-dismissible fade show' role='alert'>
    <h4>Physical Agility Test not yet finished!</h4>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->


<?php endswitch; ?>
<?php else: ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-danger alert-dismissible fade show' role='alert'>
    <h4>Forbidden.</h4>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->


<?php endif ?>

<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>

</body>
</html>
<?php
  $page      = "evaluation.php";
  $title     = "Applicants";
  $current = "home";
  ?>
<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="grey lighten-3">
  <?php include 'includes/nav.php'; ?>

  <?php
  $previous = "javascript:history.go(-1)";
if(isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}

  $recdate = $_GET['recruitmentDate'];


  // define the path and name of cached file
  $cachefile = 'cached/previous recruitment/'.$recdate.'.php';
  // Check if the cached file is still fresh. If it is, serve it up and exit.
  if (file_exists($cachefile)) {
    include($cachefile);
      exit;
  }
  // if there is either no file OR the file to too old, render the page and capture the HTML.
  ob_start();

  ?>



  <?php if ($role == 'HR Officer'): ?>
  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Applicants</span>
            <span>/</span>
            <span><?php echo date("F d, Y", strtotime($recdate)); ?></span>
          </h4>
          <h4 class="mb-2 mb-sm-0 pt-1"><a class="text-right" href="<?= $previous ?>">Back</a></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="card">
          <!--Card content-->
          <div class="card-body">

                <table class="table table-bordered table-responsive-md display" cellspacing="0" width="100%">
                <thead>
                  <th width="200">Full Name</th>
                  <th width="20">Medical Results</th>
                  <th width="20">Written Exam Score</th>
                  <th width="20">Physical Agility Score</th>
                  <th width="20">Panel Interview Score</th>
                  <th width="20">Background Investigation Results</th>
                  <th width="20">Neuro Results</th>
                  <th width="20">View Profile</th>
                </thead>
                <tbody>
                  <?php
                    $result1 = mysqli_query($conn, "SELECT * FROM applicants_pds left join test_evaluation on test_evaluation.applicantID = applicants_pds.id where applicants_pds.recruitmentDate = '$recdate' and test_evaluation.status = 'Accepted' order by applicants_pds.last");
                    $newStatus = mysqli_num_rows($result1);
                    while ($row = mysqli_fetch_array($result1)) {
                      $query1 = mysqli_query($conn, "SELECT * FROM `test_med` join applicants_pds on applicants_pds.id = test_med.applicantID where test_med.applicantID = '".$row['id']."'");
                      $row1 = mysqli_fetch_assoc($query1);


                      $query2 = mysqli_query($conn, "SELECT * FROM `test_ppt` join applicants_pds on applicants_pds.id = test_ppt.applicantID where test_ppt.applicantID = '".$row['id']."'");
                      $row2 = mysqli_fetch_assoc($query2);


                      $query3 = mysqli_query($conn, "SELECT * FROM `test_pat` join applicants_pds on applicants_pds.id = test_pat.applicantID where test_pat.applicantID = '".$row['id']."'");
                      $row3 = mysqli_fetch_assoc($query3);

                   
                      $query4 = mysqli_query($conn, "SELECT * FROM `test_interview` join applicants_pds on applicants_pds.id = test_interview.applicantID where test_interview.applicantID = '".$row['id']."'");
                      $row4 = mysqli_fetch_assoc($query4);

                      $query5 = mysqli_query($conn, "SELECT * FROM `test_investigator` join applicants_pds on applicants_pds.id = test_investigator.applicantID where test_investigator.applicantID = '".$row['id']."'");
                      $row5 = mysqli_fetch_assoc($query5);


                      $query6 = mysqli_query($conn, "SELECT * FROM `test_neuro` join applicants_pds on applicants_pds.id = test_neuro.applicantID where test_neuro.applicantID = '".$row['id']."'");
                      $row6 = mysqli_fetch_assoc($query6);


                      if ($row1['status'] == 'PASSED'){
                      $color = 'success';
                      } else {
                      $color = 'danger';
                      }
                      $status1 = '<h6><span class="badge badge-pill badge-'.$color.'">'.$row1['status'].'</span></h6>';

                      if ($row2['status'] == 'PASSED'){
                      $color = 'success';
                      } else {
                      $color = 'danger';
                      }
                      $status2 = '<h6><span class="badge badge-pill badge-'.$color.'">'.$row2['status'].'</span></h6>';

                              if ($row3['status'] == 'PASSED'){
                      $color = 'success';
                      } else {
                      $color = 'danger';
                      }
                      $status3 = '<h6><span class="badge badge-pill badge-'.$color.'">'.$row3['status'].'</span></h6>';

                              if ($row4['status'] == 'PASSED'){
                      $color = 'success';
                      } else {
                      $color = 'danger';
                      }
                      $status4 = '<h6><span class="badge badge-pill badge-'.$color.'">'.$row4['status'].'</span></h6>';

                      if ($row5['status'] == 'PASSED'){
                      $color = 'success';
                      } else {
                      $color = 'danger';
                      }
                      $status5 = '<h6><span class="badge badge-pill badge-'.$color.'">'.$row5['status'].'</span></h6>';

                      if ($row6['status'] == 'PASSED'){
                      $color = 'success';
                      } else {
                      $color = 'danger';
                      }
                      $status6 = '<h6><span class="badge badge-pill badge-'.$color.'">'.$row6['status'].'</span></h6>';

                      ?>
                  <tr>
                    <td><?php echo ucwords($row['last']); ?>, <?php echo ucwords($row['name']); ?> <?php echo ucwords($row['middle']); ?></td>
                    <td><?php echo $status1; ?></td>
                    <td><?php echo $status2; echo $row2['score']; ?></td>
                    <td><?php echo $status3; echo $row3['total']; ?></td>
                    <td><?php echo $status4; echo $row4['total']; ?></td>
                    <td><?php echo $status5; ?></td>
                    <td><?php echo $status6; ?></td>
                    <td><a href='evaluationview.php?id=<?php echo $row['id']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                  <?php } ?>
                </tbody>
              </table>



            
            </div>

          </div>
        </div>
      </div>



    </div><!--/container-->
  </main>
  <!--/Main layout-->
  <?php else: ?>
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
      <div class='alert alert-danger alert-dismissible fade show' role='alert'>
        <h4>Forbidden.</h4>
      </div>
    </div>
    <!--/container-->
  </main>
  <!--/Main layout-->
  <?php endif ?>
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/scripts.php'; ?>
</body>
</html>

<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);
  // We're done! Save the cached content to a file
  $fp = fopen($cachefile, 'w');
  fwrite($fp, ob_get_contents());
  fclose($fp);
  // finally send browser output
  ob_end_flush();

?>
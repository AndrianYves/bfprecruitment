<?php include 'includes/session.php'; ?>
<?php 
  $pageTitle = "Home";
  $page      = "Home.php";
  $title     = "Home";
  include 'includes/header.php'; ?>

<body class="grey lighten-3">

  <?php
  $current = "home";
  include 'includes/nav.php'; ?>

<?php 
switch ($role): ?>
<?php case "Medical Examiner": ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

      <!-- Heading -->
      <div class="card mb-4 wow fadeIn">

        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">

          <h5 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Dashboard</span>
          </h5>

        </div>

      </div>
      <!-- Heading -->

  </div><!--Container-->
</main><!--Main laypassed out-->

<?php break; ?>
<?php case "Written Exam Handler": ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

      <!-- Heading -->
      <div class="card mb-4 wow fadeIn">

        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">

          <h5 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Dashboard</span>
          </h5>

        </div>

      </div>
      <!-- Heading -->

  </div><!--Container-->
</main><!--Main laypassed out-->

<?php break; ?>
<?php case "Handler": ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

      <!-- Heading -->
      <div class="card mb-4 wow fadeIn">

        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">

          <h5 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Dashboard</span>
          </h5>

        </div>

      </div>
      <!-- Heading -->

  </div><!--Container-->
</main><!--Main laypassed out-->

<?php break; ?>
<?php case "Panel Interviewer": ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

      <!-- Heading -->
      <div class="card mb-4 wow fadeIn">

        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">

          <h5 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Dashboard</span>
          </h5>

        </div>

      </div>
      <!-- Heading -->

  </div><!--Container-->
</main><!--Main laypassed out-->
      
<?php break; ?>
<?php case "Background Investigator": ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

      <!-- Heading -->
      <div class="card mb-4 wow fadeIn">

        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">

          <h5 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Dashboard</span>
          </h5>

        </div>

      </div>
      <!-- Heading -->

  </div><!--Container-->
</main><!--Main laypassed out-->

<?php break; ?>
<?php case "Neurological Examiner": ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

      <!-- Heading -->
      <div class="card mb-4 wow fadeIn">

        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">

          <h5 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Dashboard</span>
          </h5>

        </div>

      </div>
      <!-- Heading -->

  </div><!--Container-->
</main><!--Main laypassed out-->

<?php break; ?>
<?php default: ?>

<?php endswitch; ?>
  <!--Main laypassed out-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
        <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fas fa-warning'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>

      <?php
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fas fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn">

        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">

          <h5 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Dashboard</span>
          </h5>

        </div>

      </div>
      <!-- Heading -->

      <div class="row">

        <div class="col-lg-3 col-xs-6 col-md-4">
          <div class="inner">
            <div class="card mb-4 p-3">
              <div class="inner">
                 <?php
                  $sql = "SELECT * FROM applicants_pds where verified = '1'";
                  $query = mysqli_query($conn, $sql);
                
                echo "<h5><strong>".mysqli_num_rows($query)."</strong></h5>";
                ?>
                <p>Total Applicants</p>
              </div>
              <div class="card-footer">
                <strong><a href="applicants.php" class="small-box-footer black-text" >More info </a></strong>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6 col-md-4">
          <div class="inner">
            <div class="card mb-4 p-3">
              <div class="inner">
                 <?php
                  $sql = "SELECT * FROM applicants_pds where recruitmentDate = '$recruitmentDate'";
                  $query = mysqli_query($conn, $sql);
                
                echo "<h5><strong>".mysqli_num_rows($query)."</strong></h5>";
                ?>
                <p>Current Applicants</p>
              </div>
              <div class="card-footer">
                <strong><a href="allapplicants.php" class="small-box-footer black-text" >More info </a></strong>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6 col-md-4">
          <!-- small box -->
          <div class="card mb-4 p-3">
              <div class="inner">
                 <?php
                  $sql = "SELECT * FROM applicants_pds join test_neuro on applicants_pds.id = test_neuro.applicantID where verified = '1' and status = 'PASSED'";
                  $query = mysqli_query($conn, $sql);
                
                echo "<h5><strong>".mysqli_num_rows($query)."</strong></h5>";
                ?>
                <p>Total Passers</p>
              </div>
            <div class="card-footer">
              <strong><a href="applicants.php" class="small-box-footer black-text" >More info </a></strong>
            </div>
          </div>
        </div>
<!-- 
        <div class="col-lg-3 col-xs-6 col-md-4">
          <div class="card mb-4 p-3 purple-gradient">
            <div class="inner">
              <h5><strong>Applicant's Files</strong></h5>
            </div>
            <div class="card-footer">
              <strong><a href="filebrowser.php" class="small-box-footer black-text" >More info </a></strong>
            </div>
          </div>
        </div> -->

        <!-- ./col -->

        <div class="col-lg-3 col-xs-6 col-md-4">
          <!-- small box -->
          <div class="card mb-4 p-3">
            <div class="inner">
              <h5><strong>Settings</strong></h5>
              <p>Change Settings</p>
            </div>
            <div class="card-footer">
              <strong><a href="settings.php" class="small-box-footer black-text" >More info </a></strong>
            </div>
          </div>
        </div>
        <!-- ./col -->

      </div><!--row-->


      <div class="row">
        <div class="col-lg-3 col-xs-6 col-md-4">
          <div class="card mb-4 p-3 white">
            <div class="inner">
              <?php

                $sql = "SELECT * FROM tests_status where status = 'ON-GOING'";
                $query = mysqli_query($conn, $sql);
                $row = mysqli_fetch_assoc($query);
                $test = $row['test'];

                if (mysqli_num_rows($query) == '0') {
                  $sql1 = "SELECT * FROM tests_status where status = 'START' and test='Medical/Dental and Physical Test'";
                  $query1 = mysqli_query($conn, $sql1);

                  if (mysqli_num_rows($query1) == '1'){ 
                    echo "<h5><strong>Recruitment not started</strong></h5>";
                  } else {
                    echo "<h5><strong>Recruitment stages done</strong></h5>";
                  }
                } else {
                  echo "<h5><strong>".$test."</strong></h5>";
                }
                ?>
            </div>
            <div class="card-footer">
              <strong><p class="small-box-footer black-text" >Current Stage</p></strong>
            </div>
          </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6 col-md-4">
          <div class="inner">
              <?php 
                $query20 = mysqli_query($conn, "SELECT * from tests_status where test = 'Medical/Dental and Physical Test'");
                $row1 = mysqli_fetch_assoc($query20);
                  if ($row1['status'] == 'ON-GOING'){
                    $medcolor = 'primary-color';
                  } elseif ($row1['status'] == 'DONE'){
                    $medcolor = 'success-color';
                  } elseif ($row1['status'] == 'START'){
                    $medcolor = 'white';
                  }
              ?>
            <div class="card mb-4 p-3 <?php echo $medcolor; ?>">
              <div class="inner">
                 <?php
                 $query2 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_med on applicants_pds.id = test_med.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate'");

                 $query3 = mysqli_query($conn, "SELECT * FROM test_med where recruitmentDate = '$recruitmentDate'");
                
                  echo "<h6>".mysqli_num_rows($query2)." passed out of ".mysqli_num_rows($query3)." applicants</h6>";
                  ?>
              </div>
              <div class="card-footer">
                <strong><a href="med.php" class="small-box-footer black-text">Medical and Physical Test</a></strong>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6 col-md-4">
          <div class="inner">
            <?php 
                $query20 = mysqli_query($conn, "SELECT * from tests_status where test = 'Pen and Paper Test'");
                $row1 = mysqli_fetch_assoc($query20);
                  if ($row1['status'] == 'ON-GOING'){
                    $pptcolor = 'primary-color';
                  } elseif ($row1['status'] == 'DONE'){
                    $pptcolor = 'success-color';
                  } elseif ($row1['status'] == 'START'){
                    $pptcolor = 'white';
                  }
              ?>
            <div class="card mb-4 p-3 <?php echo $pptcolor; ?>">
              <div class="inner">
                 <?php
                 $query4 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_ppt on applicants_pds.id = test_ppt.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate'");

                 $query5 = mysqli_query($conn, "SELECT * FROM test_ppt where recruitmentDate = '$recruitmentDate'");
          
                  echo "<h6>".mysqli_num_rows($query4)." passed out of ".mysqli_num_rows($query5)." applicants</h6>";
                  ?>
              </div>
              <div class="card-footer">
                <strong><a href="ppt.php" class="small-box-footer black-text" >Pen and Paper Test</a></strong>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6 col-md-4">
          <div class="inner">
            <?php 
                $query20 = mysqli_query($conn, "SELECT * from tests_status where test = 'Physical Agility Test'");
                $row1 = mysqli_fetch_assoc($query20);
                  if ($row1['status'] == 'ON-GOING'){
                    $patcolor = 'primary-color';
                  } elseif ($row1['status'] == 'DONE'){
                    $patcolor = 'success-color';
                  } elseif ($row1['status'] == 'START'){
                    $patcolor = 'white';
                  }
              ?>
            <div class="card mb-4 p-3 <?php echo $patcolor; ?>">
              <div class="inner">
                 <?php
                 $query6 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_pat on applicants_pds.id = test_pat.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate'");

                 $query7 = mysqli_query($conn, "SELECT * FROM test_pat where recruitmentDate = '$recruitmentDate'");
                
                  echo "<h6>".mysqli_num_rows($query6)." passed out of ".mysqli_num_rows($query7)." applicants</h6>";
                ?>
              </div>
              <div class="card-footer">
                <strong><a href="pat.php" class="small-box-footer black-text" >Physical Agility Test</a></strong>
              </div>
            </div>
          </div>
        </div>
        
      </div><!--row-->
      <div class="row">
        <div class="col-lg-3 col-xs-6 col-md-4">
          <div class="inner">
            <?php 
                $query20 = mysqli_query($conn, "SELECT * from tests_status where test = 'Panel Interview'");
                $row1 = mysqli_fetch_assoc($query20);
                  if ($row1['status'] == 'ON-GOING'){
                    $picolor = 'primary-color';
                  } elseif ($row1['status'] == 'DONE'){
                    $picolor = 'success-color';
                  } elseif ($row1['status'] == 'START'){
                    $picolor = 'white';
                  }
              ?>
            <div class="card mb-4 p-3 <?php echo $picolor; ?>">
              <div class="inner">
                 <?php
                 $query8 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_interview on applicants_pds.id = test_interview.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate'");

                 $query9 = mysqli_query($conn, "SELECT * FROM test_interview where recruitmentDate = '$recruitmentDate'");
                  echo "<h6>".mysqli_num_rows($query8)." passed out of ".mysqli_num_rows($query9)." applicants</h6>";
                  ?>

              </div>
              <div class="card-footer">
                <strong><a href="interview.php" class="small-box-footer black-text" >Panel Interview</a></strong>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6 col-md-4">
          <div class="inner">
            <?php 
                $query20 = mysqli_query($conn, "SELECT * from tests_status where test = 'Background Investigation'");
                $row1 = mysqli_fetch_assoc($query20);
                  if ($row1['status'] == 'ON-GOING'){
                    $bicolor = 'primary-color';
                  } elseif ($row1['status'] == 'DONE'){
                    $bicolor = 'success-color';
                  } elseif ($row1['status'] == 'START'){
                    $bicolor = 'white';
                  }
              ?>
            <div class="card mb-4 p-3 <?php echo $bicolor; ?>">
              <div class="inner">
                 <?php
                 $query10 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_investigator on applicants_pds.id = test_investigator.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate'");

                 $query11 = mysqli_query($conn, "SELECT * FROM test_investigator where recruitmentDate = '$recruitmentDate'");
                
                echo "<h6>".mysqli_num_rows($query10)." passed out of ".mysqli_num_rows($query11)." applicants</h6>";
                  ?>
              </div>
              <div class="card-footer">
                <strong><a href="cbi.php" class="small-box-footer black-text d-inline-block text-truncate" style="max-width: 130px;" >Background Investigation</a></strong>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6 col-md-4">
          <div class="inner">
            <?php 
                $query20 = mysqli_query($conn, "SELECT * from tests_status where test = 'Neuro-Psychiatric Examination and Drug Test'");
                $row1 = mysqli_fetch_assoc($query20);
                  if ($row1['status'] == 'ON-GOING'){
                    $neurocolor = 'primary-color';
                  } elseif ($row1['status'] == 'DONE'){
                    $neurocolor = 'success-color';
                  } elseif ($row1['status'] == 'START'){
                    $neurocolor = 'white';
                  }
              ?>
            <div class="card mb-4 p-3 <?php echo $neurocolor; ?>">
              <div class="inner">
                 <?php
                 $query12 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_neuro on applicants_pds.id = test_neuro.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate'");

                 $query13 = mysqli_query($conn, "SELECT * FROM test_neuro where recruitmentDate = '$recruitmentDate'");
                
                  echo "<h6>".mysqli_num_rows($query12)." passed out of ".mysqli_num_rows($query13)." applicants</h6>";
                  ?>
              </div>
              <div class="card-footer">
                <strong><a href="neuro.php" class="small-box-footer black-text d-inline-block text-truncate" style="max-width: 130px;" >Neuro-Psychiatric and Drug Test</a></strong>
              </div>
            </div>
          </div>
        </div>


      </div><!--row-->





    

    </div>
  </main>
  <!--Main laypassed out-->

<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>

</body>

</html>

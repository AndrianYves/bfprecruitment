<?php
$pageTitle = "Final Results";
$page      = "finalresults.php";
$title     = "Final Results";

$current = "Final Results";
?>
<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>


<body class="grey lighten-3">
<?php
  if(strtotime($today) < strtotime($submissiondate) && $submissionstatus == 'START'){
    $_SESSION['error'][] = 'Submission of form still ongoing.';
    header('location: home.php');
  }

  if($submissionstatus == 'PAUSED'){
    $_SESSION['error'][] = 'Submission of form still ongoing but temporarily paused.';
    header('location: home.php');
  }

?>
<?php
  if (isset($_POST['donerecruitment'])) {
    $result = $conn->prepare("UPDATE tests_status SET status='DONE' WHERE `test`='Recruitment'");
    $result->execute();
    $result->close();

    $_SESSION['success'] = 'Recruitment FINISH. You can now start a new one.';
  }
?>

<?php include 'includes/nav.php'; ?>
<?php if ($role == 'HR Officer'): ?>
<?php
    $result1 = $conn -> prepare("SELECT status FROM `tests_status` where `test` = 'Neuro-Psychiatric Examination and Drug Test'");
    $result1->execute();
    $query = $result1->get_result();
    $row = $query->fetch_assoc();
    $testStatus = $row['status'];
    $result1->close();

  // $result1 = mysqli_query($conn, "SELECT status FROM `tests_status` where `test` = 'Neuro-Psychiatric Examination and Drug Test'");
  // $row = mysqli_fetch_assoc($result1);
  // $testStatus = $row['status'];

  switch ($testStatus): 
?>
<?php case "DONE": ?>
<!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">


        <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fas fa-warning'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>

      <?php
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fas fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>

    <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Final Results</span>
          </h4>
        </div>
      </div>
      <!-- Heading -->

 <div class="row">
        <div class="col-md-12">
          <div class="card">
            <!--Card content-->
            <div class="card-body">
              <table class="table table-bordered table-responsive display" cellspacing="0" width="100%">
                <thead>
                  <th width="20">Rankings</th>
                  <th width="20">Applicant Number</th>
                  <th width="200">Full Name</th>
                  <th width="20">Written Exam Score</th>
                  <th width="20">Physical Agility Score</th>
                  <th width="20">Panel Interview Score</th>
                  <th width="20">Overall Performance</th>
                  <th width="20">View Profile</th>                </thead>
                <tbody>

                  <?php
                   // $result1 = mysqli_query($conn, "SELECT *, applicants_pds.id as evalID FROM `test_evaluation` join applicants_pds on applicants_pds.id = test_evaluation.applicantID where `status` = 'PASSED' and verified = '1' and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");
                  $rank = 1;
                    $result1 = mysqli_query($conn, "SELECT *, applicants_pds.applicantnumber as evalID, test_ppt.score as writtenscore, test_pat.total as agilityscore, test_interview.total as interviewscore, ((test_ppt.score * .4) + (test_pat.total * .3) + (test_interview.total * .3)) as overall FROM `applicants_pds` join test_ppt on applicants_pds.id = test_ppt.applicantID join test_pat on applicants_pds.id = test_pat.applicantID join test_interview on applicants_pds.id = test_interview.applicantID join test_neuro on applicants_pds.id = test_neuro.applicantID where applicants_pds.recruitmentDate = '$recruitmentDate' and test_neuro.status = 'PASSED' order by overall DESC");

                    while ($row = mysqli_fetch_array($result1)) {
                        
                  ?>
                        <tr>
                          <td><?php echo $rank;?></td>
                          <td><?php echo $row['evalID'];?></td>
                          <td><?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?></td>
                          <td><?php echo $row['writtenscore'];?></td>
                          <td><?php echo $row['agilityscore'];?></td>
                          <td><?php echo $row['interviewscore'];?></td>
                          <td><?php echo $row['overall'];?></td>
                          <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                        </tr>

            <?php $rank++; } ?>

                
                </tbody>
              </table>

                              <div class="row">
                  <div class="col-6">
                    <form method="POST" action="export.php" enctype="multipart/form-data">
                     <button type="submit" name="exportfinalresults" class="btn btn-success"><i class='fas fa-download'></i> Generate CSV </button>
                    </form>
                  </div>
                  <?php 
                  $query = mysqli_query($conn, "SELECT * from tests_status where status='DONE' and `test`='Recruitment'");

                  ?>
                  <div class="col-6 text-right">
                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalLoginForm" <?php if(mysqli_num_rows($query) == '1'){ echo 'hidden';}?>>End Recruitment</button>
                  </div>
                </div>



            </div>

          </div>
        </div>
      </div>

  </div><!--/container-->
</main><!--/Main layout-->

<!-- Are you sure -->
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Are you sure?</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <img src="logo.png" class="center mb-3 animated rotateIn rounded-circle" style="width: 150px; height: 150px;">
          <p class="text-center">This button, when clicked, will end the recruitment process.<br>
                  and will be able to view the final results of this recruitment batch.
          </p>

        </div>
      </div>

      <!--Footer-->
    <form method="POST" action="finalresults.php" enctype="multipart/form-data" class="needs-validation" novalidate>
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</a>
        <button class="btn btn-primary waves-effect" name="donerecruitment" id="changestatus"><i class="fa fa-check-square-o"></i>Yes</button>
      </div>
    </form>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<?php break; ?>
<?php default: ?>
<!--Main layout-->
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-info alert-dismissible fade show' role='alert'>
    <h4>Neuro-Psychiatric Examination and Drug Test not yet finished!</h4>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->
<?php break; ?>
<?php endswitch; ?>

<?php else: ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-danger alert-dismissible fade show' role='alert'>
    <h4>Forbidden.</h4>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->


<?php endif ?>
<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>

</body>
</html>
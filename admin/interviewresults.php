<?php
$title     = "Panel Interviewer";

$current = "Panel Interviewer";
?>

<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<?php
  if(strtotime($today) < strtotime($submissiondate) && $submissionstatus == 'START'){
    $_SESSION['error'][] = 'Submission of form still ongoing.';
    header('location: home.php');
  }

  if($submissionstatus == 'PAUSED'){
    $_SESSION['error'][] = 'Submission of form still ongoing but temporarily paused.';
    header('location: home.php');
  }

  $result9 = mysqli_query($conn, "SELECT status FROM `tests_status` where `test` = 'Panel Interview'");
  $row9 = mysqli_fetch_assoc($result9);
  $testStatus = $row9['status'];

  if($testStatus != 'DONE'){
    $_SESSION['error'][] = 'Panel Interview not yet finished.';
    header('location: interview.php');
  }

  $scoressql = $conn->prepare("SELECT panelinterviewpassingscore FROM settings where id = ?");
  $scoressql->bind_param("s", $id);
  $id = '1';
  $scoressql->execute();
  $scorequery = $scoressql->get_result();
  $row1 = $scorequery->fetch_assoc();
  $passingScore = $row1['panelinterviewpassingscore'];

  
 function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
 }

  if (isset($_POST['edit'])) {
    mysqli_autocommit($conn, false);
    $timestamp = date("Y-m-d H:i:s");
    $allowed = array('PASSED', 'FAILED');
    $error = false;

    $adminusername=$_POST['adminusername'];
    // $sql1 = mysqli_query($db, "SELECT * FROM admins where `id` = '$id'");
    // $row = mysqli_fetch_assoc($sql1);
    $password=$_POST['password'];

    $sql1 = $conn->prepare("SELECT * FROM admins where username = ?");
    $sql1->bind_param("s", $adminusername);
    $sql1->execute();
    $query = $sql1->get_result();
    $row = mysqli_fetch_assoc($query);

    if($query->num_rows < 1){
      $error = true;
      $_SESSION['error'][] = 'Invalid Username/Password';
    } else {
        if (!password_verify($password, $row['password'])) {
          $error = true;
          $_SESSION['error'][] = 'Invalid Username/Password';
        } 


    }

    $total = $potential = $psychosocial = $personality = "";

    if(empty($_POST['epersonality'])) {
      $error = true;
      $_SESSION['error'][] = 'Personality entered required.';
    }else if(!is_numeric($_POST['epersonality'])) {
      $error = true;
      $_SESSION['error'][] = 'Personality entered was not numeric.';
    } else {
      $personality = test_input($_POST['epersonality']);
    } 

    if(empty($_POST['epsychosocial'])) {
      $error = true;
      $_SESSION['error'][] = 'Psychosocial entered required.';
    }else if(!is_numeric($_POST['epsychosocial'])) {
      $error = true;
      $_SESSION['error'][] = 'Psychosocial entered was not numeric.';
    } else {
      $psychosocial = test_input($_POST['epsychosocial']);
    } 

    if(empty($_POST['epotential'])) {
      $error = true;
      $_SESSION['error'][] = 'Potential entered required.';
    } else if(!is_numeric($_POST['epotential'])) {
      $error = true;
      $_SESSION['error'][] = 'Potential entered was not numeric.';
    } else {
      $potential = test_input($_POST['epotential']);
    } 

    if(!empty($_POST['eremarks'])) {
      $remarks = test_input($_POST['eremarks']);
    } 

    if (empty($_POST['applicantID'])) {
      $error = true;
      $_SESSION['error'][] = 'Error. Somethings missing.';
    } else {
      $applicantID = $_POST['applicantID'];
    }

    $total = $potential + $psychosocial + $personality;

    if($total >= $passingScore){
      $status = 'PASSED';
      $result = $conn->prepare("INSERT INTO test_investigator(recruitmentDate, applicantID) VALUES(?, ?)");
      $result->bind_param("si", $recruitmentDate, $applicantID);
      $result->execute();
    } else {
      $status = 'FAILED';
      $result = $conn->prepare("DELETE FROM test_investigator WHERE applicantID = ?");
      $result->bind_param("i", $applicantID);
      $result->execute();
      $result->close();

      $result = $conn->prepare("DELETE FROM test_neuro WHERE applicantID = ?");
      $result->bind_param("i", $applicantID);
      $result->execute();
      $result->close();
    }

    $result1 = $conn->prepare("UPDATE test_interview SET potential = ?, psychosocial = ?, personality = ?, total = ?, remarks = ?, status = ?, timestamp = ?, adminID = ? WHERE applicantID = ?");
    $result1->bind_param("iiiisssii", $potential, $psychosocial, $personality, $total, $remarks, $status, $timestamp, $adminID, $applicantID);
    $result1->execute();

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Status Updated';
    } else {
      mysqli_rollback($conn);
    }

  }
?>
<body class="grey lighten-3">
  <?php include 'includes/nav.php'; ?>

<?php if ($role == 'Panel Interviewer' || $role == 'HR Officer'): ?>

<!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
      <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fas fa-exclamation-triangle'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>

      <?php
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fas fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
<?php 
$result1 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_interview on applicants_pds.id = test_interview.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate'");

$sql2 = mysqli_query($conn, "SELECT * FROM `test_interview` join applicants_pds on applicants_pds.id = test_interview.applicantID where applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");

?>
    <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Results</span>
          </h4>
         <h4 class="mb-2 mb-sm-0 pt-1 text-right"><?php echo mysqli_num_rows($result1);?> passed out of <?php echo mysqli_num_rows($sql2);?> applicants</h4>
        </div>
      </div>
      <!-- Heading -->

 <div class="row">
        <div class="col-md-12">
          <div class="card">
            <!--Card content-->
            <div class="card-body">
        <ul class="nav nav-pills mb-3 justify-content-end" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="btn-success nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
      aria-controls="pills-home" aria-selected="true">PASSED</a>
  </li>
  <li class="nav-item">
    <a class="btn-danger nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
      aria-controls="pills-profile" aria-selected="false">FAILED</a>
  </li>
  <li class="nav-item">
    <a class="btn-primary nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab"
      aria-controls="pills-contact" aria-selected="false">ALL</a>
  </li>
</ul>
<div class="tab-content pt-2 pl-1" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
    <table class="table table-bordered table-responsive display" cellspacing="0" width="100%">
                <thead>
                  <th width="50">Applicant Number</th>
                  <th width="150">Full Name</th>
                  <th width="50">Potential Trait (40 pts.)</th>
                  <th width="50">Psychosocial Attributes (30 pts.)</th>
                  <th width="50">Personality Traits (30 pts.)</th>
                  <th width="50">Total</th>
                  <th width="50">Remarks</th>
                  <th width="50">Status</th>
                  <th width="50">Last Edited</th>
                  <th width="50">View Profile</th>
                  <th width="50">Action</th>
                </thead>
                <tbody>

                  <?php
                    $result1 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_interview on applicants_pds.id = test_interview.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicantnumber");
                      while ($row = mysqli_fetch_array($result1)) {

                      ?>
                        <tr>
                          <td><?php echo $row['applicantnumber']; ?></td>
                          <td><?php echo ucwords($row['last']); ?>, <?php echo ucwords($row['name']); ?> <?php echo ucwords($row['middle']); ?></td>
                          <td><?php echo $row['potential']; ?></td>
                          <td><?php echo $row['psychosocial']; ?></td>
                          <td><?php echo $row['personality']; ?></td>
                          <td><?php echo $row['total']; ?></td>
                          <td><?php echo $row['remarks']; ?></td>
                          <td><h6><span class='badge badge-pill sm badge-success'><?php echo $row['status']; ?></span></h6></td>
                          <td><?php echo date("Y-m-d H:i", strtotime($row['timestamp']));?></td>
                          <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>

                           <td>
                           <div class='text-center'><a data-toggle='modal' data-target='#edit<?php echo $row['applicantID']; ?>' href='#edit?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-edit blue-text'></i></a></div>
                          </td>
                        </tr>
            <div class="modal fade" id="edit<?php echo $row['applicantID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold"><?php echo $row['applicantID'];?> - <?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?>
                    </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                    </button>
                  </div>
                  <div class="modal-body mx-3">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                      <div class="text-center">
                        <input type="hidden" name="applicantID" class="form-control validate" value="<?php echo $row['applicantID']; ?>" required>
                        
                                                 <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epotential" name="epotential" class="form-control validate" value="<?php echo $row['potential']; ?>" required>
                        <label for="potential">Potential Trait Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epsychosocial" name="epsychosocial" class="form-control validate" value="<?php echo $row['psychosocial']; ?>" required>
                        <label for="psychosocial">Psychosocial Attribute Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epersonality" name="epersonality" class="form-control validate" value="<?php echo $row['personality']; ?>" required>
                        <label for="birthplace">Personal Traits Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <textarea type="text" class="form-control rounded-0" id="eremarks" name="eremarks" rows="3" value="<?php echo $row['remarks']; ?>" required></textarea>
                        <label for="birthplace">Remarks</label>
                        </div>

                        <label>Super User Account</label>
                        <input type="text" name="adminusername" class="form-control mb-4 validate" placeholder="Username" required>
                            <input type="password" name="password" class="form-control mb-4" placeholder="Password" required>
                            
                                
                      </div>
                      <div class="modal-footer d-flex justify-content-center">
                        <button name="edit" class="btn btn-success">Update
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php    } ?>
                
                </tbody>
              </table>
  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        <table class="table table-bordered table-responsive display" cellspacing="0" width="100%">
                <thead>
                  <th width="50">Applicant Number</th>
                  <th width="150">Full Name</th>
                  <th width="50">Potential Trait (40 pts.)</th>
                  <th width="50">Psychosocial Attributes (30 pts.)</th>
                  <th width="50">Personality Traits (30 pts.)</th>
                  <th width="50">Total</th>
                  <th width="50">Remarks</th>
                  <th width="50">Status</th>
                  <th width="50">Last Edited</th>
                  <th width="50">View Profile</th>
                  <th width="50">Action</th>
                </thead>
                <tbody>

                  <?php
                    $result2 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_interview on applicants_pds.id = test_interview.applicantID where `status` = 'FAILED' and applicants_pds.recruitmentDate = '$recruitmentDate'");
                      while ($row = mysqli_fetch_array($result2)) {

                      ?>
                        <tr>
                          <td><?php echo $row['applicantnumber']; ?></td>
                          <td><?php echo ucwords($row['last']); ?>, <?php echo ucwords($row['name']); ?> <?php echo ucwords($row['middle']); ?></td>
                          <td><?php echo $row['potential']; ?></td>
                          <td><?php echo $row['psychosocial']; ?></td>
                          <td><?php echo $row['personality']; ?></td>
                          <td><?php echo $row['total']; ?></td>
                          <td><?php echo $row['remarks']; ?></td>
                          <td><h6><span class='badge badge-pill sm badge-danger'><?php echo $row['status']; ?></span></h6></td>
                          <td><?php echo date("Y-m-d H:i", strtotime($row['timestamp']));?></td>
                          <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>

                           <td>
                           <div class='text-center'><a data-toggle='modal' data-target='#FAILEDit<?php echo $row['applicantID']; ?>' href='#FAILEDit?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-edit blue-text'></i></a></div>
                          </td>
                        </tr>
            <div class="modal fade" id="FAILEDit<?php echo $row['applicantID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold"><?php echo $row['applicantID'];?> - <?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?>
                    </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                    </button>
                  </div>
                  <div class="modal-body mx-3">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                      <div class="text-center">
                        <input type="hidden" name="applicantID" class="form-control validate" value="<?php echo $row['applicantID']; ?>" required>
                        
                                                 <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epotential" name="epotential" class="form-control validate" value="<?php echo $row['potential']; ?>" required>
                        <label for="potential">Potential Trait Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epsychosocial" name="epsychosocial" class="form-control validate" value="<?php echo $row['psychosocial']; ?>" required>
                        <label for="psychosocial">Psychosocial Attribute Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epersonality" name="epersonality" class="form-control validate" value="<?php echo $row['personality']; ?>" required>
                        <label for="birthplace">Personal Traits Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <textarea type="text" class="form-control rounded-0" id="eremarks" name="eremarks" rows="3" value="<?php echo $row['remarks']; ?>" required></textarea>
                        <label for="birthplace">Remarks</label>
                        </div>
                        <label>Super User Account</label>
                        <input type="text" name="adminusername" class="form-control mb-4 validate" placeholder="Username" required>
                            <input type="password" name="password" class="form-control mb-4" placeholder="Password" required>
                            
                                
                      </div>
                      <div class="modal-footer d-flex justify-content-center">
                        <button name="edit" class="btn btn-success">Update
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php    } ?>
                </tbody>
              </table>
  </div>
  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
        <table class="table table-bordered table-responsive display" cellspacing="0" width="100%">
                <thead>
                  <th width="50">Applicant Number</th>
                  <th width="150">Full Name</th>
                  <th width="50">Potential Trait (40 pts.)</th>
                  <th width="50">Psychosocial Attributes (30 pts.)</th>
                  <th width="50">Personality Traits (30 pts.)</th>
                  <th width="50">Total</th>
                  <th width="50">Remarks</th>
                  <th width="50">Status</th>
                  <th width="50">Last Edited</th>
                  <th width="50">View Profile</th>
                  <th width="50">Action</th>
                </thead>
                <tbody>

                  <?php
                    $result2 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_interview on applicants_pds.id = test_interview.applicantID and applicants_pds.recruitmentDate = '$recruitmentDate'");
                      while ($row = mysqli_fetch_array($result2)) {
                      if ($row['status'] == 'PASSED'){
                        $color = 'success';
                      } else {
                        $color = 'danger';
                      }

                      ?>
                        <tr>
                          <td><?php echo $row['applicantnumber']; ?></td>
                          <td><?php echo ucwords($row['last']); ?>, <?php echo ucwords($row['name']); ?> <?php echo ucwords($row['middle']); ?></td>
                          <td><?php echo $row['potential']; ?></td>
                          <td><?php echo $row['psychosocial']; ?></td>
                          <td><?php echo $row['personality']; ?></td>
                          <td><?php echo $row['total']; ?></td>
                          <td><?php echo $row['remarks']; ?></td>
                          <td><h6><span class='badge badge-pill sm badge-<?php echo $color; ?>'><?php echo $row['status']; ?></h6></span></td>
                          <td><?php echo date("Y-m-d H:i", strtotime($row['timestamp']));?></td>
                          <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>

                           <td>
                           <div class='text-center'><a data-toggle='modal' data-target='#alledit<?php echo $row['applicantID']; ?>' href='#alledit?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-edit blue-text'></i></a></div>
                          </td>
                        </tr>
            <div class="modal fade" id="alledit<?php echo $row['applicantID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold"><?php echo $row['applicantID'];?> - <?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?>
                    </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                    </button>
                  </div>
                  <div class="modal-body mx-3">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                      <div class="text-center">
                        <input type="hidden" name="applicantID" class="form-control validate" value="<?php echo $row['applicantID']; ?>" required>
                        
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epotential" name="epotential" class="form-control validate" value="<?php echo $row['potential']; ?>" required>
                        <label for="potential">Potential Trait Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epsychosocial" name="epsychosocial" class="form-control validate" value="<?php echo $row['psychosocial']; ?>" required>
                        <label for="psychosocial">Psychosocial Attribute Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epersonality" name="epersonality" class="form-control validate" value="<?php echo $row['personality']; ?>" required>
                        <label for="birthplace">Personal Traits Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <textarea type="text" class="form-control rounded-0" id="eremarks" name="eremarks" rows="3" value="<?php echo $row['remarks']; ?>" required></textarea>
                        <label for="birthplace">Remarks</label>
                        </div>
                        <label>Super User Account</label>
                        <input type="text" name="adminusername" class="form-control mb-4 validate" placeholder="Username" required>
                            <input type="password" name="password" class="form-control mb-4" placeholder="Password" required>
                            
                                
                      </div>
                      <div class="modal-footer d-flex justify-content-center">
                        <button name="edit" class="btn btn-success">Update
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php    } ?>
                
                </tbody>
              </table>
  </div>
</div>
      <form method="POST" action="export.php" enctype="multipart/form-data">

 <button type="submit" name="exportinterviewresults" class="btn btn-success"><i class='fas fa-download'></i> Generate CSV
                        </button>
</form>


              


            </div>

          </div>
        </div>
      </div>

  </div><!--/container-->
</main><!--/Main layout-->

<?php else: ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-danger alert-dismissible fade show' role='alert'>
    <h4>Forbidden.</h4>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->


<?php endif ?>
<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>
<script type="text/javascript">
  $('#selectAll').click(function (e) {
    $(this).closest('table').find('th input:checkbox').prop('checked', this.checked);
});
</script>
<script type="text/javascript">
  $('#selectAll1').click(function (e) {
    $(this).closest('table').find('th input:checkbox').prop('checked', this.checked);
});
</script>
<script type="text/javascript">
  $('#selectAll2').click(function (e) {
    $(this).closest('table').find('th input:checkbox').prop('checked', this.checked);
});
</script>
</body>
</html>
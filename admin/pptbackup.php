<?php
$page      = "ppt.php";
$title     = "Written Exam Handler";

$current = "Written Exam Handler";
?>

<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<?php
  if(strtotime($today) < strtotime($submissiondate) && $submissionstatus == 'START'){
    $_SESSION['error'][] = 'Submission of form still ongoing.';
    header('location: home.php');
  }

  if($submissionstatus == 'PAUSED'){
    $_SESSION['error'][] = 'Submission of form still ongoing but paused.';
    header('location: home.php');
  }

  $scoressql = $conn->prepare("SELECT writterexampassingscore, writtenexamtotalscore FROM settings where id = ?");
  $scoressql->bind_param("s", $id);
  $id = '1';
  $scoressql->execute();
  $scorequery = $scoressql->get_result();
  $row1 = $scorequery->fetch_assoc();
  $passingScore = $row1['writterexampassingscore'];
  $maxScore = $row1['writtenexamtotalscore'];

  if(isset($_POST['submit'])){ 
      mysqli_autocommit($conn, false);
      $adminID=$_POST['adminid'];
      $timestamp = date("Y-m-d H:i:s");
      $error = false;

        $score = "";

    $number = count($_POST["score"]);
    for($i=0; $i<$number; $i++) {
      if(trim($_POST["score"][$i] != '')) {
          if(!is_numeric($_POST["score"][$i])) {
            $error = true;
            $_SESSION['error'][] = 'Score entered was not numeric.';
          } else {
            $score = $_POST["score"][$i];
          }

        $applicantID = mysqli_real_escape_string($conn, $_POST["appid"][$i]);

        if ($score > $maxScore){
          $error = true;
          $_SESSION['error'][] = 'Maximum score is '.$maxScore.'';
        } else {
          if ($score >= $passingScore) {
            $status = 'PASSED';
            $result = $conn->prepare("INSERT INTO test_pat(recruitmentDate, applicantID) VALUES(?, ?)");
            $result->bind_param("si", $recruitmentDate, $applicantID);
            $result->execute();
          } else {
            $status = 'FAILED';
          }

          $result1 = $conn->prepare("UPDATE test_ppt SET score = ?, status = ?, timestamp = ? WHERE applicantID = ?");
          $result1->bind_param("issi", $score, $status, $timestamp, $applicantID);
          $result1->execute();
        }
      }
    }


    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Scores Submitted';
    } else {
      mysqli_rollback($conn);
    }

  }

?>
<?php
  if (isset($_POST['edit'])) {
    mysqli_autocommit($conn, false);
    $timestamp = date("Y-m-d H:i:s");
    $allowed = array('PASSED', 'FAILED');
    $error = false;

    $applicantID = $_POST['applicantID'];
    $score = $_POST['score'];

    if ($score > $maxScore){
      $error = true;
      $_SESSION['error'][] = 'Maximum score is '.$maxScore.'';
    } else {
      if ($score >= $passingScore) {
        $status = 'PASSED';
        $result = $conn->prepare("INSERT INTO test_pat(recruitmentDate, applicantID) VALUES(?, ?)");
        $result->bind_param("si", $recruitmentDate, $applicantID);
        $result->execute();
      } else {
        $status = 'FAILED';
        $result = $conn->prepare("DELETE FROM test_pat WHERE applicantID = ?");
        $result->bind_param("i", $applicantID);
        $result->execute();
      }

      $result1 = $conn->prepare("UPDATE test_ppt SET score = ?, status = ?, timestamp = ? WHERE applicantID = ?");
      $result1->bind_param("issi", $score, $status, $timestamp, $applicantID);
      $result1->execute();
        
    }

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Scores Updated';
    } else {
      mysqli_rollback($conn);
    }


  }
?>
<?php
  if (isset($_POST['changestatus'])) {
    $result1 = mysqli_query($conn,"UPDATE tests_status SET status='DONE' WHERE `test`='Pen and Paper Test'");
    $result1 = mysqli_query($conn,"UPDATE tests_status SET status='ON-GOING' WHERE `test`='Physical Agility Test'");
    header('location: ppt.php');
  }
?>

<body class="grey lighten-3">
  <?php include 'includes/nav.php'; ?>

<?php if ($role == 'Written Exam Handler' || $role == 'HR Officer'): ?>
<?php
  $result1 = mysqli_query($conn, "SELECT status FROM `tests_status` where `test` = 'Pen and Paper Test'");
  $row = mysqli_fetch_assoc($result1);
  $testStatus = $row['status'];

  switch ($testStatus): 

?>
<?php case "ON-GOING": ?>
<!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">

        <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fas fa-warning'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>

      <?php
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fas fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>

<?php 
  $result2 = mysqli_query($conn, "SELECT * FROM `test_ppt` join applicants_pds on applicants_pds.id = test_ppt.applicantID where `status` is not null and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");

  $sql = mysqli_query($conn, "SELECT * FROM `test_ppt` join applicants_pds on applicants_pds.id = test_ppt.applicantID where applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");

?>
    <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Pen and Paper Test</span>
          </h4>
           <h4 class="mb-2 mb-sm-0 pt-1 text-right">Processed <?php echo mysqli_num_rows($result2);?> out of <?php echo mysqli_num_rows($sql);?> applicants</h4>
        </div>
      </div>
      <!-- Heading -->

 <div class="row">
        <div class="col-md-12">
          <div class="card">
            <!--Card content-->
            <div class="card-body">
              <h5 class="float-left">Passing score: <?php echo $passingScore; ?><br> Maximum score: <?php echo $maxScore; ?></h5>
            <form method="POST" action="ppt.php">
               <ul class="nav nav-pills mb-3 justify-content-end" id="pills-tab" role="tablist">

              <li class="nav-item">
              <a class="btn-primary nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
              aria-controls="pills-home" aria-selected="true">ALL</a>
              </li>
              <li class="nav-item">
              <a class="btn-info nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
              aria-controls="pills-profile" aria-selected="false">Finished</a>
              </li>
            </ul>
            <div class="tab-content pt-2 pl-1" id="pills-tabContent">
              <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
              <table class="table table-bordered table-responsive-md display" cellspacing="0" width="100%">
                <thead>
                  <th width="20">Applicant Number</th>
                  <th width="200">Full Name</th>
                  <th width="20">View</th>
                  <th width="20">Score</th>
                </thead>
                <tbody>

                  <?php
                    $result1 = mysqli_query($conn, "SELECT * FROM `test_ppt` join applicants_pds on applicants_pds.id = test_ppt.applicantID where `status` is NULL  and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");
                    $newStatus = mysqli_num_rows($result1);
                    while ($row = mysqli_fetch_array($result1)) {
                        $input = '<input type="number" id="score" name="score[]" class="form-control mb-4" placeholder="Score">
                        <input class="form-check-input" type="hidden" name="appid[]" id="'.$row['applicantID'].'" value="'.$row['applicantID'].'" style="visibility: hidden;">
                        <input class="form-check-input" type="hidden" name="adminid" id="adminid" value="'.$user['id'].'" style="visibility: hidden;">';

                      echo "
                        <tr>
                          <td>".$row['applicantnumber']."</td>
                          <td>".ucwords($row['last']).", ".ucwords($row['name'])." ".ucwords($row['middle'])."</td>
                          <td><a href='evaluationview.php?id=$row[applicantID]'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                          <td>".$input."</td>
                        </tr>
                      ";
                    }
                  ?>
                
                </tbody>
              </table>
              </div>
              <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        <table class="table table-bordered display table-responsive-md" cellspacing="0" width="100%">
                <thead>
                  <th width="5">Applicant Number</th>
                  <th width="500">Full Name</th>
                  <th width="20">Score</th>
                  <th width="20">Status</th>
                  <th width="20">View</th>
                  <th width="20">Action</th>
                </thead>
                <tbody>

                  <?php

                    while ($row = mysqli_fetch_array($result2)) {
                    if ($row['status'] == 'PASSED'){
                        $color = 'success';
                      } else {
                        $color = 'danger';
                      }
                      ?>
                        <tr>
                          <td><?php echo $row['applicantnumber'];?></td>
                          <td><?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?></td>
                          <td><?php echo $row['score'];?>/<?php echo $maxScore;?></td>
                          <td><span class='badge badge-pill badge-<?php echo $color?>'><?php echo $row['status'];?></span></td>
                          <td><a href="evaluationview.php?id=<?php echo $row['applicantID']; ?>"><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                          <td>
                           <div class='text-center'><a data-toggle='modal' data-target='#edit<?php echo $row['applicantID']; ?>' href='#edit?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-edit blue-text'></i></a></div>
                          </td>
                        </tr>
            <div class="modal fade" id="edit<?php echo $row['applicantID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold"><?php echo $row['applicantID'];?> - <?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?>
                    </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                    </button>
                  </div>
                  <div class="modal-body mx-3">
                    <form method="POST" action="ppt.php" enctype="multipart/form-data" class="needs-validation" novalidate>
                      <div class="text-center">
                        <input type="hidden" id="id" name="applicantID" class="form-control validate" value="<?php echo $row['applicantID']; ?>" required>
                        <input type="number" id="score" name="score" class="form-control validate" value="<?php echo $row['score']; ?>" required>


                      </div>
                      <div class="modal-footer d-flex justify-content-center">
                        <button name="edit" class="btn btn-success">Update
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php    } ?>

                </tbody>
              </table>
  </div>
</div>




                            <div class="row">
                <div class="col-6">
                <button class="btn btn-outline-success waves-effect" name="submit" id="submit"><i class="far fa-save pr-2" aria-hidden="true"></i>Save</button></form>
                </div>
                <div class="col-6 text-right">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalLoginForm" <?php if($newStatus > 0){ echo 'disabled'; }?>>Finalize</button>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

  </div><!--/container-->
</main><!--/Main layout-->

<!-- Are you sure -->
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Are you sure?</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <img src="logo.png" class="center mb-3 animated rotateIn rounded-circle" style="width: 150px; height: 150px;">
          <p class="text-center">Please double check the the assessment before submitting to avoid unncesseary mistakes.<br>
                  Once submitted will be the final result of the written exam.
          </p>

        </div>
      </div>

      <!--Footer-->
      <form method="POST" action="ppt.php" enctype="multipart/form-data" class="needs-validation" novalidate>
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</a>
        <button class="btn btn-primary waves-effect" name="changestatus" id="changestatus"><i class="fa fa-check-square-o"></i>Yes</button>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<?php break; ?>
<?php case "DONE": ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-success alert-dismissible fade show' role='alert'>
    <h4><i class='icon fa fa-check'></i> Done!</h4>
    <a href='pptresults.php' class='alert-link'>View Results</a>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->
<?php break; ?>
<?php default: ?>

<!--Main layout-->
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-info alert-dismissible fade show' role='alert'>
    <h4>Medical Exam not yet finished!</h4>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->

<?php endswitch; ?>
<?php else: ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-danger alert-dismissible fade show' role='alert'>
    <h4>Forbidden.</h4>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->


<?php endif ?>

<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>

</body>
</html>
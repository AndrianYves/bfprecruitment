<?php
  $page      = "evaluation.php";
  $title     = "Applicants";
  $current = "Applicants";
  ?>
<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="grey lighten-3">
  <?php include 'includes/nav.php'; ?>
  <?php if ($role == 'HR Officer'): ?>
  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Applicants</span>
          </h4>
          <h4 class="mb-2 mb-sm-0 pt-1"><a class="text-right btn btn-info btn-md"href="allapplicants.php">View all current applicants</a></h4>
        </div>
      </div>


      <div class="row">
        <div class="col-md-12">
          <div class="card">
          <!--Card content-->
          <div class="card-body">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
              <li class="nav-item">
              <a class="btn-sm btn-primary nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
              aria-controls="pills-home" aria-selected="true">Current Recruitment</a>
              </li>
              <li class="nav-item">
              <a class="btn-sm btn-info nav-link" id="prev-home-tab" data-toggle="pill" href="#prev-home" role="tab"
              aria-controls="prev-home" aria-selected="true">Previous Recruitment</a>
              </li>
              <li class="nav-item">
              <a class="btn-sm btn-info nav-link" id="hired-home-tab" data-toggle="pill" href="#hired-home" role="tab"
              aria-controls="hired-home" aria-selected="true">Passers</a>
              </li>
              <li class="nav-item">
              <a class="btn-sm btn-info nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
              aria-controls="pills-profile" aria-selected="false">Medical Failures</a>
              </li>
              <li class="nav-item">
              <a class="btn-sm btn-info nav-link" id="neuro-profile-tab" data-toggle="pill" href="#neuro-profile" role="tab"
              aria-controls="neuro-profile" aria-selected="false">Neuro-Psychiatric and Drug Test Failures</a>
              </li>
            </ul>
            <div class="tab-content pt-2 pl-1" id="pills-tabContent">
              <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <table class="table table-bordered table-responsive-md display" cellspacing="0" width="100%">
                <thead>
                  <th width="200">Full Name</th>
                  <th width="50">Medical, Dental and Physical Examination</th>
                  <th width="50">Pen and Paper Test</th>
                  <th width="50">Physical Agility Test</th>
                  <th width="50">Panel Interview</th>
                  <th width="50">Background Investigation</th>
                  <th width="50">Neuro-Psychiatric and Drug Test</th>
                  <th width="20">View Profile</th>
                </thead>
                <tbody>
                  <?php
                    $result1 = mysqli_query($conn, "SELECT *, applicants_pds.id as evalID, test_ppt.score as writtenscore, test_pat.total as agilityscore, test_interview.total as interviewscore, ((test_ppt.score * .4) + (test_pat.total * .3) + (test_interview.total * .3)) as overall FROM `applicants_pds` left join test_ppt on applicants_pds.id = test_ppt.applicantID left join test_pat on applicants_pds.id = test_pat.applicantID left join test_interview on applicants_pds.id = test_interview.applicantID join test_evaluation on test_evaluation.applicantID = applicants_pds.id where test_evaluation.status = 'Accepted' and applicants_pds.recruitmentDate = '$recruitmentDate' order by overall ASC");
                    $newStatus = mysqli_num_rows($result1);
                    while ($row = mysqli_fetch_array($result1)) {
                              $query1 = mysqli_query($conn, "SELECT * FROM `test_med` where test_med.applicantID = '".$row['id']."'");
                              $row1 = mysqli_fetch_assoc($query1);
                                if ($row1['status'] == 'PASSED'){
                                    $color = 'success';
                                } else {
                                    $color = 'danger';
                                }
                                $status1 = '<h6><span class="badge badge-pill badge-'.$color.'">'.$row1['status'].'</span></h6>';
                              ?>
                            <?php
                              $query2 = mysqli_query($conn, "SELECT * FROM `test_ppt` where test_ppt.applicantID = '".$row['id']."'");
                              $row2 = mysqli_fetch_assoc($query2);
                                if ($row2['status'] == 'PASSED'){
                                    $color = 'success';
                                } else {
                                    $color = 'danger';
                                }
                              $status2 = '<h6><span class="badge badge-pill badge-'.$color.'">'.$row2['status'].'</span></h6>';
                              ?>
                            <?php
                              $query3 = mysqli_query($conn, "SELECT * FROM `test_pat` where test_pat.applicantID = '".$row['id']."'");
                              $row3 = mysqli_fetch_assoc($query3);
                                            if ($row3['status'] == 'PASSED'){
                                    $color = 'success';
                                } else {
                                    $color = 'danger';
                                }
                              $status3 = '<h6><span class="badge badge-pill badge-'.$color.'">'.$row3['status'].'</span></h6>';
                              ?>
                            <?php
                              $query4 = mysqli_query($conn, "SELECT * FROM `test_interview` join applicants_pds on applicants_pds.id = test_interview.applicantID where test_interview.applicantID = '".$row['id']."'");
                              $row4 = mysqli_fetch_assoc($query4);
                                            if ($row4['status'] == 'PASSED'){
                                    $color = 'success';
                                } else {
                                    $color = 'danger';
                                }
                              $status4 = '<h6><span class="badge badge-pill badge-'.$color.'">'.$row4['status'].'</span></h6>';
                              ?>
                            <?php
                              $query5 = mysqli_query($conn, "SELECT * FROM `test_investigator` join applicants_pds on applicants_pds.id = test_investigator.applicantID where test_investigator.applicantID = '".$row['id']."'");
                              $row5 = mysqli_fetch_assoc($query5);
                                if ($row5['status'] == 'PASSED'){
                                    $color = 'success';
                                } else {
                                    $color = 'danger';
                                }
                              $status5 = '<h6><span class="badge badge-pill badge-'.$color.'">'.$row5['status'].'</span></h6>';
                              ?>
                            <?php
                              $query6 = mysqli_query($conn, "SELECT * FROM `test_neuro` join applicants_pds on applicants_pds.id = test_neuro.applicantID where test_neuro.applicantID = '".$row['id']."'");
                              $row6 = mysqli_fetch_assoc($query6);
                                if ($row6['status'] == 'PASSED'){
                                    $color = 'success';
                                } else {
                                    $color = 'danger';
                                }
                              $status6 = '<h6><span class="badge badge-pill badge-'.$color.'">'.$row6['status'].'</span></h6>';
                      ?>
                  <tr>
                    <td><?php echo ucwords($row['last']); ?>, <?php echo ucwords($row['name']); ?> <?php echo ucwords($row['middle']); ?></td>
                   <td><?php echo $status1; ?></td>
                   <td><?php echo $status2; ?></td>
                   <td><?php echo $status3; ?></td>
                   <td><?php echo $status4; ?></td>
                   <td><?php echo $status5; ?></td>
                   <td><?php echo $status6; ?></td>
                    <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
<!--               <form method="POST" action="export.php" enctype="multipart/form-data">

 <button type="submit" name="currentapplicant" class="btn btn-success"><i class='fas fa-download'></i> Generate CSV
                        </button>
</form> -->
            </div>

              <div class="tab-pane fade" id="prev-home" role="tabpanel" aria-labelledby="prev-home-tab">
          <table class="table table-bordered table-responsive-md display" cellspacing="0" width="100%">
                <thead>
                  <th width="100">Date of Recruitment</th>
                  <th width="100">View Applicants</th>
                </thead>
                <tbody>
                  <?php
                    $result1 = mysqli_query($conn, "SELECT * FROM applicants_pds where recruitmentDate != '$recruitmentDate' group by recruitmentDate");
                    $newStatus = mysqli_num_rows($result1);
                    while ($row = mysqli_fetch_array($result1)) {?>
                  <tr>
  
                    <td><?php echo date("F d, Y", strtotime($row['recruitmentDate'])); ?></td>
                    <td><a href='prevrecruitment.php?recruitmentDate=<?php echo $row['recruitmentDate']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                  
                  <?php } ?>
                </tbody>
              </table>

  </div>

    <div class="tab-pane fade" id="hired-home" role="tabpanel" aria-labelledby="hired-home-tab">
          <table class="table table-bordered table-responsive-md display" cellspacing="0" width="100%">
                <thead>
                  <th width="200">Full Name</th>
                  <th width="100">Date Registered</th>
                  <th width="50">View Profile</th>
                </thead>
                <tbody>
                  <?php
                    $result1 = mysqli_query($conn, "SELECT * FROM applicants_pds join test_neuro on applicants_pds.id = test_neuro.applicantID where verified = '1' and status = 'PASSED'");
                    $newStatus = mysqli_num_rows($result1);
                    while ($row = mysqli_fetch_array($result1)) {?>
                  <tr>
                    <td><?php echo ucwords($row['last']); ?>, <?php echo ucwords($row['name']); ?> <?php echo ucwords($row['middle']); ?></td>
                    <td><?php echo date("F d, Y", strtotime($row['recruitmentDate'])); ?></td>
                    <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                  
                  <?php } ?>
                </tbody>
              </table>
  </div>

  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
          <table class="table table-bordered table-responsive-md display" cellspacing="0" width="100%">
                <thead>
                  <th width="200">Full Name</th>
                  <th width="100">Date Registered</th>
                  <th width="50">View Profile</th>
                </thead>
                <tbody>
                  <?php
                    $result1 = mysqli_query($conn, "SELECT * FROM applicants_pds join test_med on applicants_pds.id = test_med.applicantID where verified = '1' and status = 'FAILED'");
                    $newStatus = mysqli_num_rows($result1);
                    while ($row = mysqli_fetch_array($result1)) {?>
                  <tr>
                    <td><?php echo ucwords($row['last']); ?>, <?php echo ucwords($row['name']); ?> <?php echo ucwords($row['middle']); ?></td>
                    <td><?php echo date("F d, Y", strtotime($row['recruitmentDate'])); ?></td>
                    <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                  
                  <?php } ?>
                </tbody>
              </table>
  </div>

  <div class="tab-pane fade" id="neuro-profile" role="tabpanel" aria-labelledby="neuro-profile-tab">
        <table class="table table-bordered table-responsive-md display" cellspacing="0" width="100%">
                <thead>
                  <th width="200">Full Name</th>
                  <th width="100">Date Registered</th>
                  <th width="50">View Profile</th>
                </thead>
                <tbody>
                  <?php
                    $result1 = mysqli_query($conn, "SELECT * FROM applicants_pds join test_neuro on applicants_pds.id = test_neuro.applicantID where verified = '1' and status = 'FAILED'");
                    $newStatus = mysqli_num_rows($result1);
                    while ($row = mysqli_fetch_array($result1)) {?>
                  <tr>
                    <td><?php echo ucwords($row['last']); ?>, <?php echo ucwords($row['name']); ?> <?php echo ucwords($row['middle']); ?></td>
                    <td><?php echo date("F d, Y", strtotime($row['recruitmentDate'])); ?></td>
                    <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
              
                  <?php } ?>
                </tbody>
              </table>
  </div>
</div>

        
            </div>

          </div>
        </div>
      </div>



    </div><!--/container-->
  </main>
  <!--/Main layout-->
  <?php else: ?>
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
      <div class='alert alert-danger alert-dismissible fade show' role='alert'>
        <h4>Forbidden.</h4>
      </div>
    </div>
    <!--/container-->
  </main>
  <!--/Main layout-->
  <?php endif ?>
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/scripts.php'; ?>
</body>
</html>


<?php
  $page      = "evaluation.php";
  $title     = "Applicants";
  $current = "Applicants";
  ?>
<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="grey lighten-3">
  <?php include 'includes/nav.php'; 
  $_SESSION['previous'] = "javascript:history.go(-1)";
  if(isset($_SERVER['HTTP_REFERER'])) {
    $_SESSION['previous'] = $_SERVER['HTTP_REFERER'];
}?>
  <?php if ($role == 'HR Officer'): ?>
  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">

        <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fas fa-warning'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>

      <?php
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fas fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Current Applicants</span>
          </h4>

          <h4 class="mb-2 mb-sm-0 pt-1"><a class="text-right" href="<?= $_SESSION['previous'] ?>">Back</a></h4>
        </div>
      </div>


      <div class="row">
        <div class="col-md-12">
          <div class="card">
          <!--Card content-->
          <div class="card-body">
                <table  class="table table-bordered table-responsive display table-sm" cellspacing="0" width="100%">
                <thead>
                  <th width="200">Action</th>
                  <th width="50">Verification</th>
                  <th width="200">Full Name</th>
                  <th width="50">Birthday</th>
                  <th width="50">Age</th>
                  <th width="50">Sex</th>
                  <th width="50">Birthplace</th>
                  <th width="50">Address</th>
                  <th width="50">Civil Status</th>
                  <th width="50">Skills</th>
                  <th width="50">Contact</th>
                  <th width="50">Email</th>
                  <th width="50">Social Media</th>
                </thead>
                <tbody>
                  <?php
                    $result1 = mysqli_query($conn, "SELECT * FROM applicants_pds where recruitmentDate = '$recruitmentDate'");
                    $newStatus = mysqli_num_rows($result1);
                    while ($row = mysqli_fetch_array($result1)) {
                      if($row['verified'] == 0){
                        $verified = 'Not Verified';
                      } else{
                        $verified = 'Verified';
                      }
                      
                      ?>
                  
                  <tr>
                    <td><a href='registrationedit.php?id=<?php echo $row['id']; ?>'><i class='fas fa-edit blue-text'></i></a> | <a href='evaluationview.php?id=<?php echo $row['id']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                    <td><?php echo $verified; ?><!--  - <?php echo $verified; ?> --></td>
                    <td><?php echo ucwords($row['last']); ?>, <?php echo ucwords($row['name']); ?> <?php echo ucwords($row['middle']); ?></td>
                    <td><?php echo date("F d, Y", strtotime($row['birthday'])); ?></td>
                    <td><?php echo $row['age']; ?></td>
                    <td><?php echo ucwords($row['sex']); ?></td>
                   <td><?php echo ucwords($row['birthplace']); ?></td>
                   <td><?php echo ucwords($row['address']); ?></td>
                   <td><?php echo ucwords($row['civilstatus']); ?></td>
                   <td><?php echo ucwords($row['skills']); ?></td>
                   <td><?php echo ucwords($row['contact']); ?></td>
                   <td><?php echo ucwords($row['email']); ?></td>
                   <td><?php echo ucwords($row['socialmedia']); ?></td>
                   
<!--                     <td><a href='evaluationview.php?id=<?php echo $row['id']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td> -->
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
           
<form method="POST" action="export.php" enctype="multipart/form-data">

 <button type="submit" name="allapplying" class="btn btn-success"><i class='fas fa-download'></i> Generate CSV
                        </button>
</form>
            </div>

          </div>
        </div>
      </div>



    </div><!--/container-->
  </main>
  <!--/Main layout-->
  <?php else: ?>
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
      <div class='alert alert-danger alert-dismissible fade show' role='alert'>
        <h4>Forbidden.</h4>
      </div>
    </div>
    <!--/container-->
  </main>
  <!--/Main layout-->
  <?php endif ?>
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/scripts.php'; ?>
</body>
</html>


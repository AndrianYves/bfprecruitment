<?php
$page      = "settings.php";
$title     = "Evaluation";
$current = "settings";
?>

<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<?php

 function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
 }

if(isset($_POST['createuser'])){ 
    mysqli_autocommit($conn, false);
    $error = false;

    if (empty($_POST['username'])) {
      $error = true;
      $_SESSION['error'][] = 'Username is required.';
    } else {
      $username = test_input(strtolower($_POST['username']));
    }

     if (empty($_POST['firstname'])) {
      $error = true;
      $_SESSION['error'][] = 'Firstname is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['firstname'])) {
      $error = true;
      $_SESSION['error'][] = 'Firstname is invalid.';
    } else {
      $firstname = test_input(strtolower($_POST['firstname']));
    }

    if (empty($_POST['lastname'])) {
      $error = true;
      $_SESSION['error'][] = 'Lastname is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['lastname'])) {
      $error = true;
      $_SESSION['error'][] = 'Lastname is invalid.';
    } else {
      $lastname = test_input(strtolower($_POST['lastname']));
    }

    if (empty($_POST['role'])) {
      $error = true;
      $_SESSION['error'][] = 'Role is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['role'])) {
      $error = true;
      $_SESSION['error'][] = 'Role is invalid.';
    } else {
      $userrole = test_input(strtolower($_POST['role']));
    }

  $password = mysqli_real_escape_string($conn, $_POST["password"]);
  $cpassword = mysqli_real_escape_string($conn, $_POST["cpassword"]);

  if ($password == $cpassword) {
    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
    $sql = $conn->prepare("INSERT INTO admins(username, password, role, name, lastname) VALUES(?, ?, ?, ?, ?)");
    $sql->bind_param("sssss", $username, $hashedPassword, $userrole, $firstname, $lastname);
    $sql->execute();

  } else {
    $error = true;
    $_SESSION['error'][] = 'Password not matched.';
  }

  if(!$error){
    mysqli_commit($conn);
    $_SESSION['success'] = 'User Created';
  } else {
    mysqli_rollback($conn);
  }


}
?>
<?php
if(isset($_POST['edituser'])){ 
  mysqli_autocommit($conn, false);
  $error = false;

  $edituserid = mysqli_real_escape_string($conn, $_POST["edituserid"]);
  $editusername = test_input(strtolower($_POST['editusername']));
  $editfirstname = test_input(strtolower($_POST['editfirstname']));
  $editlastname = test_input(strtolower($_POST['editlastname']));
  $editrole = test_input(strtolower($_POST['editrole']));
  
  $password = mysqli_real_escape_string($conn, $_POST["editpassword"]);
  
  if(!empty($password)){
    $cpassword = mysqli_real_escape_string($conn, $_POST["editcpassword"]);
    if ($password == $cpassword) {
      $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
      $result1 = $conn->prepare("UPDATE admins SET username = ?, password = ?, role = ?, name = ?, lastname = ? where id = ?");
      $result1->bind_param("sssssi", $editusername, $hashedPassword, $editrole, $editfirstname, $editlastname, $edituserid);
      $result1->execute();
      $result1->close();
    } else {
      $error = true;
      $_SESSION['error'][] = 'Password not matched.';
    }
  } else {
    $result1 = $conn->prepare("UPDATE admins SET username = ?, role = ?, name = ?, lastname = ? where id = ?");
    $result1->bind_param("ssssi", $editusername, $editrole, $editfirstname, $editlastname, $edituserid);
    $result1->execute();
    $result1->close();
  }


  if(!$error){
    mysqli_commit($conn);
    $_SESSION['success'] = 'User Updated';
  } else {
    mysqli_rollback($conn);
  }


}
?>
<?php
if (isset($_POST['changepassword'])) {
  $currentpassword = mysqli_real_escape_string($conn, $_POST["currentpassword"]);
  $result2 = mysqli_query($conn, "SELECT * FROM admins WHERE username = '".$user['username']."'");

  $row = mysqli_fetch_assoc($result2);

      if(password_verify($currentpassword, $row['password'])){
        $password = mysqli_real_escape_string($conn, $_POST["password"]);
        $cpassword = mysqli_real_escape_string($conn, $_POST["cpassword"]);

        if ($password == $cpassword) {
          $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
          $result1 = mysqli_query($conn,"UPDATE admins SET password='$hashedPassword' WHERE username='".$user['username']."'");   
        
          $_SESSION['success'] = 'Password Changed';
        } else {
          $_SESSION['error'][] = 'New Password not matched.';
        }

      } else {
        $_SESSION['error'][] = 'Invalid current password.';
      }
}
?>
<?php
if (isset($_POST['deadlineformdate'])) {
  mysqli_autocommit($conn, false);
  $adminID = mysqli_real_escape_string($conn, $_POST["adminid"]);
  $timestamp = date("Y-m-d H:i:s");
  $settingsid = 1;
  $error = false;

  if (empty($_POST['submissionofformenddate'])) {
    $error = true;
    $_SESSION['error'][] = 'Date is required.';
  } elseif (strtotime($_POST['submissionofformenddate']) < strtotime($today)) {
    $error = true;
    $_SESSION['error'][] = 'Invalid date entered. Past dates not allowed.';
  } else {
    $submissionofformenddate = $_POST['submissionofformenddate'];
  }

  $result4 = $conn->prepare("UPDATE settings SET submissionofformenddate = ? where id = ?");
  $result4->bind_param("si", $submissionofformenddate, $settingsid);
  $result4->execute(); 

  if(!$error){
    mysqli_commit($conn);
    $_SESSION['success'] = 'Submission of Form date Extended';
  } else {
    mysqli_rollback($conn);
  }

}
?>

<?php
if (isset($_POST['formstatus'])) {
  $adminID = mysqli_real_escape_string($conn, $_POST["adminid"]);
  $timestamp = date("Y-m-d H:i:s");
  $settingsid = 1;
  $error = false;
  $allowed = array('START', 'PAUSED', 'STOP');

  if (empty($_POST['submissionofformstatus'])) {
    $error = true;
    $_SESSION['error'][] = 'Form status is required.';
  } elseif (!in_array($_POST['submissionofformstatus'], $allowed)) {
    $error = true;
    $_SESSION['error'][] = 'Form status is invalid.';
  } else {
    $submissionofformstatus = $_POST['submissionofformstatus'];
  }

  $result4 = $conn->prepare("UPDATE settings SET submissionofformstatus = ? where id = ?");
  $result4->bind_param("si", $submissionofformstatus, $settingsid);
  $result4->execute(); 

  $_SESSION['success'] = 'Submission of Form '.$submissionofformstatus.'';

}
?>

<?php
if (isset($_POST['updatepassingscore'])) {
  $adminID = mysqli_real_escape_string($conn, $_POST["adminid"]);
  $timestamp = date("Y-m-d H:i:s");
  $settingsid = 1;
  $error = false;

  if(empty($_POST['writterexampassingscore'])) {
    $error = true;
    $_SESSION['error'][] = 'Written score is required.';
  } else if(!is_numeric($_POST['writterexampassingscore'])) {
    $error = true;
    $_SESSION['error'][] = 'Written score entered was not numeric.';
  } else {
    $writterexampassingscore = $_POST['writterexampassingscore'];
  }

  $sql = $conn->prepare("UPDATE settings SET writterexampassingscore = ? where id = ?");
  $sql->bind_param("si", $writterexampassingscore, $settingsid);
  $sql->execute(); 
  $sql->close(); 

    // $statuspassed = 'PASSED';
    // $statusfailed = 'FAILED';

    // $sql = $conn->prepare("UPDATE test_ppt SET status = ? where recruitmentDate = ? and score >= ?");
    // $sql->bind_param("ss", $statuspassed, $recruitmentDate, $writterexampassingscore);
    // $sql->execute(); 
    // $sql->close(); 

    // $sql = $conn->prepare("UPDATE test_ppt SET status = ? where recruitmentDate = ? and score < ?");
    // $sql->bind_param("ss", $statusfailed, $recruitmentDate, $writterexampassingscore);
    // $sql->execute(); 
    // $sql->close();

    $sql1 = mysqli_query($conn, "SELECT * FROM test_ppt where recruitmentDate = '$recruitmentDate'");
    while ($row = mysqli_fetch_array($sql1)) {
      $applicantID = $row["applicantID"];
      $score = $row["score"];

      if ($score >= $writterexampassingscore) {
        $status = 'PASSED';
        $result = $conn->prepare("INSERT INTO test_pat(recruitmentDate, applicantID) VALUES(?, ?)");
        $result->bind_param("si", $recruitmentDate, $applicantID);
        $result->execute();
        $result->close();
      } else {
        $status = 'FAILED';
        $result = $conn->prepare("DELETE FROM test_pat WHERE applicantID = ?");
        $result->bind_param("i", $applicantID);
        $result->execute();
        $result->close();
      }

      $result1 = $conn->prepare("UPDATE test_ppt SET status = ?, timestamp = ? WHERE applicantID = ?");
      $result1->bind_param("ssi", $status, $timestamp, $applicantID);
      $result1->execute();
      $result1->close();

  } 

  $_SESSION['success'] = 'Written exam passing score updates to '.$writterexampassingscore.'';

}
?>

<?php
if (isset($_POST['updatetotalscore'])) {
  $adminID = mysqli_real_escape_string($conn, $_POST["adminid"]);
  $timestamp = date("Y-m-d H:i:s");
  $settingsid = 1;
  $error = false;

  if(empty($_POST['writtenexamtotalscore'])) {
    $error = true;
    $_SESSION['error'][] = 'Written total score is required.';
  } else if(!is_numeric($_POST['writtenexamtotalscore'])) {
    $error = true;
    $_SESSION['error'][] = 'Written total score entered was not numeric.';
  } else {
    $writtenexamtotalscore = $_POST['writtenexamtotalscore'];
  }

  $result4 = $conn->prepare("UPDATE settings SET writtenexamtotalscore = ? where id = ?");
  $result4->bind_param("si", $writtenexamtotalscore, $settingsid);
  $result4->execute(); 

  $_SESSION['success'] = 'Written exam total score updates to '.$writtenexamtotalscore.'';

}
?>

<?php
if (isset($_POST['updatepanelscore'])) {
  $adminID = mysqli_real_escape_string($conn, $_POST["adminid"]);
  $timestamp = date("Y-m-d H:i:s");
  $settingsid = 1;
  $error = false;

  if(empty($_POST['panelinterviewpassingscore'])) {
    $error = true;
    $_SESSION['error'][] = 'Panel interview passing score is required.';
  } else if(!is_numeric($_POST['panelinterviewpassingscore'])) {
    $error = true;
    $_SESSION['error'][] = 'Panel interview passing score entered was not numeric.';
  } else {
    $panelinterviewpassingscore = $_POST['panelinterviewpassingscore'];
  }

  $result4 = $conn->prepare("UPDATE settings SET panelinterviewpassingscore = ? where id = ?");
  $result4->bind_param("si", $panelinterviewpassingscore, $settingsid);
  $result4->execute(); 

  $_SESSION['success'] = 'Panel interview passing score updates to '.$panelinterviewpassingscore.'';

}
?>

<?php
if (isset($_POST['createrecruitmentbatch'])) {
  mysqli_autocommit($conn, false);
  $adminID = mysqli_real_escape_string($conn, $_POST["adminid"]);
  $timestamp = date("Y-m-d H:i:s");
  $settingsid = 1;
  $submissionofformstatus = 'START';
  $status = 'START';
  $status1 = 'ON-GOING';
  $error = false;

  $result1 = mysqli_query($conn, "SELECT status FROM `tests_status` where `test` = 'Recruitment'");
  $row = mysqli_fetch_assoc($result1);
  $testStatus = $row['status'];

  if($testStatus != 'DONE'){
    $error = true;
    $_SESSION['error'][] = 'The current Reruitment is not yet finished.';
  }

  if (empty($_POST['recruitmentbatch'])) {
    $error = true;
    $_SESSION['error'][] = 'Date is required.';
  } elseif (strtotime($_POST['recruitmentbatch']) < strtotime($today)) {
    $error = true;
    $_SESSION['error'][] = 'Invalid date entered. Past dates not allowed.';
  } else {
    $submissionofformenddate = $_POST['recruitmentbatch'];
  }

  $result4 = $conn->prepare("UPDATE settings SET recruitmentbatch = ?, submissionofformstatus = ?, submissionofformenddate = ? where id = ?");
  $result4->bind_param("sssi", $submissionofformenddate, $submissionofformstatus, $submissionofformenddate, $settingsid);
  $result4->execute(); 

  $result3 = $conn->prepare("UPDATE tests_status SET status = ? where test != 'Recruitment'");
  $result3->bind_param("s", $status);
  $result3->execute(); 

  $result4 = $conn->prepare("UPDATE tests_status SET status = ? where test = 'Recruitment'");
  $result4->bind_param("s", $status1);
  $result4->execute(); 


  if(!$error){
    mysqli_commit($conn);
    $_SESSION['success'] = 'Submission of Form Starts';
  } else {
    mysqli_rollback($conn);
  }

}
?>
<body class="grey lighten-3">
<?php include 'includes/nav.php'; ?>
<?php if ($role == 'Super User'): ?>

<!--Main layout-->
  <main class="pt-4 mx-lg-4">
    <div class="container-fluid mt-5">

        <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fas fa-warning'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>

      <?php
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fas fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>


    <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Settings</span>
          </h4>
        </div>
      </div>
      <!-- Heading -->

       <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
                  <h3>Users</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0" style="height: 300px;">
              <table class="table table-head-fixed">
                <thead>
                  <tr>
                    <th width="120">Full Name</th>
                    <th width="120">Role</th>
                    <th width="120">Last Login</th>
                    <th width="50">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $result3 = mysqli_query($conn, "SELECT * FROM admins");
                  while ($row = mysqli_fetch_array($result3)) {
                        ?>
                  <tr>
                    <td><?php echo ucfirst($row['lastname']);?>, <?php echo ucfirst($row['name']);?></td>
                    <td><?php echo ucwords($row['role']);?></td>
                    <td><?php echo date("Y-m-d H:i", strtotime($row['lastlogin']));?></td>
                     <td>
                        <a data-toggle='modal' data-target='#delete<?php echo $row['id']; ?>' href='#delete?id=<?php echo $row['id']; ?>'><i class="fas fa-trash red-text"></i></a>
                        <a data-toggle='modal' data-target='#edit<?php echo $row['id']; ?>' href='#edit?id=<?php echo $row['id']; ?>'><i class="fas fa-edit blue-text"></i></a>
                          </td>
                        </tr>
            <div class="modal fade" id="edit<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Edit User</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <!-- Default form login -->
        <form class="text-center" action="settings.php" method="POST">
            <!-- Username -->
            <div class="form-row">
              <div class="col">
                <label>Username</label>
                <input type="hidden" name="edituserid" class="form-control mb-4" value="<?php echo $row['id']; ?>">
                  <input type="text" name="editusername" class="form-control mb-4" value="<?php echo $row['username']; ?>">
              </div>
              <div class="col">
                <label>Role</label>
                  <select id="one" class="browser-default custom-select" name="editrole">
                    <option value="<?php echo $row['role']; ?>" selected><?php echo $row['role']; ?></option>
                    <option value="HR Officer">HR Officer</option>
                    <option value="Written Exam Handler">Written Exam Handler</option>
                    <option value="Panel Interviewer">Panel Interviewer</option>
                    <option value="Background Investigator">Background Investigator</option>
                    <option value="Neurological Examiner">Neurological Examiner</option>
                    <option value="Medical Examiner">Medical Examiner</option>
                    <option value="Super User">Super User</option>
                    <option value="Handler">Handler</option>
                  </select>
              </div>
            </div>

            <div class="form-row">
              <div class="col">
                <label>Firstname</label>
                  <input type="text" name="editfirstname" class="form-control mb-4" value="<?php echo $row['name']; ?>">
              </div>
              <div class="col">
                <label>Lastname</label>
                  <input type="text" name="editlastname" class="form-control mb-4" value="<?php echo $row['lastname']; ?>">
              </div>
            </div>

            <div class="form-group row">
              <label for="newpassword" class="col-sm-5 col-form-label">New Password</label>
              <div class="col-sm-7">
                <input type="password" class="form-control" placeholder="New Password" name="editpassword">
              </div>
            </div>
            <div class="form-group row">
              <label for="cpassword" class="col-sm-5 col-form-label">Confirm Password</label>
              <div class="col-sm-7">
                <input type="password" class="form-control" placeholder="Confirm Password" name="editcpassword">
              </div>
            </div>
      </div>

      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</a>
        <button class="btn btn-primary waves-effect" name="edituser" id="edituser"><i class="fa fa-check-square-o"></i>Edit</button>
      </div>
    </form>
    </div>
    <!--/.Content-->
  </div>
            </div>

            <div class="modal fade" id="delete<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Are you sure to delete this user?</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">


      </div>

      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</a>
        <a href='deleteuser.php?adminid=<?php echo $row['id']; ?>' class="btn btn-primary waves-effect"><i class="fa fa-check-square-o"></i>Yes</a>
      </div>
    </div>
    <!--/.Content-->
  </div>
            </div>

                <?php } ?>
                </tbody>
              </table>
            </div><!-- /.card-body -->
                        <div class="card-footer text-muted text-center">
              <div class="row">
                <div class="col-md-6">
                <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#createuser">Create Accounts</button>
                </div>
              </div>
            </div>
          </div> <!-- /.card-->
        </div><!--/col-->

       
     
      </div><!--/row-->
<br><br>
        <div class="row">
        <div class="col-md-4">
          <div class="card">
            <div class="card-header">
              <h3>Change Password</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
             <form role="form" action="settings.php" method="POST">
                <div class="form-group row">
                  <label for="currentpassword" class="col-sm-5 col-form-label">Current Password</label>
                  <div class="col-sm-7">
                    <input type="password" class="form-control" placeholder="Current Password" name="currentpassword" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="newpassword" class="col-sm-5 col-form-label">New Password</label>
                  <div class="col-sm-7">
                    <input type="password" class="form-control" placeholder="New Password" name="password" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="cpassword" class="col-sm-5 col-form-label">Confirm Password</label>
                  <div class="col-sm-7">
                    <input type="password" class="form-control" placeholder="Confirm Password" name="cpassword" required>
                  </div>
                </div>



                <button type="submit" class="btn btn-primary" name="changepassword">Change Password</button>
              </form>



            </div><!-- /.card-body -->
            
          </div> <!-- /.card-->
        </div><!--/col-->

 <div class="col-md-8">
          <div class="card">
            <div class="card-header">
              <h3>Recruitment Options</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive" style="height: 300px;">
              
              <table class="table table-head-fixed table-sm">
                <thead>
                  <tr>
                    <th width="150">Settings</th>
                    <th width="100">Options</th>
                    <th width="30">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $result3 = mysqli_query($conn, "SELECT * FROM settings");
                  $row = mysqli_fetch_assoc($result3);

                  $result4 = mysqli_query($conn, "SELECT status from tests_status WHERE `test`='Recruitment'");
                  $row1 = mysqli_fetch_assoc($result4);
                  ?>
                  <tr>
                    <form class="text-center" action="settings.php" method="POST">
                    <td>Recruitment Batch</td>
                    <td>
                      <input type="date" id="batchstart" class="form-control mb-4" name="recruitmentbatch" value="<?php echo $row['recruitmentbatch'];?>" <?php if($row1['status'] != 'DONE'){ echo 'disabled';}?>><span  class="badge badge-default"><?php echo $row1['status'];?></span></td>
                    <td> 
                       <?php if($row1['status'] == 'DONE'){?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#recruitmentbatch">Update</button>
                        <div class="modal fade" id="recruitmentbatch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                          aria-hidden="true">
                          <div class="modal-dialog modal-notify modal-info" role="document">
                            <!--Content-->
                            <div class="modal-content">
                              <!--Header-->
                              <div class="modal-header">
                                <p class="heading lead">Are You Sure?</p>

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="white-text">&times;</span>
                                </button>
                              </div>

                              <!--Body-->
                              <div class="modal-body">
                                <input class="form-check-input" type="hidden" name="adminid" id="adminid" value="<?php echo $user['id'];?>" style="visibility: hidden;">
                              </div>

                              <div class="modal-footer justify-content-center">
                                <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</a>
                                <button class="btn btn-primary waves-effect" name="createrecruitmentbatch" id="registrationStart"><i class="fa fa-check-square-o"></i>Save Changes</button>
                              </div>
                            </form>
                            </div>
                            <!--/.Content-->
                          </div>
                        </div>
                      <?php }?>
                    </td>
                  </tr>
                  <tr>
                    <form class="text-center" action="settings.php" method="POST">
                    <td>Submission of Form Deadline</td>
                    <td>
                      <input type="date" id="deadline" class="form-control mb-4" name="submissionofformenddate" value="<?php echo $row['submissionofformenddate'];?>" <?php if($row['submissionofformstatus'] == 'STOP'){ echo 'disabled';}?> min=""></td>
                    <td> 
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#registrationDate">Update</button>
                        <div class="modal fade" id="registrationDate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                          aria-hidden="true">
                          <div class="modal-dialog modal-notify modal-info" role="document">
                            <!--Content-->
                            <div class="modal-content">
                              <!--Header-->
                              <div class="modal-header">
                                <p class="heading lead">Are You Sure?</p>

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="white-text">&times;</span>
                                </button>
                              </div>

                              <!--Body-->
                              <div class="modal-body">
                                <input class="form-check-input" type="hidden" name="adminid" id="adminid" value="<?php echo $user['id'];?>" style="visibility: hidden;">
                              </div>

                              <div class="modal-footer justify-content-center">
                                <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</a>
                                <button class="btn btn-primary waves-effect" name="deadlineformdate" id="registrationStart"><i class="fa fa-check-square-o"></i>Save Changes</button>
                              </div>
                            </form>
                            </div>
                            <!--/.Content-->
                          </div>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <form class="text-center" action="settings.php" method="POST">
                    <td>Submission of Form Status</td>
                    <td><select id="one" class="browser-default custom-select" name="submissionofformstatus" <?php if($row['submissionofformstatus'] == 'STOP'){ echo 'disabled';}?>>
                          <option value="<?php echo $row['submissionofformstatus']; ?>" selected disabled><?php echo $row['submissionofformstatus']; ?></option>
                          <option value="START">START</option>
                          <option value="PAUSED">PAUSED</option>
                          <option value="STOP">STOP! *Notice: You will not be able to Start/Pause anymore.</option>
                        </select></td>
                    <td> 
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#formstatus">Update</button>
                        <div class="modal fade" id="formstatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                          aria-hidden="true">
                          <div class="modal-dialog modal-notify modal-info" role="document">
                            <!--Content-->
                            <div class="modal-content">
                              <!--Header-->
                              <div class="modal-header">
                                <p class="heading lead">Are You Sure?</p>

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="white-text">&times;</span>
                                </button>
                              </div>

                              <!--Body-->
                              <div class="modal-body">
                              <input class="form-check-input" type="hidden" name="adminid" id="adminid" value="<?php echo $user['id'];?>" style="visibility: hidden;">
                              </div>

                              <div class="modal-footer justify-content-center">
                                <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</a>
                                <button class="btn btn-primary waves-effect" name="formstatus" id="registrationStart"><i class="fa fa-check-square-o"></i>Save Changes</button>
                              </div>
                            </form>
                            </div>
                            <!--/.Content-->
                          </div>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <form class="text-center" action="settings.php" method="POST">
                    <td>Written Exam Passing Score</td>
                    <td><input type="number" class="form-control mb-4" min="0" name="writterexampassingscore" value="<?php echo $row['writterexampassingscore'];?>" max="<?php echo $row['writtenexamtotalscore'];?>"></td>
                    <td> 
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#writtenscore">Update</button>
                        <div class="modal fade" id="writtenscore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                          aria-hidden="true">
                          <div class="modal-dialog modal-notify modal-info" role="document">
                            <!--Content-->
                            <div class="modal-content">
                              <!--Header-->
                              <div class="modal-header">
                                <p class="heading lead">Are You Sure?</p>

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="white-text">&times;</span>
                                </button>
                              </div>

                              <!--Body-->
                              <div class="modal-body">
                              <input class="form-check-input" type="hidden" name="adminid" id="adminid" value="<?php echo $user['id'];?>" style="visibility: hidden;">
                              </div>

                              <div class="modal-footer justify-content-center">
                                <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</a>
                                <button class="btn btn-primary waves-effect" name="updatepassingscore" id="registrationStart"><i class="fa fa-check-square-o"></i>Save Changes</button>
                              </div>
                            </form>
                            </div>
                            <!--/.Content-->
                          </div>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <form class="text-center" action="settings.php" method="POST">
                    <td>Written Exam Total Score</td>
                    <td><input type="number" class="form-control mb-4" min="0" name="writtenexamtotalscore" value="<?php echo $row['writtenexamtotalscore'];?>"></td>
                    <td> 
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#passingscore">Update</button>
                        <div class="modal fade" id="passingscore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                          aria-hidden="true">
                          <div class="modal-dialog modal-notify modal-info" role="document">
                            <!--Content-->
                            <div class="modal-content">
                              <!--Header-->
                              <div class="modal-header">
                                <p class="heading lead">Are You Sure?</p>

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="white-text">&times;</span>
                                </button>
                              </div>

                              <!--Body-->
                              <div class="modal-body">
                              <input class="form-check-input" type="hidden" name="adminid" id="adminid" value="<?php echo $user['id'];?>" style="visibility: hidden;">
                              </div>

                              <div class="modal-footer justify-content-center">
                                <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</a>
                                <button class="btn btn-primary waves-effect" name="updatetotalscore" id="registrationStart"><i class="fa fa-check-square-o"></i>Save Changes</button>
                              </div>
                            </form>
                            </div>
                            <!--/.Content-->
                          </div>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <form class="text-center" action="settings.php" method="POST">
                    <td>Panel Interview Passing Score</td>
                    <td><input type="number" class="form-control mb-4" min="0" name="panelinterviewpassingscore" value="<?php echo $row['panelinterviewpassingscore'];?>"></td>
                    <td> 
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#interview">Update</button>
                        <div class="modal fade" id="interview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                          aria-hidden="true">
                          <div class="modal-dialog modal-notify modal-info" role="document">
                            <!--Content-->
                            <div class="modal-content">
                              <!--Header-->
                              <div class="modal-header">
                                <p class="heading lead">Are You Sure?</p>

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true" class="white-text">&times;</span>
                                </button>
                              </div>

                              <!--Body-->
                              <div class="modal-body">
                              <input class="form-check-input" type="hidden" name="adminid" id="adminid" value="<?php echo $user['id'];?>" style="visibility: hidden;">
                              </div>

                              <div class="modal-footer justify-content-center">
                                <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</a>
                                <button class="btn btn-primary waves-effect" name="updatepanelscore" id="registrationStart"><i class="fa fa-check-square-o"></i>Save Changes</button>
                              </div>
                            </form>
                            </div>
                            <!--/.Content-->
                          </div>
                        </div>
                    </td>
                  </tr>
                </tbody>
              </table>
  



            </div><!-- /.card-body -->

            
          </div> <!-- /.card-->
        </div><!--/col-->

     
      </div><!--/row-->


  </div><!--/container-->
</main><!--/Main layout-->
<!-- Are you sure -->
<div class="modal fade" id="createuser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Create User</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <!-- Default form login -->
        <form class="text-center" action="settings.php" method="POST">
            <!-- Username -->
            <div class="form-row">
              <div class="col">
                  <input type="text" name="username" class="form-control mb-4" placeholder="Username">
              </div>
              <div class="col">
                  <select id="one" class="browser-default custom-select" name="role">
                    <option value="HR Officer">HR Officer</option>
                    <option value="Written Exam Handler">Written Exam Handler</option>
                    <option value="Panel Interviewer">Panel Interviewer</option>
                    <option value="Background Investigator">Background Investigator</option>
                    <option value="Neurological Examiner">Neurological Examiner</option>
                    <option value="Medical Examiner">Medical Examiner</option>
                    <option value="Super User">Super User</option>
                    <option value="Handler">Handler</option>
                  </select>
              </div>
            </div>

            <!-- Password -->
            <input type="password" name="password" class="form-control mb-4" placeholder="Password">

            <!-- Password -->
            <input type="password" name="cpassword" class="form-control mb-4" placeholder="Confirm Password">

            <div class="form-row">
              <div class="col">
                  <input type="text" name="firstname" class="form-control mb-4" placeholder="First Name">
              </div>
              <div class="col">
                  <input type="text" name="lastname" class="form-control mb-4" placeholder="Last Name">
              </div>
            </div>
      </div>

      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</a>
        <button class="btn btn-primary waves-effect" name="createuser" id="createuser"><i class="fa fa-check-square-o"></i>Create</button>
      </div>
    </form>
    </div>
    <!--/.Content-->
  </div>
</div>


  <!-- Are you sure -->
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true"><div id="load_screen" style="display: none;"><div id="loading"><div class="spinner-border" role="status">
  <span class="sr-only">Loading...</span>
</div></div></div>
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Are you sure?</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <img src="logo.png" class="center mb-3 animated rotateIn rounded-circle" style="width: 150px; height: 150px;">
          <p class="text-center">Please double check the the assessment before submitting to avoid unncesseary mistakes.<br>
                  Once submitted will be the final result of the written exam.
          </p>

        </div>
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</a>
        <button class="btn btn-primary waves-effect" name="submit" id="submit" onclick="loading()"><i class="fa fa-check-square-o"></i>Yes</button>
      </div>
      </form>
    </div>
    <!--/.Content-->
  </div>
</div>
<?php else: ?>
<!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">

      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>

    <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Settings</span>
          </h4>
        </div>
      </div>
      <!-- Heading -->

       <div class="row">
        <div class="col-md-5">
          <div class="card">
            <div class="card-header">
              <h3>Change Password</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
             <form role="form" action="settings.php" method="POST">
                <div class="form-group row">
                  <label for="currentpassword" class="col-sm-5 col-form-label">Current Password</label>
                  <div class="col-sm-7">
                    <input type="password" class="form-control" placeholder="Current Password" name="currentpassword" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="newpassword" class="col-sm-5 col-form-label">New Password</label>
                  <div class="col-sm-7">
                    <input type="password" class="form-control" placeholder="New Password" name="password" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="cpassword" class="col-sm-5 col-form-label">Confirm Password</label>
                  <div class="col-sm-7">
                    <input type="password" class="form-control" placeholder="Confirm Password" name="cpassword" required>
                  </div>
                </div>



                <button type="submit" class="btn btn-primary" name="changepassword">Change Password</button>
              </form>



            </div><!-- /.card-body -->
            
          </div> <!-- /.card-->
        </div><!--/col-->


     
      </div><!--/row-->

  </div><!--/container-->
</main><!--/Main layout-->


<?php endif ?>


<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>
<script type="text/javascript">
  var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
 if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 

today = yyyy+'-'+mm+'-'+dd;
document.getElementById("deadline").setAttribute("min", today);
</script>

<script type="text/javascript">
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
 if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 

today = yyyy+'-'+mm+'-'+dd;
document.getElementById("batchstart").setAttribute("min", today);
</script>


</body>
</html>

 <?php include 'includes/conn.php'; ?>
 <?php include 'includes/session.php'; ?>

 <?php 
  if (isset($_POST['export'])) {
      header("Content-type: application/octet-stream");
      header('Content-Disposition: attachment; filename=Evaluation Results - '.$recruitmentDate.'.csv');
          header("Pragma: no-cache");
    	header("Expires: 0");
      $output = fopen("php://output", "w");
      fputcsv($output, array('Evaluation Results - '.$recruitmentDate.' Accepted'));
      fputcsv($output, array('Applicant Number', 'Lastname', 'Firstname', 'Middlename', 'Status'));
      $sql = "SELECT applicants_pds.applicantnumber, applicants_pds.last, applicants_pds.name, applicants_pds.middle, test_evaluation.status FROM `applicants_pds` join test_evaluation on applicants_pds.id = test_evaluation.applicantID where `status` = 'Accepted' and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC";
      $result = mysqli_query($conn, $sql);
      while($row = mysqli_fetch_assoc($result)){
        fputcsv($output, $row);
      }
      fclose($output);
  }

    if (isset($_POST['exportmedresults'])) {
      header("Content-type: application/octet-stream");
      header('Content-Disposition: attachment; filename=Medical/Dental and Physical Examination Results - '.$recruitmentDate.'.csv');
          header("Pragma: no-cache");
      header("Expires: 0");
      $output = fopen("php://output", "w");
      fputcsv($output, array('Medical/Dental and Physical Examination Results - '.$recruitmentDate.' Passed'));
      fputcsv($output, array('Applicant Number', 'Lastname', 'Firstname', 'Middlename', 'Status'));
      $sql = "SELECT applicants_pds.applicantnumber, applicants_pds.last, applicants_pds.name, applicants_pds.middle, test_med.status FROM `applicants_pds` join test_med on applicants_pds.id = test_med.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC";
      $result = mysqli_query($conn, $sql);
      while($row = mysqli_fetch_assoc($result)){
        fputcsv($output, $row);
      }
      fclose($output);
  }

  if (isset($_POST['exportpptresults'])) {
      header("Content-type: application/octet-stream");
      header('Content-Disposition: attachment; filename=Pen and Paper Examination Results - '.$recruitmentDate.'.csv');
          header("Pragma: no-cache");
      header("Expires: 0");
      $output = fopen("php://output", "w");
      fputcsv($output, array('Pen and Paper Examination Results - '.$recruitmentDate.' Passed'));
      fputcsv($output, array('Applicant Number', 'Lastname', 'Firstname', 'Middlename', 'Score', 'Status'));
      $sql = "SELECT applicants_pds.applicantnumber, applicants_pds.applicantnumber, applicants_pds.last, applicants_pds.name, applicants_pds.middle, test_ppt.score, test_ppt.status FROM `applicants_pds` join test_ppt on applicants_pds.id = test_ppt.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC";
      $result = mysqli_query($conn, $sql);
      while($row = mysqli_fetch_assoc($result)){
        fputcsv($output, $row);
      }
      fclose($output);
  }

  if (isset($_POST['exportpatresults'])) {
      header("Content-type: application/octet-stream");
      header('Content-Disposition: attachment; filename=Physical Agility Examination Results - '.$recruitmentDate.'.csv');
          header("Pragma: no-cache");
      header("Expires: 0");
      $output = fopen("php://output", "w");
      fputcsv($output, array('Physical Agility Examination Results - '.$recruitmentDate.' Passed'));
      fputcsv($output, array('Applicant Number', 'Lastname', 'Firstname', 'Middlename', 'Gender', 'Push-Ups/Full Arms Hanging', 'Pull-Ups', 'Sit-ups', 'Endurance', 'Diameter Hose', 'Kilograms Hose', 'Total Score', 'Status'));
      $sql = "SELECT applicants_pds.applicantnumber, applicants_pds.applicantnumber, applicants_pds.last, applicants_pds.name, applicants_pds.middle, applicants_pds.sex, test_pat.push, test_pat.pull, test_pat.sits, test_pat.endurance, test_pat.diameterhose, test_pat.kilogramshose, test_pat.total, test_pat.status FROM `applicants_pds` join test_pat on applicants_pds.id = test_pat.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC";
      $result = mysqli_query($conn, $sql);
      while($row = mysqli_fetch_assoc($result)){
        fputcsv($output, $row);
      }
      fclose($output);
  }

  if (isset($_POST['exportinterviewresults'])) {
      header("Content-type: application/octet-stream");
      header('Content-Disposition: attachment; filename=Panel Interview Results - '.$recruitmentDate.'.csv');
          header("Pragma: no-cache");
      header("Expires: 0");
      $output = fopen("php://output", "w");
      fputcsv($output, array('Panel Interview Results - '.$recruitmentDate.' Passed'));
      fputcsv($output, array('Applicant Number', 'Lastname', 'Firstname', 'Middlename', 'Potential Trait', 'Psychosocial Attributes', 'Personality Traits', 'Remarks', 'Total Score', 'Status'));
      $sql = "SELECT applicants_pds.applicantnumber, applicants_pds.applicantnumber, applicants_pds.last, applicants_pds.name, applicants_pds.middle, test_interview.potential, test_interview.psychosocial, test_interview.personality, test_interview.remarks, test_interview.total, test_interview.status FROM `applicants_pds` join test_interview on applicants_pds.id = test_interview.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC";
      $result = mysqli_query($conn, $sql);
      while($row = mysqli_fetch_assoc($result)){
        fputcsv($output, $row);
      }
      fclose($output);
  }


    if (isset($_POST['exportcbiresults'])) {
      header("Content-type: application/octet-stream");
      header('Content-Disposition: attachment; filename=Background Investigation Results - '.$recruitmentDate.'.csv');
          header("Pragma: no-cache");
      header("Expires: 0");
      $output = fopen("php://output", "w");
      fputcsv($output, array('Background Investigation Results - '.$recruitmentDate.' Passed'));
      fputcsv($output, array('Applicant Number', 'Lastname', 'Firstname', 'Middlename', 'Status', 'Remarks'));
      $sql = "SELECT applicants_pds.applicantnumber, applicants_pds.last, applicants_pds.name, applicants_pds.middle, test_investigator.status, test_investigator.remarks FROM `applicants_pds` join test_investigator on applicants_pds.id = test_investigator.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC";
      $result = mysqli_query($conn, $sql);
      while($row = mysqli_fetch_assoc($result)){
        fputcsv($output, $row);
      }
      fclose($output);
    }

    if (isset($_POST['exportneuroresults'])) {
      header("Content-type: application/octet-stream");
      header('Content-Disposition: attachment; filename=Neuro-Psychiatric and Drug Examination Results - '.$recruitmentDate.'.csv');
          header("Pragma: no-cache");
      header("Expires: 0");
      $output = fopen("php://output", "w");
      fputcsv($output, array('Neuro-Psychiatric and Drug Examination Results - '.$recruitmentDate.' Passed'));
      fputcsv($output, array('Applicant Number', 'Lastname', 'Firstname', 'Middlename', 'Status'));
      $sql = "SELECT applicants_pds.applicantnumber, applicants_pds.last, applicants_pds.name, applicants_pds.middle, test_neuro.status FROM `applicants_pds` join test_neuro on applicants_pds.id = test_neuro.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC";
      $result = mysqli_query($conn, $sql);
      while($row = mysqli_fetch_assoc($result)){
        fputcsv($output, $row);
      }
      fclose($output);
    }

    if (isset($_POST['exportfinalresults'])) {
      header("Content-type: application/octet-stream");
      header('Content-Disposition: attachment; filename=Final Results - '.$recruitmentDate.'.csv');
          header("Pragma: no-cache");
      header("Expires: 0");
      $output = fopen("php://output", "w");
       fputcsv($output, array('Final Results - '.$recruitmentDate.' Rankings'));
      fputcsv($output, array('Applicant Number', 'Overall Performance', 'Lastname', 'Firstname', 'Middlename', 'Age', 'Birthday', 'Sex', 'Birthplace', 'Address', 'Civil Status', 'Skills', 'Contact', 'Email'));
      $sql = "SELECT applicants_pds.applicantnumber, ((test_ppt.score * .4) + (test_pat.total * .3) + (test_interview.total * .3)) as overall, applicants_pds.last, applicants_pds.name, applicants_pds.middle, applicants_pds.age, applicants_pds.birthday, applicants_pds.sex, applicants_pds.birthplace, applicants_pds.address, applicants_pds.civilstatus, applicants_pds.skills, applicants_pds.contact, applicants_pds.email FROM `applicants_pds` join test_ppt on applicants_pds.id = test_ppt.applicantID join test_pat on applicants_pds.id = test_pat.applicantID join test_interview on applicants_pds.id = test_interview.applicantID join test_neuro on applicants_pds.id = test_neuro.applicantID where applicants_pds.recruitmentDate = '$recruitmentDate' and test_neuro.status = 'PASSED' order by overall DESC";
      $result = mysqli_query($conn, $sql);
      while($row = mysqli_fetch_assoc($result)){
        fputcsv($output, $row);
      }
      fclose($output);
    }

    if (isset($_POST['currentapplicant'])) {
      header("Content-type: application/octet-stream");
      header('Content-Disposition: attachment; filename=Applicant Results - '.$recruitmentDate.'.csv');
          header("Pragma: no-cache");
      header("Expires: 0");
      $output = fopen("php://output", "w");
       fputcsv($output, array('Final Results - '.$recruitmentDate.' Rankings'));
      fputcsv($output, array('Applicant Number', 'Medical/Dental and Physical Examination Results', 'Pen and Paper Examination Results', 'Physical Agility Examination Results', 'Panel Interview Results', 'Background Investigation Results', 'Neuro-Psychiatric and Drug Examination Results', 'Lastname', 'Firstname', 'Middlename', 'Age', 'Birthday', 'Sex', 'Birthplace', 'Address', 'Civil Status', 'Skills', 'Contact', 'Email'));
      $sql = "SELECT applicants_pds.applicantnumber, test_med.status, test_ppt.status, test_pat.status, test_interview.status, test_investigator.status, test_neuro.status, applicants_pds.last, applicants_pds.name, applicants_pds.middle, applicants_pds.age, applicants_pds.birthday, applicants_pds.sex, applicants_pds.birthplace, applicants_pds.address, applicants_pds.civilstatus, applicants_pds.skills, applicants_pds.contact, applicants_pds.email FROM `applicants_pds` left join test_med on applicants_pds.id = test_med.applicantID left join test_ppt on applicants_pds.id = test_ppt.applicantID left join test_pat on applicants_pds.id = test_pat.applicantID left join test_investigator on applicants_pds.id = test_investigator.applicantID left join test_interview on applicants_pds.id = test_interview.applicantID left join test_neuro on applicants_pds.id = test_neuro.applicantID where applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.applicantnumber ASC";
      $result = mysqli_query($conn, $sql);
      while($row = mysqli_fetch_assoc($result)){
        fputcsv($output, $row);
      }
      fclose($output);
    }

    if (isset($_POST['allapplying'])) {
      header("Content-type: application/octet-stream");
      header('Content-Disposition: attachment; filename=All Current Applying - '.$recruitmentDate.'.csv');
          header("Pragma: no-cache");
      header("Expires: 0");
      $output = fopen("php://output", "w");
       fputcsv($output, array('All Current Applying - '.$recruitmentDate.''));
      fputcsv($output, array('Applicant Number', 'Lastname', 'Firstname', 'Middlename', 'Age', 'Birthday', 'Sex', 'Birthplace', 'Address', 'Civil Status', 'Skills', 'Restriction Code', 'Contact', 'Email', 'Facebook'));
      $sql = "SELECT id, last, middle, name, age, birthday, sex, birthplace, address, civilstatus, skills, restrictioncode, contact, email, socialmedia from applicants_pds where recruitmentDate = '$recruitmentDate' order by applicantnumber ASC";
      $result = mysqli_query($conn, $sql);
      while($row = mysqli_fetch_assoc($result)){
        fputcsv($output, $row);
      }
      fclose($output);
    }
?>
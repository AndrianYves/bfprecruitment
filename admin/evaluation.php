<?php
$page      = "evaluation.php";
$title     = "Evaluation";
$current = "Evaluation";
?>

<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<?php
  if(isset($_POST['pass'])){ 
    mysqli_autocommit($conn, false);
    $adminID=$_POST['adminid'];
    $timestamp = date("Y-m-d H:i:s");
    $error = false;

    $status = 'Accepted';
    $applicantID = $_POST["appID"]; 
    $result = $conn->prepare("INSERT INTO test_med(recruitmentDate, applicantID) VALUES(?, ?)");
    $result->bind_param("si", $recruitmentDate, $applicantID);
    $result->execute();
    $result->close();

    $result1 = $conn->prepare("UPDATE test_evaluation SET status = ?, adminID = ?, timestamp = ? where applicantID = ?");
    $result1->bind_param("sisi", $status, $adminID, $timestamp, $applicantID);
    $result1->execute();
    $result1->close();

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Save';
    } else {
      mysqli_rollback($conn);
    }
  }

  if(isset($_POST['fail'])){ 
    mysqli_autocommit($conn, false);
    $adminID=$_POST['adminid'];
    $timestamp = date("Y-m-d H:i:s");
    $error = false;

    $status = 'Rejected';
    $applicantID = $_POST["appID"]; 
    $result = $conn->prepare("INSERT INTO test_med(recruitmentDate, applicantID) VALUES(?, ?)");
    $result->bind_param("si", $recruitmentDate, $applicantID);
    $result->execute();
    $result->close();

    $result1 = $conn->prepare("UPDATE test_evaluation SET status = ?, adminID = ?, timestamp = ? where applicantID = ?");
    $result1->bind_param("sisi", $status, $adminID, $timestamp, $applicantID);
    $result1->execute();
    $result1->close();

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Save';
    } else {
      mysqli_rollback($conn);
    }
  }

  if(isset($_POST['accept'])){ 
    mysqli_autocommit($conn, false);
    $adminID=$_POST['adminid'];
    $timestamp = date("Y-m-d H:i:s");
    $error = false;

    if (empty($_POST["status"])){
      $error = true;
      $_SESSION['error'][] = 'No applicant selected.';
    } else {
      $number = count($_POST["status"]);
      for($i=0; $i<$number; $i++) {
        if(trim($_POST["status"][$i] != '')) {
            $status = 'Accepted';
          $applicantID = $_POST["status"][$i]; 
          $result = $conn->prepare("INSERT INTO test_med(recruitmentDate, applicantID) VALUES(?, ?)");
          $result->bind_param("si", $recruitmentDate, $applicantID);
          $result->execute();
          $result->close();

          $result1 = $conn->prepare("UPDATE test_evaluation SET status = ?, adminID = ?, timestamp = ? where applicantID = ?");
          $result1->bind_param("sisi", $status, $adminID, $timestamp, $applicantID);
          $result1->execute();
          $result1->close();
        }
      }
    }

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Save';
    } else {
      mysqli_rollback($conn);
    }
  }

  if(isset($_POST['reject'])){ 
    mysqli_autocommit($conn, false);
    $adminID=$_POST['adminid'];
    $timestamp = date("Y-m-d H:i:s");
    $error = false;
    $status = 'Rejected';

    if (empty($_POST["status"])){
      $error = true;
      $_SESSION['error'][] = 'No applicant selected.';
    } else {
      $number = count($_POST["status"]);
      for($i=0; $i<$number; $i++) {
        if(trim($_POST["status"][$i] != '')) {
          $applicantID = $_POST["status"][$i]; 

          $result1 = $conn->prepare("UPDATE test_evaluation SET status = ?, adminID = ?, timestamp = ? where applicantID = ?");
          $result1->bind_param("sisi", $status, $adminID, $timestamp, $applicantID);
          $result1->execute();
          $result1->close();
        }
      }
    }

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Save';
    } else {
      mysqli_rollback($conn);
    }
  }

?>
<?php
  if (isset($_POST['edit'])) {
    mysqli_autocommit($conn, false);
    $timestamp = date("Y-m-d H:i:s");
    $allowed = array('Accepted', 'Rejected');
    $error = false;

    if (empty($_POST['status'])) {
      $error = true;
      $_SESSION['error'][] = 'Status is required.';
    } elseif (!in_array($_POST['status'], $allowed)) {
      $error = true;
      $_SESSION['error'][] = 'Status is invalid.';
    } else {
      $status = $_POST['status'];
    }

    $applicantID = $_POST['applicantID'];

    $result1 = $conn->prepare("UPDATE test_evaluation SET status = ?, timestamp = ? WHERE applicantID = ?");
    $result1->bind_param("ssi", $status, $timestamp, $applicantID);
    $result1->execute();
    $result1->close();

    if ($status == 'Accepted') {
      $result = $conn->prepare("INSERT INTO test_med(recruitmentDate, applicantID) VALUES(?, ?)");
      $result->bind_param("si", $recruitmentDate, $applicantID);
      $result->execute();
      $result->close();
    } else {
      $result = $conn->prepare("DELETE FROM test_med WHERE applicantID = ?");
      $result->bind_param("i", $applicantID);
      $result->execute();
      $result->close();
    }


    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Status Updated';
    } else {
      mysqli_rollback($conn);
    }

  }
?>

<?php
  if (isset($_POST['changestatus'])) {
    $result = $conn->prepare("UPDATE tests_status SET status = 'DONE' WHERE test = 'Evaluation'");
    $result->execute();
    $result->close();

    $result1 = $conn->prepare("UPDATE tests_status SET status = 'ON-GOING' WHERE test = 'Medical/Dental and Physical Test'");
    $result1->execute();
    $result1->close();

    // $result1 = mysqli_query($conn,"UPDATE tests_status SET status = 'DONE' WHERE test = 'Evaluation'");
    // $result1 = mysqli_query($conn,"UPDATE tests_status SET status = 'ON-GOING' WHERE test = 'Medical/Dental and Physical Test'");
  }
?>

<body class="grey lighten-3">
<?php include 'includes/nav.php'; ?>
<?php if ($role == 'HR Officer'): ?>

<?php
    $results = $conn -> prepare("SELECT status FROM `tests_status` where `test` = 'Evaluation'");
    $results->execute();
    $query = $results->get_result();
    $row = $query->fetch_assoc();
    $testStatus = $row['status'];
    $results->close();

  // $result1 = mysqli_query($conn, "SELECT status FROM `tests_status` where `test` = 'Evaluation'");
  // $row = mysqli_fetch_assoc($result1);
  // $testStatus = $row['status'];

  switch ($testStatus): 
?>
<?php case "START": ?>

<!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
      <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fas fa-warning'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>

      <?php
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fas fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>

<?php 
$result2 = mysqli_query($conn, "SELECT * FROM `test_evaluation` join applicants_pds on applicants_pds.id = test_evaluation.applicantID where `status` is not null and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");

$sql = mysqli_query($conn, "SELECT * FROM `test_evaluation` join applicants_pds on applicants_pds.id = test_evaluation.applicantID where applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");

$result13 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_evaluation on applicants_pds.id = test_evaluation.applicantID where `status` = 'Accepted' and applicants_pds.recruitmentDate = '$recruitmentDate'");

?>


    <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Evaluation</span>
          </h4>
          <h4 class="mb-2 mb-sm-0 pt-1 text-right">Processed <?php echo mysqli_num_rows($result2);?> out of <?php echo mysqli_num_rows($sql);?> applicants</h4>
        </div>
      </div>
      <!-- Heading -->
 <div class="row">
        <div class="col-md-12">
          <div class="card">
            <!--Card content-->
            <div class="card-body">
              <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <div class="row">
                  <div class="col-6 float-left">
                    <button class="btn btn-outline-success" name="accept" type="submit"><i class="fas fa-save" aria-hidden="true"></i> Accept</button>
                    <button class="btn btn-outline-danger" name="reject" type="submit"><i class="fas fa-save" aria-hidden="true"></i> Reject</button>
                  </div>
                   <div class="col-6 float-right">
                    <h4 class="mb-2 mb-sm-0 pt-1 text-right text-success"> <?php echo mysqli_num_rows($result13);?> Accepted</h4>
                 
                </div>
              </div>
                              
              <ul class="nav nav-pills mb-3 justify-content-end" id="pills-tab" role="tablist">
              <li class="nav-item">
              <a class="btn-primary nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
              aria-controls="pills-home" aria-selected="true">ALL</a>
              </li>
              <li class="nav-item">
              <a class="btn-info nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
              aria-controls="pills-profile" aria-selected="false">Finished</a>
              </li>
            </ul>
            <div class="tab-content pt-2 pl-1" id="pills-tabContent">
              <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

              <table class="table table-bordered table-responsive-md display" cellspacing="0" width="100%">
                <thead>
                  <th width="20">
                    <!-- Default unchecked -->
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="selectall">
                      <label class="custom-control-label" for="selectall"></label>
                    </div>
                  </th>
                  <th width="20">Applicant Number</th>
                  <th width="200">Full Name</th>
                  <th width="20">View Profile</th>
                  <th width="20">Status</th>
                </thead>
                <tbody>

                  <?php
                    $result1 = mysqli_query($conn, "SELECT * FROM `test_evaluation` join applicants_pds on applicants_pds.id = test_evaluation.applicantID where `status` is NULL and applicants_pds.recruitmentDate = '$recruitmentDate' and verified = '1' order by applicants_pds.id ASC");
                    $newStatus = mysqli_num_rows($result1);
                    while ($row = mysqli_fetch_array($result1)) {
                      $input = '<form method="POST" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'">
                                  <input class="form-check-input" type="hidden" value="'.$row['applicantID'].'" name="appID">
                                  <input class="form-check-input" type="hidden" name="adminid" id="adminid" value="'.$user['id'].'" style="visibility: hidden;">
                                  <div class="btn-group" role="group" aria-label="Basic example">
                                  <button type="submit" name="pass" class="btn btn btn-success">Accept</button>
                                  <button type="submit" name="fail" class="btn btn btn-danger">Reject</button>
                                  </div>
                                </form>
                                ';

                      echo '
                        <tr>
                          <td scope="row">
                            <!-- Default unchecked -->
                            <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chkCheckBoxId" name="status[]" value="'.$row['applicantID'].'" id="'.$row['applicantID'].'">
                            <label class="custom-control-label" for="'.$row['applicantID'].'"></label>
                            </div>
                          </td>
                          <td>'.$row["applicantnumber"].'</td>
                          <td>'.ucwords($row["last"]).', '.ucwords($row["name"]).' '.ucwords($row["middle"]).'</td>
                          <th><h4><a href="evaluationview.php?id='.$row['applicantID'].'"><i class="fas fa-eye" class="btn btn-danger btn-rounded form-check-label"></i></a></h4></th>
                          <th>'.$input.'</th>
                        </tr>
                      ';
                    }
                  ?>
                
                </tbody>
              </table>
                </div>
              <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        <table class="table table-bordered display table-responsive-md" cellspacing="0" width="100%">
                <thead>
                  <th width="100">Applicant Number</th>
                  <th width="200">Full Name</th>
                  <th width="100">View Profile</th>
                  <th width="100">Status</th>
                  <th width="100">Action</th>
                </thead>
                <tbody>

                  <?php
                    $result2 = mysqli_query($conn, "SELECT *, applicants_pds.id as applicantID FROM `test_evaluation` join applicants_pds on applicants_pds.id = test_evaluation.applicantID where `status` is not null and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");
                    while ($row3 = mysqli_fetch_array($result2)) {
                    if ($row3['status'] == 'Accepted'){
                        $color = 'success';
                      } else {
                        $color = 'danger';
                      }

                      ?>
                        <tr>
                          <td><?php echo $row3['applicantnumber'];?></td>
                          <td><?php echo ucwords($row3['last']);?>, <?php echo ucwords($row3['name']);?> <?php echo ucwords($row3['middle']);?></td>
                          <th><h4><a href="evaluationview.php?id=<?php echo $row3['applicantID'];?>"><i class="fas fa-eye" class="btn btn-danger btn-rounded form-check-label"></i></a></h4></th>
                          <td><span class='badge badge-pill badge-<?php echo $color?>'><?php echo $row3['status'];?></span></td>
                          <td>
                           <div class='text-center'><a data-toggle='modal' data-target='#edit<?php echo $row3['applicantID']; ?>' href='#edit?id=<?php echo $row3['applicantID']; ?>'><i class='fas fa-edit blue-text'></i></a></div>
                          </td>
                        </tr>
            <div class="modal fade" id="edit<?php echo $row3['applicantID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold"><?php echo $row3['applicantID'];?> - <?php echo ucwords($row3['last']);?>, <?php echo ucwords($row3['name']);?> <?php echo ucwords($row3['middle']);?>
                    </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                      <div class="text-center">
                        <input type="hidden" id="id" name="applicantID" class="form-control validate" value="<?php echo $row3['applicantID']; ?>" required>
                        
                         <div class="btn-group btn-group-toggle" data-toggle="buttons">
                          <?php if($row3['status'] == 'Accepted'){ 
                            ?>
                          <label class="btn btn-danger form-check-label">
                            <input class="form-check-input" type="radio" name="status" autocomplete="off" value="Rejected"> Rejected
                          </label>
                        <?php } else {?>
                          <label class="btn btn-success form-check-label <?php if($row3['status'] == 'Accepted'){ echo 'active'; }?>">
                            <input class="form-check-input" type="radio" name="status" autocomplete="off" value="Accepted"> Accepted
                          </label>
                        <?php }?>

                        </div>

                      </div>
                    <div class="modal-footer justify-content-center">
                      <button type="button" class="btn btn-outline-warning waves-effect" data-dismiss="modal">Cancel</button>
                      <button class="btn btn-outline-primary waves-effect" name="edit" id="edit">Update</button>
                    </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php    } ?>

                </tbody>
              </table>
  </div>
</div>



                <div class="row">
                  <div class="col-6">
                    <button class="btn btn-outline-success" name="accept" type="submit"><i class="fas fa-save" aria-hidden="true"></i> Accept</button>
                    <button class="btn btn-outline-danger" name="reject" type="submit"><i class="fas fa-save" aria-hidden="true"></i> Reject</button></form>
                  </div>
                  <div class="col-6 text-right">
                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalLoginForm" <?php if($newStatus > 0){ echo 'disabled'; }?>>Finalize</button>
                  </div>
                </div>
            </div>

          </div>
        </div>
      </div>

  </div><!--/container-->
</main><!--/Main layout-->
<?php if($newStatus == 0){ ?>
<!-- Are you sure -->
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Are you sure?</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <img src="logo.png" class="center mb-3 animated rotateIn rounded-circle" style="width: 150px; height: 150px;">
          <p class="text-center">Please double check the assessment before submitting to avoid unncessary mistakes.<br>
                  Once submitted will be the final result of the evaluation.
          </p>

        </div>
      </div>

      <!--Footer-->
 <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data" class="needs-validation" novalidate>
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</a>
        <button class="btn btn-primary waves-effect" name="changestatus" id="changestatus"><i class="fa fa-check-square-o"></i>Yes</button>
      </div>
    </form>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<?php }?>
<?php break; ?>
<?php case "DONE": ?>

<!--Main layout-->
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-success alert-dismissible fade show' role='alert'>
    <h4><i class='icon fa fa-check'></i> Done!</h4>
    <a href='evaluationresults.php' class='alert-link'>View Results</a><br>
     
    </div>

  </div><!--/container-->
</main><!--/Main layout-->
<?php break; ?>
<?php endswitch; ?>
<?php else: ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-danger alert-dismissible fade show' role='alert'>
    <h4>Forbidden.</h4>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->


<?php endif ?>


<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>
</body>
</html>

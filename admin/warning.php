<?php
    session_start();
    if(isset($_SESSION['admin'])){
      header('location:home.php');
    }
?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition login-page">

<main class="animated fadeInDown delay-1s">
<!--Grid container-->
<div class="container">
<!--Grid row-->
<div class="row py-5">

<!-- Default form subscription -->
<form class="text-center border border-light p-5" action="#!">

    <p class="h4 mb-4">Subscribe</p>

    <p>Join our mailing list. We write rarely, but only the best content.</p>

    <p>
        <a href="" target="_blank">See the last newsletter</a>
    </p>

    <!-- Name -->
    <input type="text" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="Name">

    <!-- Email -->
    <input type="email" id="defaultSubscriptionFormEmail" class="form-control mb-4" placeholder="E-mail">

    <!-- Sign in button -->
    <button class="btn btn-info btn-block" type="submit">Sign in</button>


</form>
<!-- Default form subscription -->

</div><!--Row--> 
</div><!--Container-->
</main><!--/Main layout-->

<?php include 'includes/scripts.php' ?>
</body>
</html>
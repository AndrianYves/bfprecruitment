<header>

  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
    <div class="container-fluid">

      <!-- Brand -->
      <a class="navbar-brand waves-effect" href="index.php">
        <strong class="blue-text">BFP-CAR Recruit</strong>
      </a>

      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <!-- Left -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item <?php if($current == 'home') {echo 'active';} ?>">
            <a class="nav-link waves-effect" href="home.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
        </ul>

        <!-- Right -->
        <ul class="navbar-nav nav-flex-icons">
          <li class="nav-item">
            <a class="nav-link waves-effect waves-light"><i class="fa fa-user" aria-hidden="true"></i> <span class="clearfix d-none d-sm-inline-block"><?php echo ucwords($user['name']). ' '.ucwords($user['lastname']); ?></span></a>
          </li>
<!--           <li class="nav-item">
            <a class="nav-link waves-effect waves-light" href=""><i class="fas fa-comments"></i> <span class="clearfix d-none d-sm-inline-block">Support</span></a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link waves-effect waves-light" href="logout.php"><i class="fas fa-sign-out-alt"></i> <span class="clearfix d-none d-sm-inline-block">Sign Out</span></a>
          </li>
        </ul>

      </div>

    </div>
  </nav>
  <!-- Navbar -->

  <!-- Sidebar -->
  <div class="sidebar-fixed position-fixed">

<!--         <div class="text-center">
            <img style="height:100px;width:100px" alt="" src="logo.png">
        </div> -->
    <div class="list-group list-group-flush">
            <a href="home.php" class="list-group-item list-group-item-action <?php if($current == 'home') {echo 'active';} ?> waves-effect">Dashboard</a>
<?php 
switch ($role): ?>
<?php case "Medical Examiner": ?>
<a href="med.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'Medical/Dental and Physical Examination') {echo 'active';} ?>">
  Medical/Dental and Physical Examination</a>
        <a href="settings.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'settings') {echo 'active';} ?>">
       Settings</a>
<?php break; ?>
<?php case "Written Exam Handler": ?>
<a href="ppt.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'Written Exam Handler') {echo 'active';} ?>">
  Pen and Paper Test</a>
        <a href="settings.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'settings') {echo 'active';} ?>">
       Settings</a>
<?php break; ?>
<?php case "Handler": ?>
<a href="pat.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'Physical Agility Test') {echo 'active';} ?>">
       Physical Agility Test</a>
             <a href="settings.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'settings') {echo 'active';} ?>">
       Settings</a>
<?php break; ?>
<?php case "Panel Interviewer": ?>
      <a href="interview.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'Panel Interviewer') {echo 'active';} ?>">
       Panel Interview</a>
             <a href="settings.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'settings') {echo 'active';} ?>">
       Settings</a>
<?php break; ?>
<?php case "Background Investigator": ?>
      <a href="cbi.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'Background Investigator') {echo 'active';} ?>">
       Background Investigation</a>
             <a href="settings.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'settings') {echo 'active';} ?>">
       Settings</a>
<?php break; ?>
<?php case "Neurological Examiner": ?>
      <a href="neuro.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'Neurological Examiner') {echo 'active';} ?>">
       Neuro-Psychiatric Examination and Drug Test</a>
             <a href="settings.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'settings') {echo 'active';} ?>">
       Settings</a>
<?php break; ?>
<?php case "Super User": ?>
      <a href="settings.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'settings') {echo 'active';} ?>">
       Settings</a>
<?php break; ?>
<?php default: ?>
<!--       <a href="driver.php" class="list-group-item list-group-item-action waves-effect <?php if($current == 'Trade Test Driving') {echo 'active';} ?>">
       Trade Test Driving</a> -->
      <a href="applicants.php" class="list-group-item text-sm-left list-group-item-action waves-effect <?php if($current == 'Applicants') {echo 'active';} ?>">
       Current applicant results</a>
      <a href="registration.php" class="list-group-item text-sm-left list-group-item-action waves-effect <?php if($current == 'Register') {echo 'active';} ?>">
       Register new applicant</a>
      <a href="evaluation.php" class="list-group-item text-sm-left list-group-item-action waves-effect <?php if($current == 'Evaluation') {echo 'active';} ?>">
       Evaluation</a>
      <a href="med.php" class="list-group-item text-sm-left list-group-item-action waves-effect <?php if($current == 'Medical/Dental and Physical Examination') {echo 'active';} ?>">
       Medical/Dental and Physical Examination</a>
      <a href="ppt.php" class="list-group-item text-sm-left list-group-item-action waves-effect <?php if($current == 'Written Exam Handler') {echo 'active';} ?>">
       Pen and Paper Test</a>
      <a href="pat.php" class="list-group-item text-sm-left list-group-item-action waves-effect <?php if($current == 'Physical Agility Test') {echo 'active';} ?>">
       Agility Test</a>
      <a href="interview.php" class="list-group-item text-sm-left list-group-item-action waves-effect <?php if($current == 'Panel Interviewer') {echo 'active';} ?>">
       Panel Interview</a>
      <a href="cbi.php" class="list-group-item text-sm-left list-group-item-action waves-effect <?php if($current == 'Background Investigator') {echo 'active';} ?>">
       Background Investigation</a>
      <a href="neuro.php" class="list-group-item text-sm-left list-group-item-action waves-effect <?php if($current == 'Neurological Examiner') {echo 'active';} ?>">
       Neuro-Psychiatri Examination and Drug Test</a>
      <a href="finalresults.php" class="list-group-item text-sm-left list-group-item-action waves-effect <?php if($current == 'Final Results') {echo 'active';} ?>">
       Final Results</a>
<?php endswitch; ?>

    </div>

  </div>
  <!-- Sidebar -->

</header>
<!--Main Navigation-->
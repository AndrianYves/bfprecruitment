<?php
	session_start();
	include 'includes/conn.php';;

	if(isset($_SESSION['admin'])){
		$query = $conn -> prepare("SELECT * FROM admins WHERE id = ? ");
		$query -> bind_param("s", $variable);
		$variable = $_SESSION['admin'];
		$query -> execute();
		$result = $query->get_result();
		$user = mysqli_fetch_assoc($result);
		$role = $user['role'];

		$today = date('Y-m-d');

		$sqldate1 = $conn->prepare("SELECT recruitmentbatch, submissionofformenddate, submissionofformstatus FROM settings where id = ?");
		$sqldate1->bind_param("s", $id);
		$id = '1';
		$sqldate1->execute();
		$query = $sqldate1->get_result();
		$row1 = $query->fetch_assoc();
		$recruitmentDate = $row1['recruitmentbatch'];
		$submissiondate = $row1['submissionofformenddate'];
		$submissionstatus = $row1['submissionofformstatus'];

		if(strtotime($today) > strtotime($submissiondate)){
			$submissionofformstatus = 'STOP';
			$settingsid = '1';

			$result4 = $conn->prepare("UPDATE settings SET submissionofformstatus = ? where id = ?");
			$result4->bind_param("si", $submissionofformstatus, $settingsid);
			$result4->execute(); 
		}


	} else {
		header('location: index.php');
		exit();
	}
	
?>
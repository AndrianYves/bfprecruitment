<!--   <script type="text/javascript" src="js/compiled-4.8.8.min.js" ></script> -->

 <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Initializations -->
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();

  </script>

      <script type="text/javascript">$(document).ready(function() {
    $('table.display').DataTable();
} );
    </script>

  <!-- DataTables JS -->
  <script src="js/addons/datatables.min.js" type="text/javascript"></script>

  <!-- DataTables Select JS -->
  <script src="js/addons/datatables-select.min.js" type="text/javascript"></script>

<script>
$("#selectall").click(function () {
$('input:checkbox').not(this).prop('checked', this.checked);
});
</script>

<script type="text/javascript">
$("td").click(function(e) {
    var checkbox = $(':checkbox', $(this).parent()).get(0);
    var checked = checkbox.checked;
    if (checked == false) checkbox.checked = true;
    else checkbox.checked = false;
});
</script>



<script type="text/javascript">
  // Tooltips Initialization
$(function () {
$('[data-toggle="tooltip"]').tooltip()
})
</script>
<script type="text/javascript">
  $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
});

// Acá guarda el index al cual corresponde la tab. Lo podés ver en el dev tool de chrome.
var activeTab = localStorage.getItem('activeTab');

// En la consola te va a mostrar la pestaña donde hiciste el último click y lo
// guarda en "activeTab". Te dejo el console para que lo veas. Y cuando refresques
// el browser, va a quedar activa la última donde hiciste el click.
console.log(activeTab);

if (activeTab) {
   $('a[href="' + activeTab + '"]').tab('show');
}
</script>

<script type="text/javascript">
  (function($) {
  $.fn.uncheckableRadio = function() {
    var $root = this;
    $root.each(function() {
      var $radio = $(this);
      if ($radio.prop('checked')) {
        $radio.data('checked', true);
      } else {
        $radio.data('checked', false);
      }
        
      $radio.click(function() {
        var $this = $(this);
        if ($this.data('checked')) {
          $this.prop('checked', false);
          $this.data('checked', false);
          $this.trigger('change');
        } else {
          $this.data('checked', true);
          $this.closest('form').find('[name="' + $this.prop('name') + '"]').not($this).data('checked', false);
        }
      });
    });
    return $root;
  };
}(jQuery));

$('[type=radio]').uncheckableRadio();
$('button').click(function() {
  $('[value=V2]').prop('checked', true).trigger('change').trigger('click');
});
</script>

<script>
$(document).ready(function(){
  $('a[data-toggle="pill"]').on('show.bs.tab', function(e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
  });
  var activeTab = localStorage.getItem('activeTab');
  if(activeTab){
    $('#pills-tab a[href="' + activeTab + '"]').tab('show');
  }
});
</script>

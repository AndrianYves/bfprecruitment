<?php
$pageTitle = "Physical Agility Test";
$page      = "pat.php";
$title     = "Physical Agility Test";

$current = "Physical Agility Test";
?>

<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<?php
  if(strtotime($today) < strtotime($submissiondate) && $submissionstatus == 'START'){
    $_SESSION['error'][] = 'Submission of form still ongoing.';
    header('location: home.php');
  }

  if($submissionstatus == 'PAUSED'){
    $_SESSION['error'][] = 'Submission of form still ongoing but temporarily paused.';
    header('location: home.php');
  }
  
  $result9 = mysqli_query($conn, "SELECT status FROM `tests_status` where `test` = 'Physical Agility Test'");
  $row9 = mysqli_fetch_assoc($result9);
  $testStatus = $row9['status'];

  if($testStatus != 'DONE'){
    $_SESSION['error'][] = 'Physical Agility Test not yet finished.';
    header('location: pat.php');
  }

  if (isset($_POST['edit'])) {
    mysqli_autocommit($conn, false);
    $timestamp = date("Y-m-d H:i:s");
    $allowed = array('PASSED', 'FAILED');
    $error = false;

    $adminusername=$_POST['adminusername'];
    // $sql1 = mysqli_query($db, "SELECT * FROM admins where `id` = '$id'");
    // $row = mysqli_fetch_assoc($sql1);
    $password=$_POST['password'];

    $sql1 = $conn->prepare("SELECT * FROM admins where username = ?");
    $sql1->bind_param("s", $adminusername);
    $sql1->execute();
    $query = $sql1->get_result();
    $row = mysqli_fetch_assoc($query);

    if($query->num_rows < 1){
      $error = true;
      $_SESSION['error'][] = 'Invalid Username/Password';
    } else {
        if (!password_verify($password, $row['password'])) {
          $error = true;
          $_SESSION['error'][] = 'Invalid Username/Password';
        } 


    }

$total = $pull = $push = $sits = $endurance = $diameterhose = $kilogramshose = "";

    if(empty($_POST['epull'])) {
      $error = true;
      $_SESSION['error'][] = 'sits entered required.';
    }else if(!is_numeric($_POST['epull'])) {
      $error = true;
      $_SESSION['error'][] = 'sits entered was not numeric.';
    } else {
      $pull = $_POST['epull'];
    } 

    if(empty($_POST['epush'])) {
      $error = true;
      $_SESSION['error'][] = 'push entered required.';
    }else if(!is_numeric($_POST['epull'])) {
      $error = true;
      $_SESSION['error'][] = 'push entered was not numeric.';
    } else {
      $push = $_POST['epush'];
    }

    if(empty($_POST['esits'])) {
      $error = true;
      $_SESSION['error'][] = 'pull entered required.';
    } else if(!is_numeric($_POST['esits'])) {
      $error = true;
      $_SESSION['error'][] = 'pull entered was not numeric.';
    } else {
      $sits = $_POST['esits'];
    } 

    if(empty($_POST['eendurance'])) {
      $error = true;
      $_SESSION['error'][] = 'sits entered required.';
    }else if(!is_numeric($_POST['eendurance'])) {
      $error = true;
      $_SESSION['error'][] = 'sits entered was not numeric.';
    } else {
      $endurance = $_POST['eendurance'];
    } 

    if(empty($_POST['ediameterhose'])) {
      $error = true;
      $_SESSION['error'][] = 'push entered required.';
    }else if(!is_numeric($_POST['ediameterhose'])) {
      $error = true;
      $_SESSION['error'][] = 'push entered was not numeric.';
    } else {
      $diameterhose = $_POST['ediameterhose'];
    } 

    if(empty($_POST['ekilogramshose'])) {
      $error = true;
      $_SESSION['error'][] = 'pull entered required.';
    } else if(!is_numeric($_POST['ekilogramshose'])) {
      $error = true;
      $_SESSION['error'][] = 'pull entered was not numeric.';
    } else {
      $kilogramshose = $_POST['ekilogramshose'];
    }

    if (empty($_POST['applicantID'])) {
      $error = true;
      $_SESSION['error'][] = 'Error. Somethings missing.';
    } else {
      $applicantID = $_POST['applicantID'];
    }

    $total = $pull + $push + $sits + $endurance + $diameterhose + $kilogramshose;

    if ($pull > '10' || $push > '10' || $sits > '10' || $endurance > '20' || $diameterhose > '25' || $kilogramshose > '23'){
      $error = true;
      $_SESSION['error'][] = 'Score Exceeded';
    } else {
      if ($total >= '75') {
        $status = 'PASSED';
        $result = $conn->prepare("INSERT INTO test_interview(recruitmentDate, applicantID) VALUES(?, ?)");
        $result->bind_param("si", $recruitmentDate, $applicantID);
        $result->execute();
      } else {
        $status = 'FAILED';
        $result = $conn->prepare("DELETE FROM test_interview WHERE applicantID = ?");
        $result->bind_param("i", $applicantID);
        $result->execute();
        $result->close();

        $result = $conn->prepare("DELETE FROM test_investigator WHERE applicantID = ?");
        $result->bind_param("i", $applicantID);
        $result->execute();
        $result->close();

        $result = $conn->prepare("DELETE FROM test_neuro WHERE applicantID = ?");
        $result->bind_param("i", $applicantID);
        $result->execute();
        $result->close();
      }

      $result1 = $conn->prepare("UPDATE test_pat SET pull = ?, push = ?, sits = ?, endurance = ?, diameterhose = ?, kilogramshose = ?, total = ?, status = ?, timestamp = ?, adminID = ? WHERE applicantID = ?");
      $result1->bind_param("iiiiiiissii", $pull, $push, $sits, $endurance, $diameterhose, $kilogramshose, $total, $status, $timestamp, $adminID, $applicantID);
      $result1->execute();
    }

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Scores Updated';
    } else {
      mysqli_rollback($conn);
    }

  }
?>
<body class="grey lighten-3">
  <?php include 'includes/nav.php'; ?>

<?php if ($role == 'Handler' || $role == 'HR Officer'): ?>

<!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
      <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fas fa-exclamation-triangle'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>

      <?php
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fas fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>


<?php 
$result1 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_pat on applicants_pds.id = test_pat.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate'");

  $sql2 = mysqli_query($conn, "SELECT * FROM `test_pat` join applicants_pds on applicants_pds.id = test_pat.applicantID where applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");

?>
    <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Results</span>
          </h4>
          <h4 class="mb-2 mb-sm-0 pt-1 text-right"><?php echo mysqli_num_rows($result1);?> passed out of <?php echo mysqli_num_rows($sql2);?> applicants</h4>
        </div>
      </div>
      <!-- Heading -->

 <div class="row">
        <div class="col-md-12">
          <div class="card">
            <!--Card content-->
            <div class="card-body">
        <ul class="nav nav-pills mb-3 justify-content-end" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="btn-success nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
      aria-controls="pills-home" aria-selected="true">PASSED</a>
  </li>
  <li class="nav-item">
    <a class="btn-danger nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
      aria-controls="pills-profile" aria-selected="false">FAILED</a>
  </li>
  <li class="nav-item">
    <a class="btn-primary nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab"
      aria-controls="pills-contact" aria-selected="false">ALL</a>
  </li>
</ul>
<div class="tab-content pt-2 pl-1" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
    <table class="table table-bordered display table-responsive-md" cellspacing="0" width="100%">
                <thead>
                  <th width="10">Applicant #</th>
                  <th width="250">Full Name</th>
                  <th width="150">Total Score</th>
                  <th width="150">Status</th>
                  <th width="100">Last Edited</th>
                  <th width="50">View Profile</th>
                  <th width="50">Action</th>
                </thead>
                <tbody>

                  <?php
                    $result1 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_pat on applicants_pds.id = test_pat.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate'");
                    while ($row = mysqli_fetch_array($result1)) {

                      ?>
                        <tr>
                          <td><?php echo $row['applicantnumber']; ?></td>
                          <td><?php echo ucwords($row['last']); ?>, <?php echo ucwords($row['name']); ?> <?php echo ucwords($row['middle']); ?></td>
                          <td><?php echo $row['total']; ?></td>
                          <td><span class='badge badge-pill badge-success'><?php echo $row['status']; ?></span></td>
                          <td><?php echo date("Y-m-d H:i", strtotime($row['timestamp']));?></td>
                          <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>

                           <td>
                           <div class='text-center'><a data-toggle='modal' data-target='#edit<?php echo $row['applicantID']; ?>' href='#edit?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-edit blue-text'></i></a></div>
                          </td>
                        </tr>
            <div class="modal fade" id="edit<?php echo $row['applicantID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold"><?php echo $row['applicantID'];?> - <?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?>
                    </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                    </button>
                  </div>
                  <div class="modal-body mx-3">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                      <div class="text-center">
                        <input type="hidden" name="applicantID" class="form-control validate" value="<?php echo $row['applicantID']; ?>" required>
                        
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epull" name="epull" class="form-control validate" value="<?php echo $row['pull']; ?>" required>
                        <label for="pull">Pull-Ups/Full Arms Hanging Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epush" name="epush" class="form-control validate" value="<?php echo $row['push']; ?>" required>
                        <label for="push">Push-Ups Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="esits" name="esits" class="form-control validate" value="<?php echo $row['sits']; ?>" required>
                        <label for="birthplace">Sit-Ups Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="esits" name="eendurance" class="form-control validate" value="<?php echo $row['endurance']; ?>" required>
                        <label for="birthplace">Endurance Run Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="esits" name="ediameterhose" class="form-control validate" value="<?php echo $row['diameterhose']; ?>" required>
                        <label for="birthplace">Diameter Hose Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="esits" name="ekilogramshose" class="form-control validate" value="<?php echo $row['kilogramshose']; ?>" required>
                        <label for="birthplace">Kilograms Hose Score</label>
                        </div>

                        <label>Super User Account</label>
                        <input type="text" name="adminusername" class="form-control mb-4 validate" placeholder="Username" required>
                            <input type="password" name="password" class="form-control mb-4" placeholder="Password" required>
                            
                                
                      </div>
                      <div class="modal-footer d-flex justify-content-center">
                        <button name="edit" class="btn btn-success">Update
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php    } ?>
                
                </tbody>
              </table>
  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        <table class="table table-bordered display table-responsive-md" cellspacing="0" width="100%">
                <thead>
                 <th width="10">Applicant #</th>
                  <th width="250">Full Name</th>
                  <th width="150">Total Score</th>
                  <th width="150">Status</th>
                  <th width="100">Last Edited</th>
                  <th width="50">View Profile</th>
                  <th width="50">Action</th>
                </thead>
                <tbody>

                  <?php
                    $result2 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_pat on applicants_pds.id = test_pat.applicantID where `status` = 'FAILED' and applicants_pds.recruitmentDate = '$recruitmentDate'");
                    while ($row = mysqli_fetch_array($result2)) {
                      ?>
                        <tr>
                          <td><?php echo $row['applicantnumber']; ?></td>
                          <td><?php echo ucwords($row['last']); ?>, <?php echo ucwords($row['name']); ?> <?php echo ucwords($row['middle']); ?></td>
                          <td><?php echo $row['total']; ?></td>
                          <td><span class='badge badge-pill badge-danger'><?php echo $row['status']; ?></span></td>
                          <td><?php echo date("Y-m-d H:i", strtotime($row['timestamp']));?></td>
                          <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>

                           <td>
                           <div class='text-center'><a data-toggle='modal' data-target='#FAILEDit<?php echo $row['applicantID']; ?>' href='#FAILEDit?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-edit blue-text'></i></a></div>
                          </td>
                        </tr>
            <div class="modal fade" id="FAILEDit<?php echo $row['applicantID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold"><?php echo $row['applicantID'];?> - <?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?>
                    </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                    </button>
                  </div>
                  <div class="modal-body mx-3">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                      <div class="text-center">
                        <input type="hidden" name="applicantID" class="form-control validate" value="<?php echo $row['applicantID']; ?>" required>
                        
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epull" name="epull" class="form-control validate" value="<?php echo $row['pull']; ?>" required>
                        <label for="pull">Pull-Ups/Full Arms Hanging Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epush" name="epush" class="form-control validate" value="<?php echo $row['push']; ?>" required>
                        <label for="push">Push-Ups Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="esits" name="esits" class="form-control validate" value="<?php echo $row['sits']; ?>" required>
                        <label for="birthplace">Sit-Ups Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="esits" name="eendurance" class="form-control validate" value="<?php echo $row['endurance']; ?>" required>
                        <label for="birthplace">Endurance Run Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="esits" name="ediameterhose" class="form-control validate" value="<?php echo $row['diameterhose']; ?>" required>
                        <label for="birthplace">Diameter Hose Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="esits" name="ekilogramshose" class="form-control validate" value="<?php echo $row['kilogramshose']; ?>" required>
                        <label for="birthplace">Kilograms Hose Score</label>
                        </div>
                        <label>Super User Account</label>
                        <input type="text" name="adminusername" class="form-control mb-4 validate" placeholder="Username" required>
                            <input type="password" name="password" class="form-control mb-4" placeholder="Password" required>
                            
                                
                      </div>
                      <div class="modal-footer d-flex justify-content-center">
                        <button name="edit" class="btn btn-success">Update
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php    } ?>
                </tbody>
              </table>
  </div>
  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
        <table class="table table-bordered display table-responsive-md" cellspacing="0" width="100%">
                <thead>
                 <th width="10">Applicant #</th>
                  <th width="250">Full Name</th>
                  <th width="150">Total Score</th>
                  <th width="150">Status</th>
                  <th width="100">Last Edited</th>
                  <th width="50">View Profile</th>
                  <th width="50">Action</th>
                </thead>
                <tbody>

                  <?php
                    $result3 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_pat on applicants_pds.id = test_pat.applicantID where applicants_pds.recruitmentDate = '$recruitmentDate'");
                     while ($row = mysqli_fetch_array($result3)) {
                      if ($row['status'] == 'PASSED'){
                        $color = 'success';
                      } else {
                        $color = 'danger';
                      }

                      ?>
                        <tr>
                          <td><?php echo $row['applicantnumber']; ?></td>
                          <td><?php echo ucwords($row['last']); ?>, <?php echo ucwords($row['name']); ?> <?php echo ucwords($row['middle']); ?></td>
                          <td><?php echo $row['total']; ?></td>
                          <td><span class='badge badge-pill badge-<?php echo $color; ?>'><?php echo $row['status']; ?></span></td>
                          <td><?php echo date("Y-m-d H:i", strtotime($row['timestamp']));?></td>
                          <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>

                           <td>
                           <div class='text-center'><a data-toggle='modal' data-target='#alledit<?php echo $row['applicantID']; ?>' href='#alledit?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-edit blue-text'></i></a></div>
                          </td>
                        </tr>
            <div class="modal fade" id="alledit<?php echo $row['applicantID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold"><?php echo $row['applicantID'];?> - <?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?>
                    </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                    </button>
                  </div>
                  <div class="modal-body mx-3">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                      <div class="text-center">
                        <input type="hidden" name="applicantID" class="form-control validate" value="<?php echo $row['applicantID']; ?>" required>
                        
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epull" name="epull" class="form-control validate" value="<?php echo $row['pull']; ?>" required>
                        <label for="pull">Pull-Ups/Full Arms Hanging Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="epush" name="epush" class="form-control validate" value="<?php echo $row['push']; ?>" required>
                        <label for="push">Push-Ups Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="esits" name="esits" class="form-control validate" value="<?php echo $row['sits']; ?>" required>
                        <label for="birthplace">Sit-Ups Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="esits" name="eendurance" class="form-control validate" value="<?php echo $row['endurance']; ?>" required>
                        <label for="birthplace">Endurance Run Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="esits" name="ediameterhose" class="form-control validate" value="<?php echo $row['diameterhose']; ?>" required>
                        <label for="birthplace">Diameter Hose Score</label>
                        </div>
                        <div class="md-form mt-0">
                        <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" id="esits" name="ekilogramshose" class="form-control validate" value="<?php echo $row['kilogramshose']; ?>" required>
                        <label for="birthplace">Kilograms Hose Score</label>
                        </div>
                        <label>Super User Account</label>
                        <input type="text" name="adminusername" class="form-control mb-4 validate" placeholder="Username" required>
                            <input type="password" name="password" class="form-control mb-4" placeholder="Password" required>
                            
                                
                      </div>
                      <div class="modal-footer d-flex justify-content-center">
                        <button name="edit" class="btn btn-success">Update
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php    } ?>
                
                </tbody>
              </table>
  </div>
</div>
      <form method="POST" action="export.php" enctype="multipart/form-data">

 <button type="submit" name="exportpatresults" class="btn btn-success"><i class='fas fa-download'></i> Generate CSV
                        </button>
</form>

            </div>

          </div>
        </div>
      </div>

  </div><!--/container-->
</main><!--/Main layout-->
<?php else: ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-danger alert-dismissible fade show' role='alert'>
    <h4>Forbidden.</h4>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->


<?php endif ?>

<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>
</body>
</html>
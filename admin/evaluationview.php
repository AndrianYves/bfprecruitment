<?php
$page      = "evaluation.php";
$title     = "Evaluation";
$current = "home";
?>
<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>

<?php
$previous = "javascript:history.go(-1)";
if(isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}

  $id = $_GET['id'];
  
  //   // define the path and name of cached file
  // $cachefile = 'cached/applicants/'.$id.'.php';
  // // Check if the cached file is still fresh. If it is, serve it up and exit.
  // if (file_exists($cachefile)) {
  //   include($cachefile);
  //     exit;
  // }
  // // if there is either no file OR the file to too old, render the page and capture the HTML.
  // ob_start();

  $result = mysqli_query($conn, "SELECT * FROM applicants_pds where id = '$id'");
  

    while($row = mysqli_fetch_array($result)){
    $last = $row['last'];
    $name = $row['name'];
    $middle = $row['middle'];
    $age = $row['age'];
    $birthday = $row['birthday'];
    $birthplace = $row['birthplace'];
    $address = $row['address'];
    $sex = $row['sex'];
    $contact = $row['contact'];
    $email = $row['email'];
    $socialmedia = $row['socialmedia'];
    $image = $row['image'];
    $applicantnumber = $row['applicantnumber'];
    $skills = $row['skills'];
    $civilstatus = $row['civilstatus'];


  }
  


  ?>

<body class="grey lighten-3">
  <?php include 'includes/nav.php'; ?>

<?php if ($role == 'Medical Examiner' || $role == 'HR Officer'): ?>

<!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">


    <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Profile</span>
          </h4>
          <h4 class="mb-2 mb-sm-0 pt-1"><a class="text-right" href="<?= $previous ?>">Back</a></h4>
        </div>
      </div>
      <!-- Heading -->

      <div class="row">
        <div class="col-md-4">
          <!-- Card -->
<div class="card booking-card">
  <!-- Card image -->
  <div class="view overlay">
    <?php if(!empty($image)): ?>
    <img class="card-img-top img-thumbnail" src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/photo.<?php pathinfo($image, PATHINFO_EXTENSION);?>" alt="Card image cap">
  <?php else: ?>
    <img class="card-img-top img-thumbnail" src="img/stock.jpg" alt="Card image cap">
  <?php endif ?>


    <a href="#!">
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>

  <!-- Card content -->
  <div class="card-body">

    <!-- Title -->
    <h4 class="card-title font-weight-bold"><a><?php echo ucwords($name);?> <?php echo ucwords($middle);?> <?php echo ucwords($last);?></a></h4>
    <p><strong>Age:</strong> <?php echo $age;?></p>
    <p><strong>Birthday:</strong> <?php echo date("F d, Y", strtotime($birthday));?></p>
    <p><strong>Birthplace:</strong> <?php echo ucwords($birthplace);?></p>
    <p><strong>Address:</strong> <?php echo ucwords($address);?></p>
    <p><strong>Gender:</strong> <?php echo $sex;?></p>
    <p><strong>Civil Status:</strong> <?php echo $civilstatus;?></p>
    <p><strong>Contact:</strong> <?php echo $contact;?></p>
    <p><strong>Email:</strong> <?php echo $email;?></p>
    <p <?php if(empty($socialmedia)){ echo 'hidden'; }?>><strong>Facebook:</strong><a href="<?php echo $socialmedia;?>" target="_blank">
                <i class="fab fa-facebook-f"></i>
                </a></p>
  </div>

</div>
<!-- Card -->
        </div><!--/column-->
    <?php    
                        $result1 = mysqli_query($conn, "SELECT * FROM applicants_folders where applicantID = '$id'");
                    while ($row = mysqli_fetch_array($result1)) {
                          $applicationLetter = $row['applicationLetter'];
                          $serviceRecord = $row['serviceRecord'];  
                          $transcript = $row['transcript'];   
                          $collegeDiploma = $row['collegeDiploma'];   
                          $secondEligibility = $row['secondEligibility'];  
                          $birthCertificate = $row['birthCertificate'];   
                          $marriageCertificate = $row['marriageCertificate'];  
                          $barangayClearance = $row['barangayClearance'];
                          $mtcClearance = $row['mtcClearance'];  
                          $nbiClearance = $row['nbiClearance'];   
                          $policeClearance = $row['policeClearance'];  
                          $rtcClearance = $row['rtcClearance'];  
                          $waiverCertificate = $row['waiverCertificate'];   
                          $tesdaDriverCertificate = $row['tesdaDriverCertificate'];  
                          
                        }?>
         <div class="col-md-8">
          <div class="card">
            <!--Card content-->
            <div class="card-body">

              <table id="dtBasicExample" class="table table-bordered table-responsive-md" cellspacing="0" width="100%">
                <tbody>
                  <tr>
                    <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#applicationLetter">Application Letter</button></td>
                    <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#serviceRecord">Authenticated Service Record</button></td>
                    <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#transcript">Transcript of Records</button></td>
                  </tr>
                  <tr>
                    <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#collegeDiploma">College Diploma</button></td>
                    <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#secondEligibility">Second Level Eligibility</button></td>
                    <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#birthCertificate">Certificate of Live Birth</button></td>
                  </tr>
                  <tr>
                    <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#rtcClearance">RTC Clearance</button></td>
                    <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#barangayClearance">Barangay Clearance</button></td>
                    <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mtcClearance">MTC Clearance</button></td>
                  </tr>
                    <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#nbiClearance">NBI Clearance</button></td>
                    <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#policeClearance">Police Clearance</button></td>
                    <td><button <?php if(empty($marriageCertificate)){ echo 'hidden'; }?> type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#marriageCertificate">Certificate of Marriage</button></td>
                  <tr>
                    <td><button <?php if(empty($waiverCertificate)){ echo 'hidden'; }?> type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#waiverCertificate">Certificate of Waiver</button></td>
                    <td><button <?php if(empty($tesdaDriverCertificate)){ echo 'hidden'; }?> type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#tesdaDriverCertificate">TESDA Driver Certificate</button></td>
                  </tr>





                
                </tbody>
              </table>
            </div>
          </div>
        </div>


      </div><!--/row-->

  </div><!--/container-->
</main><!--/Main layout-->
<!-- Central Modal Small -->
<div class="modal fade" id="applicationLetter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Application Letter</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/Application Letter.<?php pathinfo($applicationLetter, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<!-- Central Modal Small -->
<div class="modal fade" id="serviceRecord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Authenticated Service Record</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/Service Record.<?php pathinfo($serviceRecord, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->


<!-- Central Modal Small -->
<div class="modal fade" id="transcript" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Transcript of Records</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/Transcript.<?php pathinfo($transcript, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<!-- Central Modal Small -->
<div class="modal fade" id="collegeDiploma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">College Diploma</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/College Diploma.<?php pathinfo($collegeDiploma, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->


<!-- Central Modal Small -->
<div class="modal fade" id="secondEligibility" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Second Level Eligibility (Issued from CSC/PRC)</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/Second Eligibility.<?php pathinfo($secondEligibility, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->


<!-- Central Modal Small -->
<div class="modal fade" id="birthCertificate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Certificate of Live Birth (Issued by PSA)</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/Birth Certificate.<?php pathinfo($birthCertificate, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->


<!-- Central Modal Small -->
<div class="modal fade" id="marriageCertificate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Certificate of Marriage (If Applicable)</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/Marriage Certificate.<?php pathinfo($marriageCertificate, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->


<!-- Central Modal Small -->
<div class="modal fade" id="barangayClearance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Barangay Clearance</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/Barangay Clearance.<?php pathinfo($barangayClearance, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->


<!-- Central Modal Small -->
<div class="modal fade" id="mtcClearance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">MTC Clearance</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/MTC Clearance.<?php pathinfo($mtcClearance, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->


<!-- Central Modal Small -->
<div class="modal fade" id="nbiClearance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">NBI Clearance</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/NBI Clearance.<?php pathinfo($nbiClearance, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->


<!-- Central Modal Small -->
<div class="modal fade" id="policeClearance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Police Clearance</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/Police Clearance.<?php pathinfo($policeClearance, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<!-- Central Modal Small -->
<div class="modal fade" id="rtcClearance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">RTC Clearance</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/RTC Clearance.<?php pathinfo($rtcClearance, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->


<!-- Central Modal Small -->
<div class="modal fade" id="waiverCertificate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Certificate of Waiver</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/Waiver Certificate.<?php pathinfo($waiverCertificate, PATHINFO_EXTENSION);?>" class="img-fluid" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<!-- Central Modal Small -->
<div class="modal fade" id="tesdaDriverCertificate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">TESDA Driver Certificate</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../applicationforms/Current/<?php echo $id;?> - <?php echo $last;?>, <?php echo $name;?>/Tesda Driver Certificate .<?php pathinfo($tesdaDriverCertificate, PATHINFO_EXTENSION);?>" alt="Responsive image">

        
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<?php endif ?>

<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>

</body>
</html>

<!-- <?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);
  // We're done! Save the cached content to a file
  $fp = fopen($cachefile, 'w');
  fwrite($fp, ob_get_contents());
  fclose($fp);
  // finally send browser output
  ob_end_flush();

?> -->
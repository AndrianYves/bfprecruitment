<?php
  $page      = "evaluation.php";
  $title     = "Register";
  $current = "Applicants";
  ?>
<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<?php
$_SESSION['previous'] = "javascript:history.go(-1)";
if(isset($_SERVER['HTTP_REFERER'])) {
    $_SESSION['previous'] = $_SERVER['HTTP_REFERER'];
}

  if (isset($_POST['submit'])) {
    $conn->autocommit(FALSE);
    $error = false;
    $allowed = array('image/JPEG', 'image/jpeg', 'image/JPG', 'image/jpg', 'image/X-PNG', 'image/PNG', 'image/png', 'image/x-png');

    $applicantid = test_input(strtolower($_POST['applicantGetID']));

   if (empty($_POST['name'])) {
      $error = true;
      $_SESSION['error'][] = 'Firstname is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['name'])) {
      $error = true;
      $_SESSION['error'][] = 'Firstname is invalid.';
    } else {
      $name = test_input(strtolower($_POST['name']));
    }

     if (empty($_POST['middle'])) {
      $error = true;
      $_SESSION['error'][] = 'Middlename is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['middle'])) {
      $error = true;
      $_SESSION['error'][] = 'Middlename is invalid.';
    } else {
      $middle = test_input(strtolower($_POST['middle']));
    }

     if (empty($_POST['last'])) {
      $error = true;
      $_SESSION['error'][] = 'Lastname is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['last'])) {
      $error = true;
      $_SESSION['error'][] = 'Lastname is invalid.';
    } else {
      $last = test_input(strtolower($_POST['last']));
    }
  
    
    if (!empty($_FILES['image']['name'])) {
      if ($_FILES['image']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = '2x2 photo size must be less than 2MB.';
      }

      if (in_array($_FILES['image']['type'], $allowed)){
        $image = $_FILES['image']['name'];
        mkdir('../applicationforms/Current/'.$applicantid. ' - '.$last. ', '.$name, 0777, true);
        move_uploaded_file($_FILES["image"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'photo.'.pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION));
        $result = $conn->prepare("UPDATE applicants_pds SET image = ? where id = ?");
    $result->bind_param("si", $image, $applicantid);
    $result->execute();
    $result->close();
      } else {
        $error = true;
        $_SESSION['error'][] = '2x2 photo must be png and jpg only.';
      }
    } 

    

    if (empty($_POST['birthday'])) {
      $error = true;
      $_SESSION['error'][] = 'Birthday is required.';
    } else {
      $birthday = $_POST['birthday'];
      $age = floor((time() - strtotime($birthday)) / 31556926);
    }

    if (empty($_POST['address'])) {
      $error = true;
      $_SESSION['error'][] = 'Address is required.';
    } else {
      $address = test_input(strtolower($_POST['address']));
    }

   if (empty($_POST['sex'])) {
      $error = true;
      $_SESSION['error'][] = 'Gender is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['sex'])) {
      $error = true;
      $_SESSION['error'][] = 'Gender is invalid.';
    } else {
      $sex = test_input(strtolower($_POST['sex']));
    }

    if (empty($_POST['civilstatus'])) {
      $error = true;
      $_SESSION['error'][] = 'Civilstatus is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['civilstatus'])) {
      $error = true;
      $_SESSION['error'][] = 'Civilstatus is invalid.';
    } else {
      $civilstatus = test_input(strtolower($_POST['civilstatus']));
    }

    if (empty($_POST['skills'])) {
      $error = true;
      $_SESSION['error'][] = 'Skills is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['skills'])) {
      $error = true;
      $_SESSION['error'][] = 'Skills is invalid.';
    } else {
      $skills = test_input(strtolower($_POST['skills']));
    }

    if(!empty($_POST['contact'])) {
      if(!is_numeric($_POST['contact'])) {
        $error = true;
        $_SESSION['error'][] = 'Contact entered was not numeric.';
      } else if(strlen($_POST['contact']) < 10) {
        $error = true;
        $_SESSION['error'][] = 'Contact entered was not 11 digits long.';
      } else {
        $contact = test_input($_POST['contact']);
      }

    } 

    if (!empty($_POST['email'])) {
      if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $error = true;
        $_SESSION['error'][] = 'Invalid email format.'; 
      } else {
        $email = strtolower($_POST['email']);
      }
    } 

    if (empty($_POST['birthplace'])) {
      $error = true;
      $_SESSION['error'][] = 'Birthplace is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['birthplace'])) {
      $error = true;
      $_SESSION['error'][] = 'Birthplace is invalid.';
    } else {
      $birthplace = test_input(strtolower($_POST['birthplace']));
    }

    if (!empty($_POST['restrictioncode'])) {
      $restrictioncode = test_input(strtolower($_POST['restrictioncode']));
    } 

    if (!empty($_POST['socialmedia'])) {
      $fbUrlCheck = '/^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/';
      $secondCheck = '/home((\/)?\.[a-zA-Z0-9])?/';

      if(preg_match($fbUrlCheck, $_POST['socialmedia']) == 1 && preg_match($secondCheck, $_POST['socialmedia']) == 0) {
        $socialmedia = $_POST['socialmedia'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'Facebook URL is invalid.';
      }
    } 

    if (!empty($_FILES['applicationLetter']['name'])) {
      if ($_FILES['applicationLetter']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Application Letter image size must be less than 2MB.';
      }

      if (in_array($_FILES['applicationLetter']['type'], $allowed)){
        $applicationLetter = $_FILES['applicationLetter']['name'];
        move_uploaded_file($_FILES["applicationLetter"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'Application Letter'.pathinfo($_FILES["applicationLetter"]["name"], PATHINFO_EXTENSION));
        $result2 = $conn->prepare("UPDATE applicants_folders SET applicationLetter = ? where applicantID = ?");
        $result2->bind_param("si", $applicationLetter, $applicantid);
        $result2->execute();
        $result2->close();

        
      } else {
        $error = true;
        $_SESSION['error'][] = 'Application Letter image must be png and jpg only.';
      }
    }

    if (!empty($_FILES['serviceRecord']['name'])) {
      if ($_FILES['serviceRecord']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Service Record image size must be less than 2MB.';
      }

      if (in_array($_FILES['serviceRecord']['type'], $allowed)){
        $serviceRecord = $_FILES['serviceRecord']['name'];
        move_uploaded_file($_FILES["serviceRecord"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'Service Record.'.pathinfo($_FILES["serviceRecord"]["name"], PATHINFO_EXTENSION));
        $result2 = $conn->prepare("UPDATE applicants_folders SET serviceRecord = ? where applicantID = ?");
        $result2->bind_param("si", $serviceRecord, $applicantid);
        $result2->execute();
        $result2->close();
      
      } else {
        $error = true;
        $_SESSION['error'][] = 'Service Record image must be png and jpg only.';
      }
    } 

    if (!empty($_FILES['transcript']['name'])) {
      if ($_FILES['transcript']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Transcript of Record image size must be less than 2MB.';
      }

      if (in_array($_FILES['transcript']['type'], $allowed)){
        $transcript = $_FILES['transcript']['name'];
        move_uploaded_file($_FILES["transcript"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'Transcript.'.pathinfo($_FILES["transcript"]["name"], PATHINFO_EXTENSION));
        $result2 = $conn->prepare("UPDATE applicants_folders SET transcript = ? where applicantID = ?");
        $result2->bind_param("si", $transcript, $applicantid);
        $result2->execute();
        $result2->close();
      
      } else {
        $error = true;
        $_SESSION['error'][] = 'Transcript of Record image must be png and jpg only.';
      }
    }

    if (!empty($_FILES['collegeDiploma']['name'])) {
       if ($_FILES['collegeDiploma']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'College Diploma image size must be less than 2MB.';
      }

      if (in_array($_FILES['collegeDiploma']['type'], $allowed)){
        $collegeDiploma = $_FILES['collegeDiploma']['name'];
        move_uploaded_file($_FILES["collegeDiploma"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'College Diploma.'.pathinfo($_FILES["collegeDiploma"]["name"], PATHINFO_EXTENSION));
        $result2 = $conn->prepare("UPDATE applicants_folders SET collegeDiploma = ? where applicantID = ?");
        $result2->bind_param("si", $collegeDiploma, $applicantid);
        $result2->execute();
        $result2->close();
     
      } else {
        $error = true;
        $_SESSION['error'][] = 'College Diploma image must be png and jpg only.';
      }
    }

    if (!empty($_FILES['secondEligibility']['name'])) {
      if ($_FILES['secondEligibility']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Second Eligibility image size must be less than 2MB.';
      }

      if (in_array($_FILES['secondEligibility']['type'], $allowed)){
        $secondEligibility = $_FILES['secondEligibility']['name'];
         move_uploaded_file($_FILES["secondEligibility"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'Second Eligibility.'.pathinfo($_FILES["secondEligibility"]["name"], PATHINFO_EXTENSION));
         $result2 = $conn->prepare("UPDATE applicants_folders SET secondEligibility = ? where applicantID = ?");
        $result2->bind_param("si", $secondEligibility, $applicantid);
        $result2->execute();
        $result2->close();
      
      } else {
        $error = true;
        $_SESSION['error'][] = 'Second Eligibility image must be png and jpg only.';
      }
    }

    if (!empty($_FILES['birthCertificate']['name'])) {
      if ($_FILES['birthCertificate']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Birth Certificate image size must be less than 2MB.';
      }

      if (in_array($_FILES['birthCertificate']['type'], $allowed)){
        $birthCertificate = $_FILES['birthCertificate']['name'];
        move_uploaded_file($_FILES["birthCertificate"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'Birth Certificate.'.pathinfo($_FILES["birthCertificate"]["name"], PATHINFO_EXTENSION));
        $result2 = $conn->prepare("UPDATE applicants_folders SET birthCertificate = ?, marriageCertificate = ?, barangayClearance = ?, mtcClearance = ?, nbiClearance = ?, policeClearance = ?, rtcClearance = ?, waiverCertificate = ?, tesdaDriverCertificate = ? where applicantID = ?");
        $result2->bind_param("si", $birthCertificate, $applicantid);
        $result2->execute();
        $result2->close();
      
      } else {
        $error = true;
        $_SESSION['error'][] = 'Birth Certificate image must be png and jpg only.';
      }
    }

    if (!empty($_FILES['marriageCertificate']['name'])) {
      if ($_FILES['marriageCertificate']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Marriage Certificate image size must be less than 2MB.';
      }

      if (in_array($_FILES['marriageCertificate']['type'], $allowed)){
        move_uploaded_file($_FILES["marriageCertificate"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'Marriage Certificate.'.pathinfo($_FILES["marriageCertificate"]["name"], PATHINFO_EXTENSION));
        $marriageCertificate = $_FILES['marriageCertificate']['name'];
        $result2 = $conn->prepare("UPDATE applicants_folders SET marriageCertificate = ? where applicantID = ?");
        $result2->bind_param("si", $marriageCertificate, $applicantid);
        $result2->execute();
        $result2->close();
      } else {
        $error = true;
        $_SESSION['error'][] = 'Marriage Certificate image must be png and jpg only.';
      }
    }

    if (!empty($_FILES['barangayClearance']['name'])) {
      if ($_FILES['barangayClearance']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Barangay Clearance image size must be less than 2MB.';
      }

      if (in_array($_FILES['barangayClearance']['type'], $allowed)){
        $barangayClearance = $_FILES['barangayClearance']['name'];
        move_uploaded_file($_FILES["barangayClearance"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'Barangay Clearance.'.pathinfo($_FILES["barangayClearance"]["name"], PATHINFO_EXTENSION));
        $result2 = $conn->prepare("UPDATE applicants_folders SET barangayClearance = ? where applicantID = ?");
        $result2->bind_param("si", $barangayClearance, $applicantid);
        $result2->execute();
        $result2->close();
      
      } else {
        $error = true;
        $_SESSION['error'][] = 'Barangay Clearance image must be png and jpg only.';
      }
    }

    if (!empty($_FILES['mtcClearance']['name'])) {
     if ($_FILES['mtcClearance']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'MTC Clearance image size must be less than 2MB.';
      }

      if (in_array($_FILES['mtcClearance']['type'], $allowed)){
        $mtcClearance = $_FILES['mtcClearance']['name'];
        move_uploaded_file($_FILES["mtcClearance"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'MTC Clearance.'.pathinfo($_FILES["mtcClearance"]["name"], PATHINFO_EXTENSION));
        $result2 = $conn->prepare("UPDATE applicants_folders SET mtcClearance = ? where applicantID = ?");
        $result2->bind_param("si",  $mtcClearance, $applicantid);
        $result2->execute();
        $result2->close();
     
      } else {
        $error = true;
        $_SESSION['error'][] = 'MTC Clearance image must be png and jpg only.';
      }
    }

    if (!empty($_FILES['nbiClearance']['name'])) {
      if ($_FILES['nbiClearance']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'NBI Clearance image size must be less than 2MB.';
      }

      if (in_array($_FILES['nbiClearance']['type'], $allowed)){
        $nbiClearance = $_FILES['nbiClearance']['name'];
         move_uploaded_file($_FILES["nbiClearance"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'NBI Clearance.'.pathinfo($_FILES["nbiClearance"]["name"], PATHINFO_EXTENSION));
         $result2 = $conn->prepare("UPDATE applicants_folders SET nbiClearance = ? where applicantID = ?");
        $result2->bind_param("si", $nbiClearance, $applicantid);
        $result2->execute();
        $result2->close();
     
      } else {
        $error = true;
        $_SESSION['error'][] = 'NBI Clearance image must be png and jpg only.';
      }
    } 

    if (!empty($_FILES['policeClearance']['name'])) {
      if ($_FILES['policeClearance']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Police Clearance image size must be less than 2MB.';
      }

      if (in_array($_FILES['policeClearance']['type'], $allowed)){
        $policeClearance = $_FILES['policeClearance']['name'];
         move_uploaded_file($_FILES["policeClearance"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'Police Clearance.'.pathinfo($_FILES["policeClearance"]["name"], PATHINFO_EXTENSION));
         $result2 = $conn->prepare("UPDATE applicants_folders SET policeClearance = ? where applicantID = ?");
        $result2->bind_param("si", $policeClearance, $applicantid);
        $result2->execute();
        $result2->close();
      
      } else {
        $error = true;
        $_SESSION['error'][] = 'Police Clearance image must be png and jpg only.';
      }
    }

    if (!empty($_FILES['rtcClearance']['name'])) {
      if ($_FILES['rtcClearance']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'RTC Clearance image size must be less than 2MB.';
      }

      if (in_array($_FILES['rtcClearance']['type'], $allowed)){
        $rtcClearance = $_FILES['rtcClearance']['name'];
        move_uploaded_file($_FILES["rtcClearance"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'RTC Clearance.'.pathinfo($_FILES["rtcClearance"]["name"], PATHINFO_EXTENSION));
        $result2 = $conn->prepare("UPDATE applicants_folders SET rtcClearance = ? where applicantID = ?");
        $result2->bind_param("si", $rtcClearance, $applicantid);
        $result2->execute();
        $result2->close();
      } else {
        $error = true;
        $_SESSION['error'][] = 'RTC Clearance image must be png and jpg only.';
      }
    } 

    if (!empty($_FILES['waiverCertificate']['name'])) {
      if ($_FILES['waiverCertificate']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Waiver Certificate image size must be less than 2MB.';
      }

      if (in_array($_FILES['waiverCertificate']['type'], $allowed)){
        move_uploaded_file($_FILES["waiverCertificate"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'Waiver Certificate.'.pathinfo($_FILES["waiverCertificate"]["name"], PATHINFO_EXTENSION));
        $waiverCertificate = $_FILES['waiverCertificate']['name'];
        $result2 = $conn->prepare("UPDATE applicants_folders SET waiverCertificate = ? where applicantID = ?");
        $result2->bind_param("si", $waiverCertificate, $applicantid);
        $result2->execute();
        $result2->close();
      } else {
        $error = true;
        $_SESSION['error'][] = 'Waiver Certificate image must be png and jpg only.';
      }
    }

    if (!empty($_FILES['tesdaDriverCertificate']['name'])) {
      if ($_FILES['tesdaDriverCertificate']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Tesda Driver Certificate image size must be less than 2MB.';
      }

      if (in_array($_FILES['tesdaDriverCertificate']['type'], $allowed)){
        move_uploaded_file($_FILES["tesdaDriverCertificate"]["tmp_name"],"../applicationforms/Current/$applicantid - $last, $name/".'Tesda Driver Certificate.'.pathinfo($_FILES["tesdaDriverCertificate"]["name"], PATHINFO_EXTENSION));
        $tesdaDriverCertificate = $_FILES['tesdaDriverCertificate']['name'];
        $result2 = $conn->prepare("UPDATE applicants_folders SET tesdaDriverCertificate = ? where applicantID = ?");
        $result2->bind_param("si", $tesdaDriverCertificate, $applicantid);
        $result2->execute();
        $result2->close();
      } else {
        $error = true;
        $_SESSION['error'][] = 'Tesda Driver Certificate image must be png and jpg only.';
      }
    }

$verified = $_POST['verification'];


   $result = $conn->prepare("UPDATE applicants_pds SET name = ?, middle = ?, last = ?, age = ?, birthday = ?, birthplace = ?, address = ?, sex = ?, civilstatus = ?, skills = ?, restrictioncode = ?, contact = ?, email = ?, socialmedia = ?, verified = ? where id = ?");
    $result->bind_param("sssisssssssisssi", $name, $middle, $last, $age, $birthday, $birthplace, $address, $sex, $civilstatus, $skills, $restrictioncode, $contact, $email, $socialmedia, $verified, $applicantid);
    $result->execute();
    $result->close();

    if (!$error) {
      $conn->autocommit(TRUE);
    
      $_SESSION['success'] = 'Applicant Updated';
      header('location: allapplicants.php');
    } else {
      $conn->rollback();
    }

}

 function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
 }

   $id = $_GET['id'];

  $result = mysqli_query($conn, "SELECT * FROM applicants_pds where id = '$id'");
  
    while($row = mysqli_fetch_array($result)){
      $last = $row['last'];
      $name = $row['name'];
      $middle = $row['middle'];
      $age = $row['age'];
      $birthday = $row['birthday'];
      $birthplace = $row['birthplace'];
      $address = $row['address'];
      $sex = $row['sex'];
      $contact = $row['contact'];
      $civilstatus = $row['civilstatus'];
      $email = $row['email'];
      $socialmedia = $row['socialmedia'];
      $image = $row['image'];
      $applicantnumber = $row['applicantnumber'];
      $verified = $row['verified'];
      $skills = ucfirst($row['skills']);
      if(!empty($row['restrictioncode'])){
        $restrictioncode = $row['restrictioncode'];
      } else {
        $restrictioncode = NULL;
      }


  }

  $result1 = mysqli_query($conn, "SELECT * FROM applicants_folders where applicantID = '$id'");
  while ($row = mysqli_fetch_array($result1)) {
                    $applicationLetter = $row['applicationLetter'];
                    $serviceRecord = $row['serviceRecord'];  
                    $transcript = $row['transcript'];   
                    $collegeDiploma = $row['collegeDiploma'];   
                    $secondEligibility = $row['secondEligibility'];  
                    $birthCertificate = $row['birthCertificate'];   
                    $marriageCertificate = $row['marriageCertificate'];  
                    $barangayClearance = $row['barangayClearance'];
                    $mtcClearance = $row['mtcClearance'];  
                    $nbiClearance = $row['nbiClearance'];   
                    $policeClearance = $row['policeClearance'];  
                    $rtcClearance = $row['rtcClearance'];  
                    $waiverCertificate = $row['waiverCertificate'];   
                    $tesdaDriverCertificate = $row['tesdaDriverCertificate'];  
                    
                  }

?>
<body class="grey lighten-3">
  <?php include 'includes/nav.php'; ?>
  <?php if ($role == 'HR Officer'): ?>

  <!--Main layout-->
  <main class="pt-5">

      <div class="container-fluid mt-5">
            <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Edit Profile</span>
          </h4>
          <h4 class="mb-2 mb-sm-0 pt-1"><a class="text-right" href="<?= $_SESSION['previous'] ?>">Back</a></h4>
        </div>
      </div>
      <!-- Heading -->

        <!--Grid row-->
        <div class="row">
          <!--Grid column-->
          <div class="col-md-12 text-center">
          <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fa fa-warning'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>
            <div class="card">
              <div class="card-body">
                <div class="tab-content pt-2 pl-1" id="pills-tabContent">

                  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data" class="needs-validation" novalidate>
                      <div class="form-row">
                        <div class="col">
                          <label>* First name</label>
                          <input value="<?php echo $id;?>" type="hidden" id="applicantGetID" name="applicantGetID" class="form-control mb-3">
                          <input value="<?php echo $name;?>" type="text" id="name" name="name" class="form-control mb-3" placeholder="First name">
                          <div id="error_name" class="invalid-feedback"></div>
                        </div>
                        <div class="col">
                          <label>* Middle name</label>
                          <input value="<?php echo $middle;?>" type="text" id="middle" name="middle" class="form-control mb-3" placeholder="Middle name">
                          <div id="error_middle" class="invalid-feedback"></div>
                        </div>
                        <div class="col">
                          <label>* Last name</label>
                          <input value="<?php echo $last;?>" type="text" id="last" name="last" class="form-control mb-3" placeholder="Last name">
                          <div id="error_last" class="invalid-feedback"></div>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col">
                          <label>* Birthday</label>
                          <input value="<?php echo $birthday;?>" type="date" id="birthday" name="birthday" class="form-control mb-3">
                          <div id="error_birthday" class="invalid-feedback"></div>
                        </div>
                        <div class="col">
                          <label for="address">* Current Address</label>
                          <input value="<?php echo $address;?>" type="text" id="address" name="address" class="form-control mb-3" placeholder="Full Address">
                          <div id="error_address" class="invalid-feedback"></div>
                        </div>
                        <div class="col">
                          <label for="contact">Phone number</label>
                          <input value="<?php echo $contact;?>" type="number" id="contact" name="contact" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="11" class="form-control mb-3" placeholder="Phone Number">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col"><label>*Gender</label><br>
                          <div class="custom-control custom-radio custom-control-inline text-left">
                            <input <?php if($sex == 'Male'){ echo 'checked'; }?> type="radio" class="custom-control-input mb-4" id="sex1" name="sex" value="Male">
                            <label class="custom-control-label" for="sex1">Male</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline text-left">
                            <input <?php if($sex == 'Female'){ echo 'checked'; }?> type="radio" class="custom-control-input mb-4" id="sex2" name="sex" value="Female"> 
                            <label class="custom-control-label" for="sex2">Female</label>
                           <div id="error_sex" class="invalid-feedback"></div>
                          </div>
                        </div>
                        <div class="col"><label>*Civil Status</label><br>
                          <div class="custom-control custom-radio custom-control-inline text-left">
                            <input <?php if($civilstatus == 'Single'){ echo 'checked'; }?> type="radio" class="custom-control-input mb-4" id="single" name="civilstatus" value="Single">
                            <label class="custom-control-label" for="single">Single</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline text-left">
                            <input <?php if($civilstatus == 'Married'){ echo 'checked'; }?> type="radio" class="custom-control-input mb-4" id="married" name="civilstatus" value="Married">
                            <label class="custom-control-label" for="married">Married</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline text-left">
                            <input <?php if($civilstatus == 'Widowed'){ echo 'checked'; }?> type="radio" class="custom-control-input mb-4" id="widowed" name="civilstatus" value="Widowed">
                            <label class="custom-control-label" for="widowed">Widowed</label>
                            <div id="error_civilstatus" class="invalid-feedback"></div>
                          </div>
                        </div>
                        <div class="col">
                          <label for="email">E-mail</label>
                          <input value="<?php echo $email;?>" type="email" id="email" name="email" class="form-control mb-3" placeholder="Email Address">
                          <div id="error_email" class="invalid-feedback"></div>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-3"><label for="image">* 2x2 Photo</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input value="<?php echo $image;?>" type="file" class="custom-file-input mb-4" id="image" name="image" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="image"><?php echo $image;?></label>
                              <div id="error_image" class="invalid-feedback"></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-6"><label>*Birth Place</label>
                          <input value="<?php echo $birthplace;?>" type="text" id="birthplace" name="birthplace" class="form-control mb-3">
                          
                        </div>
                        <div class="col-3"><label>Facebook URL</label>
                          <input value="<?php echo $socialmedia;?>" type="text" id="socialmedia" name="socialmedia" class="form-control mb-3" placeholder="Facebook URL">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col">
                          <label>*Specialization</label><br>
                          <input value="<?php echo $skills;?>" type="text" id="others" name="skills" class="form-control">
                        </div>
                       <div class="col"><label>*Verified?</label><br>
                          <div class="custom-control custom-radio custom-control-inline text-left">
                            <input <?php if($verified == '1'){ echo 'checked'; }?> type="radio" class="custom-control-input mb-4" id="1" name="verification" value="1">
                            <label class="custom-control-label" for="1">Yes</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline text-left">
                            <input <?php if($verified == '0'){ echo 'checked'; }?> type="radio" class="custom-control-input mb-4" id="0" name="verification" value="0"> 
                            <label class="custom-control-label" for="0">No</label>
                           <div id="error_sex" class="invalid-feedback"></div>
                          </div>
                        </div>
                      </div><br>
                  </div>
                  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <!-- Material form register -->                       

                    <h6 class="red-text text-center py-4 pulse">
                    NOTE: Upload your files as JPEG, JPG, or PNG
                    </h6>
                    <div class="row">
                      <div class="col-md-6 text-center">

                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Application Letter</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $applicationLetter;?>" type="file" class="custom-file-input" name="applicationLetter" id="applicationLetter" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $applicationLetter;?></label>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Authenticated Service Record</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $serviceRecord;?>" type="file" class="custom-file-input" name="serviceRecord" id="serviceRecord" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $serviceRecord;?></label>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Transcript of Records</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $transcript;?>" type="file" class="custom-file-input" name="transcript" id="transcript" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $transcript;?></label>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">College Diploma</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $collegeDiploma;?>" type="file" class="custom-file-input" name="collegeDiploma" id="collegeDiploma" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $collegeDiploma;?></label>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Second Level Eligibility (Issued from CSC/PRC)</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $secondEligibility;?>" type="file" class="custom-file-input" name="secondEligibility" id="secondEligibility" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $secondEligibility;?></label>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Certificate of Live Birth (Issued by PSA)</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $birthCertificate;?>" type="file" class="custom-file-input" name="birthCertificate" id="birthCertificate" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $birthCertificate;?></label>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Certificate of Marriage (If Applicable)</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $marriageCertificate;?>" type="file" class="custom-file-input" name="marriageCertificate" id="marriageCertificate" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $marriageCertificate;?></label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 text-center">
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Barangay Clearance</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $barangayClearance;?>" type="file" class="custom-file-input" name="barangayClearance" id="barangayClearance" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $barangayClearance;?></label>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">MTC Clearance</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $mtcClearance;?>" type="file" class="custom-file-input" name="mtcClearance" id="mtcClearance" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $mtcClearance;?></label>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">NBI Clearance</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $nbiClearance;?>" type="file" class="custom-file-input" name="nbiClearance" id="nbiClearance" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $nbiClearance;?></label>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Police Clearance</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $policeClearance;?>" type="file" class="custom-file-input" name="policeClearance" id="policeClearance" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $policeClearance;?></label>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">RTC Clearance</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $rtcClearance;?>" type="file" class="custom-file-input" name="rtcClearance" id="rtcClearance" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $rtcClearance;?></label>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Certificate of Waiver (age/height deficiencies if applicable)</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $waiverCertificate;?>" type="file" class="custom-file-input" name="waiverCertificate" id="waiverCertificate" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $waiverCertificate;?></label>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">TESDA Driver Certificate (If Applicable)</span>
                          </div>
                          <div class="custom-file">
                            <input value="<?php echo $tesdaDriverCertificate;?>" type="file" class="custom-file-input" name="tesdaDriverCertificate" id="tesdaDriverCertificate" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01"><?php echo $tesdaDriverCertificate;?></label>
                          </div>
                        </div>

                      </div>
                    </div><!--Grid row-->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalLoginForm">Submit</button>
                    
                    <!-- Form -->
                  </div>
                </div>
                <ul class="nav nav-pills justify-content-end" id="pills-tab" role="tablist">
                  <li class="nav-item" style="display: none;" id="prevbut">
                    <button class="btn btn-primary btn-sm waves-effect" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" onclick="myFunction()">
                      <h5>Prev</h5>
                    </button>
                  </li>
                  <li class="nav-item" id="nextbut">
                    <button class="btn btn-primary btn-sm waves-effect" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" onclick="myFunction()">
                      <h5>Next</h5>
                    </button>
                  </li>
                </ul>
              </div>
            </div>
            <!--Grid column-->

          </div>
          <!--Grid row-->
        </div>
        <!--Container-->
      </div>

    </main>
    <!--/Main layout-->


  <!-- Are you sure -->
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Are you sure?</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <img src="logo.png" class="center mb-3 animated rotateIn rounded-circle" style="width: 150px; height: 150px;">
          <p class="text-center">Please double check the the assessment before submitting to avoid unncesseary mistakes.<br>
          </p>

        </div>
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</a>
        <button class="btn btn-primary waves-effect" name="submit" id="submit"><i class="fa fa-check-square-o"></i>Yes</button>
      </div>
      </form>
    </div>
    <!--/.Content-->
  </div>
</div>
  <?php else: ?>
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
      <div class='alert alert-danger alert-dismissible fade show' role='alert'>
        <h4>Forbidden.</h4>
      </div>
    </div>
    <!--/container-->
  </main>
  <!--/Main layout-->
  <?php endif ?>
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/scripts.php'; ?>
      <script type="text/javascript">
      function myFunction() {
        var x = document.getElementById("prevbut");
        var y = document.getElementById("nextbut");
        if (x.style.display === "none") {
          x.style.display = "block";
          y.style.display = "none";
        } else {
          x.style.display = "none";
          y.style.display = "block";
        }
      }
    </script>

    <!-- JQuery -->
<?php include 'includes/scripts.php'; ?>
<script type="text/javascript">
  (function() {
'use strict';
window.addEventListener('load', function() {
// Fetch all the forms we want to apply custom Bootstrap validation styles to
var forms = document.getElementsByClassName('needs-validation');
// Loop over them and prevent submission
var validation = Array.prototype.filter.call(forms, function(form) {
form.addEventListener('submit', function(event) {
if (form.checkValidity() === false) {
event.preventDefault();
event.stopPropagation();
}
form.classList.add('was-validated');
}, false);
});
}, false);
})();
</script>
</body>
</html>


<?php
$pageTitle = "Neurological Examiner";
$page      = "neuro.php";
$title     = "Neurological Examiner";

$current = "Neurological Examiner";
?>
<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<?php
  if(strtotime($today) < strtotime($submissiondate) && $submissionstatus == 'START'){
    $_SESSION['error'][] = 'Submission of form still ongoing.';
    header('location: home.php');
  }

  if($submissionstatus == 'PAUSED'){
    $_SESSION['error'][] = 'Submission of form still ongoing but paused.';
    header('location: home.php');
  }

  
  // if(isset($_POST['submit'])){ 
  //     mysqli_autocommit($conn, false);
  //     $adminID=$_POST['adminid'];
  //     $timestamp = date("Y-m-d H:i:s");
  //     $sql = $conn -> prepare("SELECT * FROM applicants_pds where recruitmentDate = ?");
  //     $sql->bind_param("s", $recruitmentDate);
  //     $sql -> execute();
  //     $query = $sql->get_result();
  //     $error = false;
  //     $sql_array = array();
  //     $sql_array1 = array();

  //     while ($row = mysqli_fetch_assoc($query)) {
  //       $applicant = mysqli_real_escape_string($conn, $row['id']);
  //       if (isset($_POST[$applicant])) {
  //         $status = mysqli_real_escape_string($conn, $_POST[$applicant]);
  //         $sql_array[] = $conn->prepare( "UPDATE test_neuro SET status = ?, timestamp = ?, adminID = ?  where applicantID = ?");
  //         foreach($sql_array as $sql_row){
  //           $sql_row->bind_param("ssii", $status, $timestamp, $adminID, $applicant);
  //           $sql_row->execute();
  //         }
  //       }
  //     }

  //   if(!$error){
  //     mysqli_commit($conn);
  //     $_SESSION['success'] = 'Save';
  //   } else {
  //     mysqli_rollback($conn);
  //   }
  // }

?>
<?php
  if(isset($_POST['pass'])){ 
    mysqli_autocommit($conn, false);
    $adminID=$_POST['adminid'];
    $timestamp = date("Y-m-d H:i:s");
    $error = false;

    $status = 'PASSED';
    $applicantID = $_POST["appID"]; 

    $result1 = $conn->prepare("UPDATE test_neuro SET status = ?, adminID = ?, timestamp = ? where applicantID = ?");
    $result1->bind_param("sisi", $status, $adminID, $timestamp, $applicantID);
    $result1->execute();
    $result1->close();

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Save';
    } else {
      mysqli_rollback($conn);
    }
  }

  if(isset($_POST['fail'])){ 
    mysqli_autocommit($conn, false);
    $adminID=$_POST['adminid'];
    $timestamp = date("Y-m-d H:i:s");
    $error = false;

    $status = 'FAILED';
    $applicantID = $_POST["appID"]; 

    $result1 = $conn->prepare("UPDATE test_neuro SET status = ?, adminID = ?, timestamp = ? where applicantID = ?");
    $result1->bind_param("sisi", $status, $adminID, $timestamp, $applicantID);
    $result1->execute();
    $result1->close();

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Save';
    } else {
      mysqli_rollback($conn);
    }
  }

  if(isset($_POST['accept'])){ 
    mysqli_autocommit($conn, false);
    $adminID=$_POST['adminid'];
    $timestamp = date("Y-m-d H:i:s");
    $error = false;

    if (empty($_POST["status"])){
      $error = true;
      $_SESSION['error'][] = 'No applicant selected.';
    } else {
      $number = count($_POST["status"]);
      for($i=0; $i<$number; $i++) {
        if(trim($_POST["status"][$i] != '')) {
            $status = 'PASSED';
          $applicantID = $_POST["status"][$i]; 

          $result1 = $conn->prepare("UPDATE test_neuro SET status = ?, adminID = ?, timestamp = ? where applicantID = ?");
          $result1->bind_param("sisi", $status, $adminID, $timestamp, $applicantID);
          $result1->execute();
          $result1->close();
        }
      }
    }

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Save';
    } else {
      mysqli_rollback($conn);
    }
  }

  if(isset($_POST['reject'])){ 
    mysqli_autocommit($conn, false);
    $adminID=$_POST['adminid'];
    $timestamp = date("Y-m-d H:i:s");
    $error = false;
  
    if (empty($_POST["status"])){
      $error = true;
      $_SESSION['error'][] = 'No applicant selected.';
    } else {
      $number = count($_POST["status"]);
      for($i=0; $i<$number; $i++) {
        if(trim($_POST["status"][$i] != '')) {
            $status = 'FAILED';
          $applicantID = $_POST["status"][$i]; 

          $result1 = $conn->prepare("UPDATE test_neuro SET status = ?, adminID = ?, timestamp = ? where applicantID = ?");
          $result1->bind_param("sisi", $status, $adminID, $timestamp, $applicantID);
          $result1->execute();
          $result1->close();
        }
      }
    }

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Save';
    } else {
      mysqli_rollback($conn);
    }
  }

?>
<?php
  if (isset($_POST['edit'])) {
    mysqli_autocommit($conn, false);
    $timestamp = date("Y-m-d H:i:s");
    $allowed = array('PASSED', 'FAILED');
    $error = false;

    if (empty($_POST['status'])) {
      $error = true;
      $_SESSION['error'][] = 'Status is required.';
    } elseif (!in_array($_POST['status'], $allowed)) {
      $error = true;
      $_SESSION['error'][] = 'Status is invalid.';
    } else {
      $status = $_POST['status'];
    }

    if (empty($_POST['applicantID'])) {
      $error = true;
      $_SESSION['error'][] = 'Status is required.';
    } else {
      $applicantID = $_POST['applicantID'];
    }

    $result1 = $conn->prepare("UPDATE test_neuro SET status = ?, timestamp = ? WHERE applicantID = ?");
    $result1->bind_param("ssi", $status, $timestamp, $applicantID);
    $result1->execute();
    $result1->close();

    if(!$error){
      mysqli_commit($conn);
      $_SESSION['success'] = 'Status Updated';
    } else {
      mysqli_rollback($conn);
    }
  }


?>

<?php
  if (isset($_POST['changestatus'])) {
    $result = $conn->prepare("UPDATE tests_status SET status='DONE' WHERE `test`='Neuro-Psychiatric Examination and Drug Test'");
    $result->execute();
    $result->close();
  }
?>

<body class="grey lighten-3">
  <?php include 'includes/nav.php'; ?>

<?php if ($role == 'Neurological Examiner' || $role == 'HR Officer'): ?>
<?php
    $result1 = $conn -> prepare("SELECT status FROM `tests_status` where `test` = 'Neuro-Psychiatric Examination and Drug Test'");
    $result1->execute();
    $query = $result1->get_result();
    $row = $query->fetch_assoc();
    $testStatus = $row['status'];
    $result1->close();

  // $result1 = mysqli_query($conn, "SELECT status FROM `tests_status` where `test` = 'Neuro-Psychiatric Examination and Drug Test'");
  // $row = mysqli_fetch_assoc($result1);
  // $testStatus = $row['status'];

  switch ($testStatus): 
?>
<?php case "ON-GOING": ?>
<!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
        <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fas fa-warning'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>

      <?php
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fas fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>

<?php 
  $result2 = mysqli_query($conn, "SELECT * FROM `test_neuro` join applicants_pds on applicants_pds.id = test_neuro.applicantID where `status` is not null and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");

  $sql = mysqli_query($conn, "SELECT * FROM `test_neuro` join applicants_pds on applicants_pds.id = test_neuro.applicantID where applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");

  $result13 = mysqli_query($conn, "SELECT * FROM `applicants_pds` join test_neuro on applicants_pds.id = test_neuro.applicantID where `status` = 'PASSED' and applicants_pds.recruitmentDate = '$recruitmentDate'");

?>

    <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Neuro-Psychiatric Examination and Drug Test</span>
          </h4>
          <h4 class="mb-2 mb-sm-0 pt-1 text-right">Processed <?php echo mysqli_num_rows($result2);?> out of <?php echo mysqli_num_rows($sql);?> applicants</h4>
        </div>
      </div>
      <!-- Heading -->

 <div class="row">
        <div class="col-md-12">
          <div class="card">
            <!--Card content-->
            <div class="card-body">
                                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <div class="row">
                  <div class="col-6 float-left">
                    <button class="btn btn-outline-success" name="accept" type="submit"><i class="fas fa-save" aria-hidden="true"></i> Pass</button>
                    <button class="btn btn-outline-danger" name="reject" type="submit"><i class="fas fa-save" aria-hidden="true"></i> Fail</button>
                  </div>
                   <div class="col-6 float-right">
                    <h4 class="mb-2 mb-sm-0 pt-1 text-right text-success"> <?php echo mysqli_num_rows($result13);?> Passed</h4>
                 
                </div>
              </div>
                                <ul class="nav nav-pills mb-3 justify-content-end" id="pills-tab" role="tablist">
              <li class="nav-item">
              <a class="btn-primary nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
              aria-controls="pills-home" aria-selected="true">ALL</a>
              </li>
              <li class="nav-item">
              <a class="btn-info nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
              aria-controls="pills-profile" aria-selected="false">Finished</a>
              </li>
            </ul>
            <div class="tab-content pt-2 pl-1" id="pills-tabContent">
              <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
              <table id="dtBasicExample" class="table table-bordered table-responsive-md" cellspacing="0" width="100%">
                <thead>
                  <th>
                    <!-- Default unchecked -->
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="selectall">
                      <label class="custom-control-label" for="selectall"></label>
                    </div>
                  </th>
                  <th>Applicant Number</th>
                  <th>Full Name</th>
                  <th>Action</th>
                  <th>Status</th>
                </thead>
                <tbody>
                  <?php
                    $result1 = mysqli_query($conn, "SELECT * FROM `test_neuro` join applicants_pds on applicants_pds.id = test_neuro.applicantID where `status` is NULL and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");
                    $newStatus = mysqli_num_rows($result1);
                    while ($row = mysqli_fetch_array($result1)) {
                      $input = '<form method="POST" action="neuro.php">
                                  <input class="form-check-input" type="hidden" value="'.$row['applicantID'].'" name="appID">
                                  <input class="form-check-input" type="hidden" name="adminid" id="adminid" value="'.$user['id'].'" style="visibility: hidden;">
                                  <div class="btn-group" role="group" aria-label="Basic example">
                                  <button type="submit" name="pass" class="btn btn btn-success">Pass</button>
                                  <button type="submit" name="fail" class="btn btn btn-danger">Fail</button>
                                  </div>
                                </form>
                                ';

                      echo "
                        <tr>
                          <td scope='row'>
                            <!-- Default unchecked -->
                            <div class='custom-control custom-checkbox'>
                            <input type='checkbox' class='custom-control-input chkCheckBoxId' name='status[]' value='".$row['applicantID']."' id='".$row['applicantID']."'>
                            <label class='custom-control-label' for='".$row['applicantID']."'></label>
                            </div>
                          </td>
                          <td>".$row['applicantnumber']."</td>
                          <td>".ucwords($row['last']).", ".ucwords($row['name'])." ".ucwords($row['middle'])."</td>
                          <td><a href='evaluationview.php?id=$row[applicantID]'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                          <td>".$input."</td>
                        </tr>
                      ";
                    }
                  ?>
                
                </tbody>
              </table>
                   </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        <table class="table table-bordered display table-responsive-md" cellspacing="0" width="100%">
                <thead>
                  <th width="20">Applicant Number</th>
                  <th width="200">Full Name</th>
                  <th width="20">View Profile</th>
                  <th width="20">Status</th>
                  <th width="20">Action</th>
                </thead>
                <tbody>

                  <?php
                    $result2 = mysqli_query($conn, "SELECT * FROM `test_neuro` join applicants_pds on applicants_pds.id = test_neuro.applicantID where `status` is not null and applicants_pds.recruitmentDate = '$recruitmentDate' order by applicants_pds.id ASC");
                    while ($row = mysqli_fetch_array($result2)) {
                    if ($row['status'] == 'PASSED'){
                        $color = 'success';
                      } else {
                        $color = 'danger';
                      }
                      ?>
                        <tr>
                          <td><?php echo $row['applicantnumber'];?></td>
                          <td><?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?></td>
                          <td><span class='badge badge-pill badge-<?php echo $color?>'><?php echo $row['status'];?></span></td>
                         <td><a href='evaluationview.php?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                          <td>
                           <div class='text-center'><a data-toggle='modal' data-target='#edit<?php echo $row['applicantID']; ?>' href='#edit?id=<?php echo $row['applicantID']; ?>'><i class='fas fa-edit blue-text'></i></a></div>
                          </td>
                        </tr>
            <div class="modal fade" id="edit<?php echo $row['applicantID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold"><?php echo $row['applicantID'];?> - <?php echo ucwords($row['last']);?>, <?php echo ucwords($row['name']);?> <?php echo ucwords($row['middle']);?>
                    </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                    </button>
                  </div>
                  <div class="modal-body mx-3">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data" class="needs-validation" novalidate>
                      <div class="text-center">
                        <input type="hidden" id="id" name="applicantID" class="form-control validate" value="<?php echo $row['applicantID']; ?>" required>

                         <div class="btn-group btn-group-toggle" data-toggle="buttons">
                          <?php if($row['status'] == 'PASSED'){ 
                            ?>
                          <label class="btn btn-danger form-check-label">
                            <input class="form-check-input" type="radio" name="status" autocomplete="off" value="FAILED"> Fail
                          </label>
                        <?php } else {?>
                          <label class="btn btn-success form-check-label <?php if($row['status'] == 'PASSED'){ echo 'active'; }?>">
                            <input class="form-check-input" type="radio" name="status" autocomplete="off" value="PASSED"> Pass
                          </label>
                        <?php }?>

                        </div>

                      </div>
                      <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-outline-warning waves-effect" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-outline-primary waves-effect" name="edit" id="edit">Update</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php    } ?>

                </tbody>
              </table>
  </div>
</div>
                <div class="row">
                  <div class="col-6">
                    <button class="btn btn-outline-success" name="accept" type="submit"><i class="fas fa-save" aria-hidden="true"></i> Pass</button>
                    <button class="btn btn-outline-danger" name="reject" type="submit"><i class="fas fa-save" aria-hidden="true"></i> Fail</button></form>
                  </div>
                  <div class="col-6 text-right">
                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalLoginForm" <?php if($newStatus > 0){ echo 'disabled'; }?>>Finalize</button>
                  </div>
                </div>
            </div>

          </div>
        </div>
      </div>

  </div><!--/container-->
</main><!--/Main layout-->

<?php if($newStatus == 0){ ?>
<!-- Are you sure -->
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Are you sure?</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <img src="logo.png" class="center mb-3 animated rotateIn rounded-circle" style="width: 150px; height: 150px;">
          <p class="text-center">Please double check the assessment before submitting to avoid unncessary mistakes.<br>
                  Once submitted will be the final result of the neuro-psychiatric examination and drug test.
          </p>

        </div>
      </div>

      <!--Footer-->
 <form method="POST" action="neuro.php" enctype="multipart/form-data" class="needs-validation" novalidate>
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</a>
        <button class="btn btn-primary waves-effect" name="changestatus" id="changestatus"><i class="fa fa-check-square-o"></i>Yes</button>
      </div>
    </form>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<?php }?>
<?php break; ?>
<?php case "DONE": ?>
<!--Main layout-->
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-success alert-dismissible fade show' role='alert'>
    <h4><i class='icon fa fa-check'></i> Done!</h4>
    <a href='neuroresults.php' class='alert-link'>View Results</a><br>
    <a href='finalresults.php' class='alert-link'>Go to Final Results</a>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->
<?php break; ?>
<?php default: ?>
<!--Main layout-->
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-info alert-dismissible fade show' role='alert'>
    <h4>Background Investigation not yet finished!</h4>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->

<?php endswitch; ?>
<?php else: ?>
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">

    <div class='alert alert-danger alert-dismissible fade show' role='alert'>
    <h4>Forbidden.</h4>
    </div>

  </div><!--/container-->
</main><!--/Main layout-->


<?php endif ?>

<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>

</body>
</html>



    <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn">
        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">
          <h4 class="mb-2 mb-sm-0 pt-1">
            <a href="home.php">Home Page</a>
            <span>/</span>
            <span>Applicants</span>
            <span>/</span>
            <span>January 15, 2020</span>
          </h4>
          <h4 class="mb-2 mb-sm-0 pt-1"><a class="text-right" href="http://localhost/bfprecruitment/admin/applicants.php">Back</a></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="card">
          <!--Card content-->
          <div class="card-body">

                <table class="table table-bordered table-responsive-md display" cellspacing="0" width="100%">
                <thead>
                  <th width="200">Full Name</th>
                  <th width="20">Medical Results</th>
                  <th width="20">Written Exam Score</th>
                  <th width="20">Physical Agility Score</th>
                  <th width="20">Panel Interview Score</th>
                  <th width="20">Background Investigation Results</th>
                  <th width="20">Neuro Results</th>
                  <th width="20">View Profile</th>
                </thead>
                <tbody>
                                    <tr>
                    <td>Bautista, Tony Daniels</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>78</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><a href='evaluationview.php?id=6'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Bautista, Scarlet Rivera</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=66'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Collins, Morgan De Los Santos</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6>40</td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=52'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Cruise, John Walter</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>89</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><a href='evaluationview.php?id=8'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Cruise, Kenny Walter</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=24'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Cruise, Chris Chua</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=60'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Cumberbatch, Mark Castro</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6>54</td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=29'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Cumberbatch, Sanders De Los Santos</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=44'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Cumberbatch, Alan Pascual</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=46'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Cumberbatch, Scarlet Ocampo</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=50'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Dalton, Scarlet Pascual</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=16'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Dalton, Morris Santos</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=41'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Dalton, Chris Castro</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=48'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Dalton, Scarlet Velasco</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=49'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Daniels, Morgan Walter</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6>40</td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=34'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Daniels, Chris Pascual</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=57'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Dantes, Daniel Ocampo</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=19'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Dantes, Daniel Castro</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=58'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Davis, Mike Reyes</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=37'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Davis, Stewie Castillo</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=47'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Depp, David Stark</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>89</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><a href='evaluationview.php?id=1'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Depp, Stewie Ocampo</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6>6</td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=32'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Depp, Rafael Rogers</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=40'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Depp, Daniel Reyes</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=43'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Depp, Michael Rivera</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=51'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Evans, Scarlet Daniels</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>80</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><a href='evaluationview.php?id=5'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Evans, Alan Tom</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=35'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Evans, Kenny Castro</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=42'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Evans, Mike Rivera</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=53'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Gonzales, Sanders Pascual</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><a href='evaluationview.php?id=4'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Gonzales, David Rogers</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>79</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><a href='evaluationview.php?id=9'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Gonzales, Michael Rivera</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>89</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><a href='evaluationview.php?id=14'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Gonzales, Morris De Los Santos</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>98</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=23'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Gonzales, John Castro</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>97</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=25'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Gonzales, Rafael Rivera</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6>35</td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=30'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Jones, Sanders Fernandez</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=18'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Jones, Michael Velasco</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=20'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Jones, Leo Messi</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=64'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Martinez, Paul Reyes</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>89</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><a href='evaluationview.php?id=7'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Martinez, Daniel Chua</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>98</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><a href='evaluationview.php?id=10'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Martinez, Peter Ocampo</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>96</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=26'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Martinez, Rafael Messi</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6>6</td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=33'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Martinez, Stewie Rivera</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=39'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Martinez, Mike Reyes</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=59'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Martinez, Kenny Gomez</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=65'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Rivera, Leo Tom</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=36'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Rivera, Alan Chua</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=55'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Santiago, Mike Gonzales</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>78</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><a href='evaluationview.php?id=11'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Santiago, Rafael Walter</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=38'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Smith, Stewie Chua</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=54'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Stacey, Maria Velasco</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><a href='evaluationview.php?id=13'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Stacey, Kenny Santos</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=15'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Stacey, Michael Gomez</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=17'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Stacey, Daniel Walter</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=22'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Stacey, Kenny Walter</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6>40</td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=31'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Stacey, Daniel Chua</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=45'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Stacey, David Reyes</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=56'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Weston, Chris Velasco</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=63'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>White, Morgan Chua</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>97</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6>6</td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=28'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>White, David Daniels</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=61'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>White, David Ocampo</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=62'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Williams, David Reyes</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=21'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Williams, David Rogers</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>97</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-danger">FAILED</span></h6>40</td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><h6><span class="badge badge-pill badge-danger"></span></h6></td>
                    <td><a href='evaluationview.php?id=27'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                    <tr>
                    <td>Wilson, Tony Fernandez</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>79</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>90</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6>100</td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><h6><span class="badge badge-pill badge-success">PASSED</span></h6></td>
                    <td><a href='evaluationview.php?id=12'><i class='fas fa-eye' class='btn btn-danger btn-rounded form-check-label'></i></a></td>
                  </tr>
                 
                                  </tbody>
              </table>



            
            </div>

          </div>
        </div>
      </div>



    </div><!--/container-->
  </main>
  <!--/Main layout-->
      <!--Footer-->
  <footer class="page-footer text-center font-small primary-color-dark darken-2 mt-4">

    <!--Copyright-->
    <div class="footer-copyright py-3">
      © 2019 Copyright:
      <a target="_blank"> BFP </a>
    </div>
    <!--/.Copyright-->

  </footer>
  <!--/.Footer-->

  <!-- SCRIPTS -->  <!--   <script type="text/javascript" src="js/compiled-4.8.8.min.js" ></script> -->

 <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Initializations -->
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();

  </script>

      <script type="text/javascript">$(document).ready(function() {
    $('table.display').DataTable();
} );
    </script>

  <!-- DataTables JS -->
  <script src="js/addons/datatables.min.js" type="text/javascript"></script>

  <!-- DataTables Select JS -->
  <script src="js/addons/datatables-select.min.js" type="text/javascript"></script>

<script>
$("#selectall").click(function () {
$('input:checkbox').not(this).prop('checked', this.checked);
});
</script>

<script type="text/javascript">
$("td").click(function(e) {
    var checkbox = $(':checkbox', $(this).parent()).get(0);
    var checked = checkbox.checked;
    if (checked == false) checkbox.checked = true;
    else checkbox.checked = false;
});
</script>



<script type="text/javascript">
  // Tooltips Initialization
$(function () {
$('[data-toggle="tooltip"]').tooltip()
})
</script>
<script type="text/javascript">
  $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
});

// Acá guarda el index al cual corresponde la tab. Lo podés ver en el dev tool de chrome.
var activeTab = localStorage.getItem('activeTab');

// En la consola te va a mostrar la pestaña donde hiciste el último click y lo
// guarda en "activeTab". Te dejo el console para que lo veas. Y cuando refresques
// el browser, va a quedar activa la última donde hiciste el click.
console.log(activeTab);

if (activeTab) {
   $('a[href="' + activeTab + '"]').tab('show');
}
</script></body>
</html>


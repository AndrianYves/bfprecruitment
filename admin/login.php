<?php
	session_start();
	include 'includes/conn.php';

	if(isset($_POST['login'])){
		if(empty($_POST['username'])) {
			$error = true;
			$_SESSION['error'][] = 'Username is required.';
		} else {
			$username = mysqli_real_escape_string($conn, $_POST['username']);
		}

		if(empty($_POST['password'])) {
			$error = true;
			$_SESSION['error'][] = 'Password is required.';
		} else {
			$password = mysqli_real_escape_string($conn, $_POST['password']);
		}
	
		$sql = $conn -> prepare("SELECT * FROM admins where username = ?");
		$sql->bind_param("s", $username);
		$sql->execute();
		$query = $sql->get_result();

		if($query->num_rows < 1){
			$_SESSION['error'][] = 'Invalid username/password';
		}
		else{
			$row = $query->fetch_assoc();
			if(password_verify($password, $row['password'])){
				$timestamp = date("Y-m-d H:i:s");

				$results1 = $conn->prepare( "UPDATE admins SET lastlogin = ? WHERE username=?");
				$results1->bind_param("ss", $timestamp, $username);
				$results1->execute();
				$results1->close();
				$sql->close();

				$_SESSION['admin'] = $row['id'];
			}
			else{
				$_SESSION['error'][] = 'Invalid username/password';
			}
		}
	} else {
		$_SESSION['error'][] = 'Input admin credentials first';
	}

	header('location: home.php');

?>
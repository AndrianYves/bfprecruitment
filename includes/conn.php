<?php

	$conn = new mysqli("localhost", "root", "", "bfp");
	if($conn->connect_error) {
	  exit('Error connecting to database'); //Should be a message a typical user could understand in production
	}
	
	// mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$conn->set_charset("utf8mb4");

	date_default_timezone_set("Asia/Bangkok");
	$today = date('Y-m-d');

	$sqldate1 = $conn->prepare("SELECT recruitmentbatch, submissionofformenddate, submissionofformstatus FROM settings where id = ?");
	$sqldate1->bind_param("s", $id);
	$id = '1';
	$sqldate1->execute();
	$query = $sqldate1->get_result();
	$row1 = $query->fetch_assoc();
	$end = $row1['recruitmentbatch'];
	$submissiondate = $row1['submissionofformenddate'];
	$submissionstatus = $row1['submissionofformstatus'];

	if(strtotime($today) > strtotime($submissiondate) || $submissionstatus != 'START'){
		session_start();
		$_SESSION['end'] = 1;
		if($submissionstatus == 'PAUSED'){
			header('location: submissionspaused.php');
		} else {
			  $submissionofformstatus = 'STOP';
			  $settingsid = '1';

			  $result4 = $conn->prepare("UPDATE settings SET submissionofformstatus = ? where id = ?");
			  $result4->bind_param("si", $submissionofformstatus, $settingsid);
			  $result4->execute(); 

			header('location: submissions.php');
		}
	}

switch (connection_status()){
	case CONNECTION_ABORTED:
	header('location: 404.php');
	break;
	case CONNECTION_TIMEOUT:
	header('location: 404.php');
	  break;
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bureau of Fire Protection - Application Form</title>
    <!-- Font Awesome -->
    <script defer src="js/all.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/style.min.css" rel="stylesheet">

  </head>
  <style>
    * {
    margin: 0;
    padding: 0;
    }
    html,
    body {
    height: 100%;
    width: 100%;
    }
    main {
    min-height: 100%;
    }
    #container {
    overflow: auto;
    }
    header {
    width: 100%;
    }
    footer {
    position: relative;
    clear: both;
    width: 100%;
    }

  p, h1, h2, h3, h4, h5, h6, label, td, dt, dd, li{
  user-select: none; /* supported by Chrome and Opera */
   -webkit-user-select: none; /* Safari */
   -khtml-user-select: none; /* Konqueror HTML */
   -moz-user-select: none; /* Firefox */
   -ms-user-select: none; /* Internet Explorer/Edge */
  }
  </style>
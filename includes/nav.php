  <!--Main Navigation-->
    <header>
      <!-- Navbar -->
      <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
        <div class="container">
          <!-- Brand -->
          <a class="navbar-brand waves-effect" href="index.php">
          <strong class="blue-text">BFP Recruit - Application Form</strong>
          </a>
          <!-- Collapse -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <!-- Links -->
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left -->
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link waves-effect" href="index.php">Home
                <span class="sr-only">(current)</span>
                </a>
              </li>
            </ul>
            <!-- Right -->
            <ul class="navbar-nav nav-flex-icons">
              <li class="nav-item">
                <a href="https://www.facebook.com/BFPCARpis/" class="nav-link waves-effect" target="_blank">
                 <img class="mr-2" style="height:20px;width:20px" alt="BFP Logo" src="img/fblogo.png">
                </a>
              </li>
              <li class="nav-item">
                <a href="http://car.bfp.gov.ph/" class="nav-link border border-light rounded waves-effect" target="_blank">
                <img class="mr-2" style="height:20px;width:20px" alt="BFP Logo" src="logo.png">Bureau of Fire Protection - CAR
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- Navbar -->
    </header>
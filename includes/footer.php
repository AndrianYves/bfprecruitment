    <!--Footer-->
    <footer class="page-footer text-center font-small mdb-color darken-2 mt-4">
  <!--Copyright-->
      <div class="footer-copyright py-3">
        © 2019 Copyright:
        <a href="http://car.bfp.gov.ph/" target="_blank"> Bureau of Fire Protection - CAR </a>
      </div>
      <!--/.Copyright-->
    </footer>
    <!--/.Footer-->
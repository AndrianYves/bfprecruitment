<?php
  session_start();

  if(empty(isset($_SESSION['end']))){
    header('location: index.php');
  } else {
    session_destroy();
  }
?>
<?php include 'includes/header.php'; ?>

  <body>

  <?php include 'includes/nav.php'; ?>
    <!--Main layout-->
    <main class="mt-5 pt-5">
      <div class="container">
        <!--Grid row-->
        <div class="row py-5">
          <!--Grid column-->
          <div class="col-md-12 text-center">

       <!-- Heading & Description -->
        <div class="wow fadeIn">
          <!--Section heading-->
          <img src="logo.png" class="center mb-3 animated rotateIn rounded-circle" style="width: 300px; height: 300px;">
          <h2 class="h1 text-center mb-5">Submission of Forms Temporarily Paused</h2>
          <!--Section description-->
          <p class="text-center">If you are currently submitting form, your form will be INVALIDATED.</p>

        </div>
        <!-- Heading & Description -->

        <!-- 
            <div class="card">
              <div class="card-body">
              </div>
            </div>
          -->


          </div>
          <!--Grid row-->
        </div>
        <!--Container-->
      </div>
    </main>
    <!--/Main layout-->



<?php include 'includes/footer.php'; ?>

<?php include 'includes/scripts.php'; ?>
  </body>
</html>
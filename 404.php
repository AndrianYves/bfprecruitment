<?php include 'includes/header.php'; ?>

  <body>

  <?php include 'includes/nav.php'; ?>
    <!--Main layout-->
    <main class="mt-5 pt-5">
      <div class="container">
        <!--Grid row-->
        <div class="row py-5">
          <!--Grid column-->
          <div class="col-md-12 text-center">

                  <?php
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']. "</a>
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>

       <!-- Heading & Description -->
        <div class="wow fadeIn">
          <!--Section heading-->
          <img src="logo.png" class="center mb-3 animated rotateIn rounded-circle" style="width: 300px; height: 300px;">
          <h2 class="h1 text-center mb-5 text-danger">Error!</h2>
          <!--Section description-->
          <p class="text-center">If you are currently submitting form, your form will be INVALIDATED.</p>
        </div>
        <!-- Heading & Description -->

        <!-- 
            <div class="card">
              <div class="card-body">
              </div>
            </div>
          -->


          </div>
          <!--Grid row-->
        </div>
        <!--Container-->
      </div>
    </main>
    <!--/Main layout-->
<?php include 'includes/footer.php'; ?>

<?php include 'includes/scripts.php'; ?>
  </body>
</html>
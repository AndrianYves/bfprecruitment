<?php
  require_once 'includes/conn.php';
  session_start();

  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\SMPT;
  use PHPMailer\PHPMailer\Exception;

  if (isset($_POST['submit'])) {
    $conn->autocommit(FALSE);

    $result1 = mysqli_query($conn, "SELECT max(id) as id FROM applicants_pds where recruitmentDate = '$end'");
    $query = mysqli_fetch_assoc($result1);
    $applicantnumber = $query['id'] + 1;
    $result2 = mysqli_query($conn, "SELECT max(id) as id FROM applicants_pds");
    $query2 = mysqli_fetch_assoc($result2);
    $applicantid = $query2['id'] + 1;

    $result1->close();
    $error = false;
    $verificationcode = '0123456789';
    $verificationcode = str_shuffle($verificationcode);
    $verificationcode = substr($verificationcode, 0, 5);
    $allowed = array('image/JPEG', 'image/jpeg', 'image/JPG', 'image/jpg', 'image/X-PNG', 'image/PNG', 'image/png', 'image/x-png');

   if (empty($_POST['name'])) {
      $error = true;
      $_SESSION['error'][] = 'Firstname is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['name'])) {
      $error = true;
      $_SESSION['error'][] = 'Firstname is invalid.';
    } else {
      $name = test_input(strtolower($_POST['name']));
    }

    if (!empty($_POST['middle'])) {
      if (preg_match("/([%\$#\*]+)/", $_POST['middle'])) {
      $error = true;
      $_SESSION['error'][] = 'Middlename is invalid.';
      } else {
        $middle = test_input(strtolower($_POST['middle']));
      }
    }

     if (empty($_POST['last'])) {
      $error = true;
      $_SESSION['error'][] = 'Lastname is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['last'])) {
      $error = true;
      $_SESSION['error'][] = 'Lastname is invalid.';
    } else {
      $last = test_input(strtolower($_POST['last']));
    }
 
    if (empty($_FILES['image']['name'])) {
      $error = true;
      $_SESSION['error'][] = '2x2 photo is required.';
    } else {
      if ($_FILES['image']['size'] > 2000000) {
        $error = true;
        $_SESSION['error'][] = '2x2 photo size must be less than 2MB.';
      }

      if (in_array($_FILES['image']['type'], $allowed)){
        $image = $_FILES['image']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = '2x2 photo must be png and jpg only.';
      }
      
    }

    if (empty($_POST['birthday'])) {
      $error = true;
      $_SESSION['error'][] = 'Birthday is required.';
    } else {
      $birthday = $_POST['birthday'];
      $age = floor((time() - strtotime($birthday)) / 31556926);
    }

    if (empty($_POST['address'])) {
      $error = true;
      $_SESSION['error'][] = 'Address is required.';
    } else {
      $address = test_input(strtolower($_POST['address']));
    }

   if (empty($_POST['sex'])) {
      $error = true;
      $_SESSION['error'][] = 'Gender is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['sex'])) {
      $error = true;
      $_SESSION['error'][] = 'Gender is invalid.';
    } else {
      $sex = test_input(strtolower($_POST['sex']));
    }

    if (empty($_POST['civilstatus'])) {
      $error = true;
      $_SESSION['error'][] = 'Civilstatus is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['civilstatus'])) {
      $error = true;
      $_SESSION['error'][] = 'Civilstatus is invalid.';
    } else {
      $civilstatus = test_input(strtolower($_POST['civilstatus']));

      if($civilstatus == 'married'){
        if (empty($_FILES['marriageCertificate']['name'])) {
          $error = true;
          $_SESSION['error'][] = 'Marriage Certificate is required.';
        } else {
          if ($_FILES['marriageCertificate']['size'] > 2097152) {
            $error = true;
            $_SESSION['error'][] = 'Marriage Certificate image size must be less than 2MB.';
          }

          if (in_array($_FILES['marriageCertificate']['type'], $allowed)){
            $marriageCertificate = $_FILES['marriageCertificate']['name'];
          } else {
            $error = true;
            $_SESSION['error'][] = 'Marriage Certificate image must be png and jpg only.';
          }

        }
      }

    }

    if (empty($_POST['skills'])) {
      $error = true;
      $_SESSION['error'][] = 'Skills is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['skills'])) {
      $error = true;
      $_SESSION['error'][] = 'Skills is invalid.';
    } else {
      $skill = $_POST['skills'];
      if($skill == 'Others'){
        $skills = test_input(strtolower($_POST['otherskills']));
      } else {
        $skills = test_input(strtolower($_POST['skills']));
      }
    }

    if(!empty($_POST['contact'])) {
      if(!is_numeric($_POST['contact'])) {
        $error = true;
        $_SESSION['error'][] = 'Contact entered was not numeric.';
      } else if(strlen($_POST['contact']) < 10) {
        $error = true;
        $_SESSION['error'][] = 'Contact entered was not 11 digits long.';
      } else {
        $contact = test_input($_POST['contact']);
      }

    } 

    if (!empty($_POST['email'])) {
      if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $error = true;
        $_SESSION['error'][] = 'Invalid email format.'; 
      } else {
        $email = strtolower($_POST['email']);
      }
    }

    if (empty($_POST['barangayBirth'])) {
      $error = true;
      $_SESSION['error'][] = 'Barangay of your birthplace is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['barangayBirth'])) {
      $error = true;
      $_SESSION['error'][] = 'Barangay of your birthplace is invalid.';
    } else {
      $barangayBirth = test_input(strtolower($_POST['barangayBirth']));
    }

    if (empty($_POST['municipalityBirth'])) {
      $error = true;
      $_SESSION['error'][] = 'Municipality of your birthplace is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['municipalityBirth'])) {
      $error = true;
      $_SESSION['error'][] = 'Municipality of your birthplace is invalid.';
    } else {
      $municipalityBirth = test_input(strtolower($_POST['municipalityBirth']));
    }

    if (empty($_POST['provinceBirth'])) {
      $error = true;
      $_SESSION['error'][] = 'Province of your birthplace is required.';
    } elseif (preg_match("/([%\$#\*]+)/", $_POST['provinceBirth'])) {
      $error = true;
      $_SESSION['error'][] = 'Province of your birthplace is invalid.';
    } else {
      $provinceBirth = test_input(strtolower($_POST['provinceBirth']));
      $birthplace = $barangayBirth.', '.$municipalityBirth.', '.$provinceBirth;
    }

    if (!empty($_POST['restrictioncode'])) {
      $restrictioncode = test_input(strtolower($_POST['restrictioncode']));
    } 

    if (!empty($_POST['socialmedia'])) {
      $fbUrlCheck = '/^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/';
      $secondCheck = '/home((\/)?\.[a-zA-Z0-9])?/';

      if(preg_match($fbUrlCheck, $_POST['socialmedia']) == 1 && preg_match($secondCheck, $_POST['socialmedia']) == 0) {
        $socialmedia = $_POST['socialmedia'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'Facebook URL is invalid.';
      }
    } 

    if (empty($_FILES['applicationLetter']['name'])) {
      $error = true;
      $_SESSION['error'][] = 'Application Letter is required.';
    } else {
      if ($_FILES['applicationLetter']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Application Letter image size must be less than 2MB.';
      }

      if (in_array($_FILES['applicationLetter']['type'], $allowed)){
        $applicationLetter = $_FILES['applicationLetter']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'Application Letter image must be png and jpg only.';
      }
      
    }

    if (empty($_FILES['serviceRecord']['name'])) {
      $error = true;
      $_SESSION['error'][] = 'Service Record is required.';
    } else {
      if ($_FILES['serviceRecord']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Service Record image size must be less than 2MB.';
      }

      if (in_array($_FILES['serviceRecord']['type'], $allowed)){
        $serviceRecord = $_FILES['serviceRecord']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'Service Record image must be png and jpg only.';
      }
      
    }

    if (empty($_FILES['transcript']['name'])) {
      $error = true;
      $_SESSION['error'][] = 'Transcript of Record is required.';
    } else {
      if ($_FILES['transcript']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Transcript of Record image size must be less than 2MB.';
      }

      if (in_array($_FILES['transcript']['type'], $allowed)){
        $transcript = $_FILES['transcript']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'Transcript of Record image must be png and jpg only.';
      }
      
    }

    if (empty($_FILES['collegeDiploma']['name'])) {
      $error = true;
      $_SESSION['error'][] = 'College Diploma is required.';
    } else {
      if ($_FILES['collegeDiploma']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'College Diploma image size must be less than 2MB.';
      }

      if (in_array($_FILES['collegeDiploma']['type'], $allowed)){
        $collegeDiploma = $_FILES['collegeDiploma']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'College Diploma image must be png and jpg only.';
      }
      
    }

    if (empty($_FILES['secondEligibility']['name'])) {
      $error = true;
      $_SESSION['error'][] = 'Second Eligibility is required.';
    } else {
      if ($_FILES['secondEligibility']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Second Eligibility image size must be less than 2MB.';
      }

      if (in_array($_FILES['secondEligibility']['type'], $allowed)){
        $secondEligibility = $_FILES['secondEligibility']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'Second Eligibility image must be png and jpg only.';
      }
      
    }

    if (empty($_FILES['birthCertificate']['name'])) {
      $error = true;
      $_SESSION['error'][] = 'Birth Certificate is required.';
    } else {
      if ($_FILES['birthCertificate']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Birth Certificate image size must be less than 2MB.';
      }

      if (in_array($_FILES['birthCertificate']['type'], $allowed)){
        $birthCertificate = $_FILES['birthCertificate']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'Birth Certificate image must be png and jpg only.';
      }
      
    }


    if (empty($_FILES['barangayClearance']['name'])) {
      $error = true;
      $_SESSION['error'][] = 'Barangay Clearance is required.';
    } else {
      if ($_FILES['barangayClearance']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Barangay Clearance image size must be less than 2MB.';
      }

      if (in_array($_FILES['barangayClearance']['type'], $allowed)){
        $barangayClearance = $_FILES['barangayClearance']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'Barangay Clearance image must be png and jpg only.';
      }
      
    }

    if (empty($_FILES['mtcClearance']['name'])) {
      $error = true;
      $_SESSION['error'][] = 'MTC Clearance is required.';
    } else {
      if ($_FILES['mtcClearance']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'MTC Clearance image size must be less than 2MB.';
      }

      if (in_array($_FILES['mtcClearance']['type'], $allowed)){
        $mtcClearance = $_FILES['mtcClearance']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'MTC Clearance image must be png and jpg only.';
      }
      
    }

    if (empty($_FILES['nbiClearance']['name'])) {
      $error = true;
      $_SESSION['error'][] = 'NBI Clearance is required.';
    } else {
      if ($_FILES['nbiClearance']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'NBI Clearance image size must be less than 2MB.';
      }

      if (in_array($_FILES['nbiClearance']['type'], $allowed)){
        $nbiClearance = $_FILES['nbiClearance']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'NBI Clearance image must be png and jpg only.';
      }
      
    }

    if (empty($_FILES['policeClearance']['name'])) {
      $error = true;
      $_SESSION['error'][] = 'Police Clearance is required.';
    } else {
      if ($_FILES['policeClearance']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Police Clearance image size must be less than 2MB.';
      }

      if (in_array($_FILES['policeClearance']['type'], $allowed)){
        $policeClearance = $_FILES['policeClearance']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'Police Clearance image must be png and jpg only.';
      }
      
    }

    if (empty($_FILES['rtcClearance']['name'])) {
      $error = true;
      $_SESSION['error'][] = 'RTC Clearance is required.';
    } else {
      if ($_FILES['rtcClearance']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'RTC Clearance image size must be less than 2MB.';
      }

      if (in_array($_FILES['rtcClearance']['type'], $allowed)){
        $rtcClearance = $_FILES['rtcClearance']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'RTC Clearance image must be png and jpg only.';
      }
      
    }

    if (!empty($_FILES['waiverCertificate']['name'])) {
      if ($_FILES['waiverCertificate']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Waiver Certificate image size must be less than 2MB.';
      }

      if (in_array($_FILES['waiverCertificate']['type'], $allowed)){

        $waiverCertificate = $_FILES['waiverCertificate']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'Waiver Certificate image must be png and jpg only.';
      }
    }

    if (!empty($_FILES['tesdaDriverCertificate']['name'])) {
      if ($_FILES['tesdaDriverCertificate']['size'] > 2097152) {
        $error = true;
        $_SESSION['error'][] = 'Tesda Driver Certificate image size must be less than 2MB.';
      }

      if (in_array($_FILES['tesdaDriverCertificate']['type'], $allowed)){
        $tesdaDriverCertificate = $_FILES['tesdaDriverCertificate']['name'];
      } else {
        $error = true;
        $_SESSION['error'][] = 'Tesda Driver Certificate image must be png and jpg only.';
      }
    }

 
    $result = $conn->prepare("INSERT INTO applicants_pds(name, middle, last, age, birthday, birthplace, address, sex, civilstatus, skills, restrictioncode, contact, email, socialmedia, image, verificationcode, recruitmentDate, applicantnumber) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $result->bind_param("sssisssssssisssisi", $name, $middle, $last, $age, $birthday, $birthplace, $address, $sex, $civilstatus, $skills, $restrictioncode, $contact, $email, $socialmedia, $image, $verificationcode, $end, $applicantnumber);
    $result->execute();
    if ($conn->errno === 1062) {
      $error = true;
      $_SESSION['error'][] = 'Something Occured. Please Try Again.';
    }
    $result->close();

  $result2 = $conn->prepare("INSERT INTO applicants_folders(applicationLetter, serviceRecord, transcript, collegeDiploma, secondEligibility, birthCertificate, marriageCertificate, barangayClearance, mtcClearance, nbiClearance, policeClearance, rtcClearance, waiverCertificate, tesdaDriverCertificate, applicantID) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
  $result2->bind_param("ssssssssssssssi", $applicationLetter, $serviceRecord, $transcript, $collegeDiploma, $secondEligibility, $birthCertificate, $marriageCertificate, $barangayClearance, $mtcClearance, $nbiClearance, $policeClearance, $rtcClearance, $waiverCertificate, $tesdaDriverCertificate, $applicantid);
  $result2->execute();
  $result2->close();


    

     if (!$error) {
      if (!empty($email)) {
        require './vendor/autoload.php';
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com'; // Which SMTP server to use.
        $mail->Port = 587; // Which port to use, 587 is the default port for TLS security.
        $mail->SMTPSecure = 'tls'; // Which security method to use. TLS is most secure.
        $mail->SMTPAuth = true; // Whether you need to login. This is almost always required.
        $mail->Username = "zanrouck.gamad@gmail.com"; // Your Gmail address.
        $mail->Password = "farjaicwiyvjoghs"; // App Specific Password.
        $mail->setFrom('BFPCAR_Recruitment@itprojectbfp.epizy.com', 'BFP CAR Recruitment');
        $mail->addAddress($email);
        $mail->Subject = "Bureau of Fire Protection - CAR Application Form";
        $mail->Body = "
        Your Verification Code:

        $verificationcode
        ";

        // if (!$mail->send()){
        //   $error = true;
        //   $_SESSION['error'][] = 'Something Occured. Please Try Again.';
        // }
        $mail->send();
      }
      
      mysqli_commit($conn);
      mkdir('applicationforms/Current/'.$applicantid. ' - '.$last. ', '.$name, 0777, true);
      move_uploaded_file($_FILES["image"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'photo.'.pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION));
      move_uploaded_file($_FILES["applicationLetter"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Application Letter.'.pathinfo($_FILES["applicationLetter"]["name"], PATHINFO_EXTENSION));
      move_uploaded_file($_FILES["serviceRecord"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Service Record.'.pathinfo($_FILES["serviceRecord"]["name"], PATHINFO_EXTENSION));
      move_uploaded_file($_FILES["transcript"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Transcript.'.pathinfo($_FILES["transcript"]["name"], PATHINFO_EXTENSION));
      move_uploaded_file($_FILES["collegeDiploma"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'College Diploma.'.pathinfo($_FILES["collegeDiploma"]["name"], PATHINFO_EXTENSION));
      move_uploaded_file($_FILES["secondEligibility"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Second Eligibility.'.pathinfo($_FILES["secondEligibility"]["name"], PATHINFO_EXTENSION));
      move_uploaded_file($_FILES["birthCertificate"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Birth Certificate.'.pathinfo($_FILES["birthCertificate"]["name"], PATHINFO_EXTENSION));
      move_uploaded_file($_FILES["barangayClearance"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Barangay Clearance.'.pathinfo($_FILES["barangayClearance"]["name"], PATHINFO_EXTENSION));
      move_uploaded_file($_FILES["mtcClearance"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'MTC Clearance.'.pathinfo($_FILES["mtcClearance"]["name"], PATHINFO_EXTENSION));
      move_uploaded_file($_FILES["nbiClearance"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'NBI Clearance.'.pathinfo($_FILES["nbiClearance"]["name"], PATHINFO_EXTENSION));
      move_uploaded_file($_FILES["policeClearance"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Police Clearance.'.pathinfo($_FILES["policeClearance"]["name"], PATHINFO_EXTENSION));
      move_uploaded_file($_FILES["rtcClearance"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'RTC Clearance.'.pathinfo($_FILES["rtcClearance"]["name"], PATHINFO_EXTENSION));

      if (!empty($_FILES['marriageCertificate']['name'])) {
        move_uploaded_file($_FILES["marriageCertificate"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Marriage Certificate.'.pathinfo($_FILES["marriageCertificate"]["name"], PATHINFO_EXTENSION));
      }

      if (!empty($_FILES['waiverCertificate']['name'])) {
         move_uploaded_file($_FILES["waiverCertificate"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Waiver Certificate.'.pathinfo($_FILES["waiverCertificate"]["name"], PATHINFO_EXTENSION));
      }

      if (!empty($_FILES['tesdaDriverCertificate']['name'])) {
        move_uploaded_file($_FILES["tesdaDriverCertificate"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Tesda Driver Certificate.'.pathinfo($_FILES["tesdaDriverCertificate"]["name"], PATHINFO_EXTENSION));
      }
    
      
      $_SESSION['success'] = 'Message sent';
      $_SESSION['application'] = $applicantid;
      header('location: verification.php');
     } else {
      // $_SESSION['error'][] = "Mailer Error: ". $mail->ErrorInfo;
      mysqli_rollback($conn);
     }

    // if (!$error) {
    //   $conn->autocommit(TRUE);
    //   mkdir('applicationforms/Current/'.$applicantid. ' - '.$last. ', '.$name, 0777, true);
    //   move_uploaded_file($_FILES["image"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'photo.'.pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION));
      // move_uploaded_file($_FILES["applicationLetter"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Application Letter.'.pathinfo($_FILES["applicationLetter"]["name"], PATHINFO_EXTENSION));
      // move_uploaded_file($_FILES["serviceRecord"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Service Record.'.pathinfo($_FILES["serviceRecord"]["name"], PATHINFO_EXTENSION));
      // move_uploaded_file($_FILES["transcript"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Transcript.'.pathinfo($_FILES["transcript"]["name"], PATHINFO_EXTENSION));
      // move_uploaded_file($_FILES["collegeDiploma"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'College Diploma.'.pathinfo($_FILES["collegeDiploma"]["name"], PATHINFO_EXTENSION));
      // move_uploaded_file($_FILES["secondEligibility"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Second Eligibility.'.pathinfo($_FILES["secondEligibility"]["name"], PATHINFO_EXTENSION));
      // move_uploaded_file($_FILES["birthCertificate"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Birth Certificate.'.pathinfo($_FILES["birthCertificate"]["name"], PATHINFO_EXTENSION));
      // move_uploaded_file($_FILES["barangayClearance"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Barangay Clearance.'.pathinfo($_FILES["barangayClearance"]["name"], PATHINFO_EXTENSION));
      // move_uploaded_file($_FILES["mtcClearance"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'MTC Clearance.'.pathinfo($_FILES["mtcClearance"]["name"], PATHINFO_EXTENSION));
      // move_uploaded_file($_FILES["nbiClearance"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'NBI Clearance.'.pathinfo($_FILES["nbiClearance"]["name"], PATHINFO_EXTENSION));
      // move_uploaded_file($_FILES["policeClearance"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'Police Clearance.'.pathinfo($_FILES["policeClearance"]["name"], PATHINFO_EXTENSION));
      // move_uploaded_file($_FILES["rtcClearance"]["tmp_name"],"applicationforms/Current/$applicantid - $last, $name/".'RTC Clearance.'.pathinfo($_FILES["rtcClearance"]["name"], PATHINFO_EXTENSION));
    //   $_SESSION['success'] = 'Message sent';
    //   $_SESSION['application'] = $applicantid;
    //   header('location: verification.php');
    // } else {
    //   $conn->rollback();
    // }

}

 function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
 }

?>
<?php include 'includes/header.php'; ?>

  <body>

  <?php include 'includes/nav.php'; ?>
    <!--Main layout-->
    <main class="mt-5 pt-5">
      <div class="container">
        <!--Grid row-->
        <div class="row py-5">
          <!--Grid column-->
          <div class="col-md-12 text-center">
          <?php
        if(isset($_SESSION['error'])){ ?>
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button><h4><i class='icon fa fa-warning'></i> Error!</h4>
              <?php 
                foreach($_SESSION['error'] as $error){
                  echo "".$error."<br>";
                }
              ?>
            </div>
        <?php
            unset($_SESSION['error']);
          }
        ?>
            <div class="card">
              <div class="card-body">
                <div class="tab-content pt-2 pl-1" id="pills-tabContent">

                  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data" class="needs-validation" novalidate>


                      <div class="form-row">
                        <div class="col">
                          <label>* First name</label>
                          <input type="text" id="name" name="name" class="form-control mb-3" placeholder="First name" value="<?php echo isset($_POST["name"]) ? $_POST["name"] : ''; ?>" onkeypress="return /[a-z]/i.test(event.key)">
                          <div id="error_name" class="invalid-feedback"></div>
                        </div>
                        <div class="col">
                          <label>* Middle name</label>
                          <input type="text" id="middle" name="middle" class="form-control mb-3" placeholder="Middle name" value="<?php echo isset($_POST["middle"]) ? $_POST["middle"] : ''; ?>" onkeypress="return /[a-z]/i.test(event.key)">
                          <div id="error_middle" class="invalid-feedback"></div>
                        </div>
                        <div class="col">
                          <label>* Last name</label>
                          <input type="text" id="last" name="last" class="form-control mb-3" placeholder="Last name" value="<?php echo isset($_POST["last"]) ? $_POST["last"] : ''; ?>" onkeypress="return /[a-z]/i.test(event.key)">
                          <div id="error_last" class="invalid-feedback"></div>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col">
                          <label>* Birthday</label>
                          <input type="date" id="birthday" name="birthday" class="form-control mb-3" value="<?php echo isset($_POST["birthday"]) ? $_POST["birthday"] : ''; ?>">
                          <div id="error_birthday" class="invalid-feedback"></div>
                        </div>
                        <div class="col">
                          <label for="address">* Current Address</label>
                          <input type="text" id="address" name="address" class="form-control mb-3" placeholder="Full Address" value="<?php echo isset($_POST["address"]) ? $_POST["address"] : ''; ?>">
                          <div id="error_address" class="invalid-feedback"></div>
                        </div>
                        <div class="col">
                          <label for="contact">* Phone number</label>
                          <input type="number" id="contact" name="contact" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="11" class="form-control mb-3" placeholder="Phone Number" value="<?php echo isset($_POST["contact"]) ? $_POST["contact"] : ''; ?>" min="0" required>
                          <div id="error_contact" class="invalid-feedback"></div>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col"><label>*Gender</label><br>
                          <div class="custom-control custom-radio custom-control-inline text-left">
                            <input <?php if (isset($_POST['sex']) && $_POST['sex']=="Male") echo "checked";?> type="radio" class="custom-control-input mb-4" id="sex1" name="sex" value="Male">
                            <label class="custom-control-label" for="sex1">Male</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline text-left">
                            <input <?php if (isset($_POST['sex']) && $_POST['sex']=="Female") echo "checked";?> type="radio" class="custom-control-input mb-4" id="sex2" name="sex" value="Female"> 
                            <label class="custom-control-label" for="sex2">Female</label>
                           <div id="error_sex" class="invalid-feedback"></div>
                          </div>
                        </div>
                        <div class="col"><label>*Civil Status</label><br>
                          <div class="custom-control custom-radio custom-control-inline text-left">
                            <input <?php if (isset($_POST['civilstatus']) && $_POST['civilstatus']=="Single") echo "checked";?> type="radio" class="custom-control-input mb-4" id="single" name="civilstatus" value="Single">
                            <label class="custom-control-label" for="single">Single</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline text-left">
                            <input <?php if (isset($_POST['civilstatus']) && $_POST['civilstatus']=="Married") echo "checked";?> type="radio" class="custom-control-input mb-4" id="married" name="civilstatus" value="Married">
                            <label class="custom-control-label" for="married">Married</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline text-left">
                            <input <?php if (isset($_POST['civilstatus']) && $_POST['civilstatus']=="Widowed") echo "checked";?> type="radio" class="custom-control-input mb-4" id="widowed" name="civilstatus" value="Widowed">
                            <label class="custom-control-label" for="widowed">Widowed</label>
                            <div id="error_civilstatus" class="invalid-feedback"></div>
                          </div>
                        </div>
                        <div class="col">
                          <label for="email">* E-mail</label>
                          <input type="email" id="email" name="email" class="form-control mb-3" placeholder="Email Address" value="<?php echo isset($_POST["email"]) ? $_POST["email"] : ''; ?>" required>
                          <div id="error_email" class="invalid-feedback"></div>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-3"><label for="image">* 2x2 Photo</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input mb-4" id="image" name="image" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="image">Upload 2x2 Photo</label>
                              <div id="error_image" class="invalid-feedback"></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-6"><label>*Birth Place</label>
                          <div class="input-group">
                            <input type="text" id="barangayBirth" name="barangayBirth" class="form-control mb-3" placeholder="Barangay" value="<?php echo isset($_POST["barangayBirth"]) ? $_POST["barangayBirth"] : ''; ?>">
                            <input type="text" id="municipalityBirth" name="municipalityBirth" class="form-control mb-3" placeholder="Municipality" value="<?php echo isset($_POST["municipalityBirth"]) ? $_POST["municipalityBirth"] : ''; ?>">
                            <input type="text" id="provinceBirth" name="provinceBirth" class="form-control mb-3" placeholder="Province" value="<?php echo isset($_POST["provinceBirth"]) ? $_POST["provinceBirth"] : ''; ?>">
                            <div id="error_birthplace" class="invalid-feedback"></div><br>
                          </div>
                          
                        </div>
                        <div class="col-3"><label>Facebook URL</label>
                          <input type="text" id="socialmedia" name="socialmedia" class="form-control mb-3" placeholder="Facebook URL" value="<?php echo isset($_POST["socialmedia"]) ? $_POST["socialmedia"] : ''; ?>">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col">
                          <label>*Specialization</label><br>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" onclick="document.getElementById('others').disabled=true;document.getElementById('restrictioncode').disabled=true;" class="custom-control-input mb-4" id="Architecture" name="skills" value="Architect">
                            <label class="custom-control-label" for="Architecture">Architect</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" onclick="document.getElementById('others').disabled=true;document.getElementById('restrictioncode').disabled=true;" class="custom-control-input mb-4" id="Engineer" name="skills" value="Engineer">
                            <label class="custom-control-label" for="Engineer">Engineer</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" onclick="document.getElementById('others').disabled=false;document.getElementById('restrictioncode').disabled=true;" class="custom-control-input mb-4" id="Others" name="skills" value="Others">
                            <label class="custom-control-label" for="Others">Others</label>
                            <div class="col">
                              <input type="text" id="others" name="otherskills" class="form-control" placeholder="Doctor, Lawyer, etc..." disabled>
                            </div>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" onclick="document.getElementById('others').disabled=true;document.getElementById('restrictioncode').disabled=false;" class="custom-control-input mb-4" id="Driver" name="skills" value="Driver">
                            <label class="custom-control-label" for="Driver">Driver</label>
                            <div class="col">
                              <select class="custom-select d-block w-100" id="restrictioncode" name="restrictioncode" disabled>
                                <option disabled selected>Restriction Code</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                              </select>
                              <div id="error_skills" class="invalid-feedback"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <!-- Material form register -->
                    <h6 class="red-text text-center py-4 pulse">
                    NOTE: Upload your files as JPEG, JPG, or PNG
                    </h6>
                    <div class="row">
                      <div class="col-md-6 text-center">
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="applicationLetter" id="applicationLetter" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="applicationLetter">Application Letter</label>
                              <div id="error_application" class="invalid-feedback"></div>
                            </div>
                          </div>
                        </div>
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="serviceRecord" id="serviceRecord" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="serviceRecord">Authenticated Service Record</label>
                            </div>
                          </div>
                        </div>
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="transcript" id="transcript" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="transcript">Transcript of Records</label>
                            </div>
                          </div>
                        </div>
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="collegeDiploma" id="collegeDiploma" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="">College Diploma</label>
                            </div>
                          </div>
                        </div>
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="secondEligibility" id="secondEligibility" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="">Second Level Eligibility (Issued from CSC/PRC)</label>
                            </div>
                          </div>
                        </div>
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="birthCertificate" id="birthCertificate" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="">Certificate of Live Birth (Issued by PSA)</label>
                            </div>
                          </div>
                        </div>
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="marriageCertificate"id ="marriageCertificate" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="">Certificate of Marriage (If Applicable)</label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 text-center">
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="barangayClearance" id="barangayClearance" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="">Barangay Clearance</label>
                            </div>
                          </div>
                        </div>
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="mtcClearance" id="mtcClearance" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="">MTC Clearance</label>
                              
                            </div>
                          </div>
                        </div>
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="nbiClearance" id="nbiClearance" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="">NBI Clearance</label>
                            </div>
                          </div>
                        </div>
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="policeClearance" id="policeClearance" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="">Police Clearance</label>
                            </div>
                          </div>
                        </div>
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="rtcClearance" id="rtcClearance" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="">RTC Clearance</label>
                            </div>
                          </div>
                        </div>
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="waiverCertificate" id="waiverCertificate" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="">Certificate of Waiver (age/height deficiencies if applicable)</label>
                            </div>
                          </div>
                        </div>
                        <div class="md-form mt-0">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="tesdaDriverCertificate" id="tesdaDriverCertificate" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label text-left" for="">TESDA Driver Certificate (If Applicable)</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div><!--Grid row-->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalLoginForm">Submit</button>
                    
                    <!-- Form -->
                  </div>
                </div>
                <ul class="nav nav-pills justify-content-end" id="pills-tab" role="tablist">
                  <li class="nav-item" style="display: none;" id="prevbut">
                    <button class="btn btn-primary btn-sm waves-effect" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" onclick="myFunction()">
                      <h5>Prev</h5>
                    </button>
                  </li>
                  <li class="nav-item" id="nextbut">
                    <button class="btn btn-primary btn-sm waves-effect" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" onclick="myFunction()">
                      <h5>Next</h5>
                    </button>
                  </li>
                </ul>
              </div>
            </div>
            <!--Grid column-->

          </div>
          <!--Grid row-->
        </div>
        <!--Container-->
      </div>

    </main>
    <!--/Main layout-->


  <!-- Are you sure -->
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Are you sure?</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <img src="logo.png" class="center mb-3 animated rotateIn rounded-circle" style="width: 150px; height: 150px;">
          <p class="text-center">Please double check the the assessment before submitting to avoid unncesseary mistakes.<br>
          </p>

        </div>
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</a>
        <button class="btn btn-primary waves-effect" name="submit" id="submit"><i class="fa fa-check-square-o"></i>Yes</button>
      </div>
      </form>
    </div>
    <!--/.Content-->
  </div>
</div>

<?php include 'includes/footer.php'; ?>
    <!-- SCRIPTS -->
    <script type="text/javascript">
      function myFunction() {
        var x = document.getElementById("prevbut");
        var y = document.getElementById("nextbut");
        if (x.style.display === "none") {
          x.style.display = "block";
          y.style.display = "none";
        } else {
          x.style.display = "none";
          y.style.display = "block";
        }
      }
    </script>

    <!-- JQuery -->
<?php include 'includes/scripts.php'; ?>
<script type="text/javascript">
  (function() {
'use strict';
window.addEventListener('load', function() {
// Fetch all the forms we want to apply custom Bootstrap validation styles to
var forms = document.getElementsByClassName('needs-validation');
// Loop over them and prevent submission
var validation = Array.prototype.filter.call(forms, function(form) {
form.addEventListener('submit', function(event) {
if (form.checkValidity() === false) {
event.preventDefault();
event.stopPropagation();
}
form.classList.add('was-validated');
}, false);
});
}, false);
})();
</script>
<script>
$(document).ready(function(){
 
 $('#pills-profile-tab').click(function(){
  var error_name = '';
  var error_middle = '';
  var error_name = '';
  var error_email = '';
  var error_image = '';
  var error_birthday = '';
  var error_address = '';
  var error_birthplace = '';
  var error_sex = '';
  var error_contact = '';

var sex = document.getElementsByName('sex');
var ischecked_sex = false;
for ( var i = 0; i < sex.length; i++) {
    if(sex[i].checked) {
        ischecked_sex = true;

        break;
    }
}
if(!ischecked_sex)   { 
    $('#sex1').addClass('is-invalid');
    $('#sex2').addClass('is-invalid');
} else{
    $('#sex1').removeClass('is-invalid');
    $('#sex2').removeClass('is-invalid');
    $('#sex1').addClass('is-valid');
    $('#sex2').addClass('is-valid');
}

var civilstatus = document.getElementsByName('civilstatus');
var ischecked_civilstatus = false;
for ( var i = 0; i < civilstatus.length; i++) {
    if(civilstatus[i].checked) {
        ischecked_civilstatus = true;
        break;
    }
}
if(!ischecked_civilstatus)   { 
    $('#single').addClass('is-invalid');
    $('#married').addClass('is-invalid');
    $('#widowed').addClass('is-invalid');
} else {
    $('#single').removeClass('is-invalid');
    $('#married').removeClass('is-invalid');
    $('#widowed').removeClass('is-invalid');
    $('#single').addClass('is-valid');
    $('#married').addClass('is-valid');
    $('#widowed').addClass('is-valid');
}

var skills = document.getElementsByName('skills');
var ischecked_skills = false;
for ( var i = 0; i < skills.length; i++) {
    if(skills[i].checked) {
        ischecked_skills = true;
        break;
    }
}
if(!ischecked_skills)   { 
    $('#Engineer').addClass('is-invalid');
    $('#Architecture').addClass('is-invalid');
    $('#Others').addClass('is-invalid');
    $('#Driver').addClass('is-invalid');
} else {
    $('#Engineer').removeClass('is-invalid');
    $('#Architecture').removeClass('is-invalid');
    $('#Others').removeClass('is-invalid');
    $('#Driver').removeClass('is-invalid');
    $('#Engineer').addClass('is-valid');
    $('#Architecture').addClass('is-valid');
    $('#Others').addClass('is-valid');
    $('#Driver').addClass('is-valid');
}

  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  
  if($.trim($('#name').val()).length == 0){
   error_name = 'First Name is required';
   $('#error_name').text(error_name);
   $('#name').addClass('is-invalid');
  } else {
   error_name = '';
   $('#error_name').text(error_name);
   $('#name').removeClass('is-invalid');
   $('#name').addClass('is-valid');
  }

    if($.trim($('#middle').val()).length == 0){
   error_middle = 'Middle Name is required';
   $('#error_middle').text(error_middle);
   $('#middle').addClass('is-invalid');
  } else {
   error_middle = '';
   $('#error_middle').text(error_middle);
   $('#middle').removeClass('is-invalid');
   $('#middle').addClass('is-valid');
  }

  if($.trim($('#last').val()).length == 0){
   error_last = 'Last Name is required';
   $('#error_last').text(error_last);
   $('#last').addClass('is-invalid');
  } else {
   error_last = '';
   $('#error_last').text(error_last);
   $('#last').removeClass('is-invalid');
  $('#last').addClass('is-valid');
  }
  
  if($.trim($('#image').val()).length == 0){
   error_image = 'Image is required';
   $('#error_image').text(error_image);
   $('#image').addClass('is-invalid');
  } else {
   error_image = '';
   $('#error_image').text(error_image);
   $('#image').removeClass('is-invalid');
   $('#image').addClass('is-valid');
  }

  if($.trim($('#birthday').val()).length == 0){
   error_birthday = 'Birthday is required';
   $('#error_birthday').text(error_birthday);
   $('#birthday').addClass('is-invalid');
  } else {
   error_birthday = '';
   $('#error_birthday').text(error_birthday);
   $('#birthday').removeClass('is-invalid');
   $('#birthday').addClass('is-valid');
  }

  if($.trim($('#contact').val()).length == 0){
   error_contact = 'Phone Number is required';
   $('#error_contact').text(error_contact);
   $('#contact').addClass('is-invalid');
  } else {
   error_contact = '';
   $('#error_contact').text(error_contact);
   $('#contact').removeClass('is-invalid');
   $('#contact').addClass('is-valid');
  }

  if($.trim($('#email').val()).length == 0){
   error_email = 'Email is required';
   $('#error_email').text(error_email);
   $('#email').addClass('is-invalid');
  } else {
   error_email = '';
   $('#error_email').text(error_email);
   $('#email').removeClass('is-invalid');
   $('#email').addClass('is-valid');
  }

  if($.trim($('#address').val()).length == 0){
   error_address = 'Address is required';
   $('#error_address').text(error_address);
   $('#address').addClass('is-invalid');
  } else {
   error_address = '';
   $('#error_address').text(error_address);
   $('#address').removeClass('is-invalid');
   $('#address').addClass('is-valid');
  }

  if($.trim($('#barangayBirth').val()).length == 0){
   error_birthplace = 'Full birthplace is required';
   $('#error_birthplace').text(error_birthplace);
   $('#barangayBirth').addClass('is-invalid');
  } else {
   error_birthplace = '';
   $('#error_birthplace').text(error_birthplace);
   $('#barangayBirth').removeClass('is-invalid');
   $('#barangayBirth').addClass('is-valid');
  }

    if($.trim($('#municipalityBirth').val()).length == 0){
   error_birthplace = 'Full birthplace is required';
   $('#error_birthplace').text(error_birthplace);
   $('#municipalityBirth').addClass('is-invalid');
  } else {
   error_birthplace = '';
   $('#error_birthplace').text(error_birthplace);
   $('#municipalityBirth').removeClass('is-invalid');
   $('#municipalityBirth').addClass('is-valid');
  }

    if($.trim($('#provinceBirth').val()).length == 0){
   error_birthplace = 'Full birthplace is required';
   $('#error_birthplace').text(error_birthplace);
   $('#provinceBirth').addClass('is-invalid');
  } else {
   error_birthplace = '';
   $('#error_birthplace').text(error_birthplace);
   $('#provinceBirth').removeClass('is-invalid');
   $('#provinceBirth').addClass('is-valid');
  }


  if(error_name != '' || error_middle != '' || error_name != '' || error_email != '' || error_image != '' || error_birthday != '' || error_address != '' || error_birthplace != '' || error_sex != '' || error_contact != '')  {
    var x = document.getElementById("prevbut");
    var y = document.getElementById("nextbut");
    x.style.display = "none";
    y.style.display = "block";
   return false;

  } else {
    var x = document.getElementById("prevbut");
    var y = document.getElementById("nextbut");

    x.style.display = "block";
    y.style.display = "block";

  }
 });
 


});
</script>
<script type="text/javascript">
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
 if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 

today = yyyy+'-'+mm+'-'+dd;
var minage = yyyy - 18;
var maxage = yyyy - 36;
var maxdate = minage+'-'+mm+'-'+dd;
var mindate = maxage+'-'+'01'+'-'+'01';
document.getElementById("birthday").setAttribute("max", maxdate);
document.getElementById("birthday").setAttribute("min", mindate);
</script>
  </body>
</html>